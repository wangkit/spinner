import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:spinner/network/api_services.dart';
import 'package:spinner/network/downloader.dart';
import 'package:spinner/network/iap_manager.dart';
import 'package:spinner/network/socket.dart';
import 'package:spinner/network/uploader.dart';
import 'package:spinner/page_route/get_route.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/time_utils.dart';
import 'package:encrypt/encrypt.dart' as en;
import 'package:spinner/utils/utils.dart';

/// Firebase user holder
User firebaseUser;

FirebaseAnalytics analytics;

final firestore = FirebaseFirestore.instance;

/// Cloud messaging
FirebaseMessaging firebaseMessaging;

/// Remote config
RemoteConfig remoteConfig;

/// Cat key
String catKey;

/// is check face
bool checkFace;

/// Firebase auth instance
final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

/// Firebase db collection name
final messageDbName = "message";
final chatDbName = "chat";

/// Quick profile
QuickProfile qp = QuickProfile(
  uniqueId: null,
  id: null,
  secret: null,
  email: null,
  password: null,
);

/// my strict preference
StrictPreference myStrictPreference;

/// my preference
UserPreference myPreference;

/// my profile
UserProfile myProfile;

/// Utils instance
Utils utils;

/// socket instance
SocketManager socketManager;

/// iap instance
IapManager iapManager;

/// Downloader instance
Downloader downloader;

/// Uploader instance
Uploader uploader;

/// Time utils instance
TimeUtils timeUtils;

/// Api manager
ApiManager apiManager;

/// Navigator get
GetRoute getRoute;

String apiBaseUrl;

/// App font family
const appFontFamily = "proxima";

/// header font familt
const headerFontFamily = "proxima_alt_bold";

/// Shared preference instance
SharedPreferences prefs;

/// Lists for emoji storage
List<String> recentEmojis = new List();
Map<String, String> smileyMap = new Map();
Map<String, String> animalMap = new Map();
Map<String, String> foodMap = new Map();
Map<String, String> travelMap = new Map();
Map<String, String> activityEmojiMap = new Map();
Map<String, String> objectMap = new Map();
Map<String, String> symbolMap = new Map();
Map<String, String> flagMap = new Map();

/// Share message
const shareMsg = "The newest dating platform where you will find your love.";

/// default black color
const defaultBlackColor = Colors.black87;

/// default white color
const defaultWhiteColor = Colors.white;

/// App name
final myAppName = "Spinner";

/// Company contact email
const companyEmail = "admin@yarner.app";

/// Icons 8 link
const icon8Link = "https://icons8.com/";

/// Company website
const companyWebsiteLink = "yarner.app";

/// Forum EULA link
const eulaLink = companyWebsiteLink + "/#/terms-of-use";

/// Bucket url
const bucketURl = "https://spinner-version.s3.us-east-2.amazonaws.com";

/// Yarner android link
const yarnerAndroidLink = "https://play.google.com/store/apps/details?id=com.story.yarner";

/// Yarner iOS link
const yarnerIosLink = "https://apps.apple.com/hk/app/yarner/id1512119130";

/// Link to check version
const checkVersionAndGetUrls = bucketURl + "/version.txt";

/// Link to religion
const religionLink = bucketURl + "/religion.txt";

/// Link to profession
const professionLink = bucketURl + "/professions.txt";

/// Link to hobbies
const hobbyLink = bucketURl + "/hobbies.txt";

/// Link to country
const countryLink = bucketURl + "/country.txt";

/// Link to school
const schoolLink = bucketURl + "/schools.txt";

/// Link to instruction
const instructionLink = bucketURl + "/instruction.txt";

/// Link to ethnicity
const ethnicityLink = bucketURl + "/ethnicity.txt";

/// Link to education
const educationLink = bucketURl + "/education.txt";

/// apple link and google link
String googleStoreUrl, appleStoreUrl;

/// Link to privacy policy
const privacyPolicyUrl = companyWebsiteLink + "/#/privacy-policy";

/// App package information retrieved
String appName, versionName;
int versionCode;

/// first in app dialog
bool hasShownFirstInAppDialog;

/// App dimension
double deviceHeight, deviceWidth;

/// url prefix
const urlPrefix = "https://www.";

const userTag = "@";

const normalTag = "#";

/// cache user details for quick use
Map<String, String> nicknameVsColorMap = {};
Map<String, String> nicknameVsIdMap = {};

/// List of ethnicity
List<String> ethnicityList = [];

/// List of schools
List<String> schoolList = [];

/// List of religion
List<String> religionList = [];

/// List of country
List<String> countryList = [];

/// List of education
List<String> educationLevelList = [];

/// List of profession
List<String> professionList = [];

/// List of instruction
List<String> instructionList = [];

/// List of hobbies
List<String> hobbyList = [];

/// List of zodiac signs
List<String> zodiacSignList = [
  TextData.aries,
  TextData.taurus,
  TextData.gemini,
  TextData.cancer,
  TextData.leo,
  TextData.virgo,
  TextData.libra,
  TextData.scorpio,
  TextData.sagittarius,
  TextData.capricorn,
  TextData.aquarius,
  TextData.pisces,
];

/// Encrypter
final iv = en.IV.fromLength(16);
final encrypter = en.Encrypter(en.AES(en.Key.fromLength(32)));

/// user placeholder
const placeholder = "assets/placeholder.png";
const placeholderLink = "https://res.cloudinary.com/hyclslbe2/image/upload/v1588150703/profile/placeholder_s9bajp.png";

/// video placeholder
const videoPlaceholder = "assets/video_placeholder.png";

/// Item id on both stores
class VipSubscription {
  static const String monthKid = 'vip.subscription.monthly';
  static const String entitlementId = 'vip';
  static const String yearKid = 'vip.subscription.yearly';
  static const String halfYearKid = 'vip.subscription.halfyearly';
}