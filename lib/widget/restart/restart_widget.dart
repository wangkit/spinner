import 'package:flutter/cupertino.dart';
import 'package:spinner/constants/const.dart';

class RestartMainWidget extends StatefulWidget {
  RestartMainWidget({this.child});

  final Widget child;

  static void restartApp() {
    getRoute.getContext().findAncestorStateOfType<_RestartMainWidgetState>().restartApp();
  }

  @override
  _RestartMainWidgetState createState() => _RestartMainWidgetState();
}

class _RestartMainWidgetState extends State<RestartMainWidget> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: widget.child,
    );
  }
}