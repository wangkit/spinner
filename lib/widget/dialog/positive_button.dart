import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';

class PositiveButton extends StatefulWidget {
  final Function onPressed;
  final String text;
  final Color color;
  final bool isHalfWidth;
  final bool autoPop;
  final TextStyle style;
  final bool useFlexible;

  PositiveButton({
    Key key,
    this.onPressed,
    this.text = TextData.yes,
    this.color = success,
    this.isHalfWidth = true,
    this.autoPop = true,
    this.useFlexible = true,
    this.style,
  }) : super(key: key);

  @override
  PositiveButtonState createState() => PositiveButtonState();
}

class PositiveButtonState extends State<PositiveButton> {

  Widget _main() {
    return Padding(
      padding: EdgeInsets.all(4.0),
      child: CustomBouncingWidget(
        child: Container(
          width: widget.isHalfWidth ? deviceWidth * 0.45 : deviceWidth * 0.9,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(DialogConfig.dialogCornerRadius),
                side: BorderSide(color: widget.color)
            ),
            color: widget.color,
            child: Text(
              widget.text,
              style: widget.style != null ? widget.style : DialogConfig.dialogButtonStyle,
            ),
            onPressed: () {
              if (widget.autoPop) {
                getRoute.pop();
              }
              if (widget.onPressed != null) {
                widget.onPressed();
              }
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.useFlexible ? Flexible(
      child: _main(),
    ) : _main();
  }
}