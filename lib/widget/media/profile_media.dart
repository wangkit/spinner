import 'dart:async';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/init_dev.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/icon_set/detail_icons.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/gender_status.dart';
import 'package:spinner/resources/status/preferred_relationship_status.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/divider/separation_dot.dart';
import 'package:spinner/widget/media/image/image_holder.dart';
import 'package:spinner/widget/media/video/video_download_thumbnail.dart';
import 'package:visibility_detector/visibility_detector.dart';

import 'image/video_url_thumbnail.dart';

class ProfileMedia extends StatefulWidget {
  final UserProfile userProfile;
  final bool shouldAutoPlay;

  ProfileMedia({
    Key key,
    @required this.userProfile,
    this.shouldAutoPlay = false,
  }) : super(key: key);

  @override
  ProfileMediaState createState() => ProfileMediaState();
}

class ProfileMediaState extends State<ProfileMedia> {

  UserProfile _thisProfile;
  SwiperController _swipeController;
  int _realMediaLength;

  @override
  void initState() {
    _thisProfile = widget.userProfile;
    _swipeController = SwiperController();
    _realMediaLength = _thisProfile.images.length + (_thisProfile.video.isNullOrEmpty ? 0 : 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: deviceWidth,
        height: utils.getProfileMediaHeight(),
        child: Swiper(
          itemCount: _realMediaLength == 0 ? 1 : _realMediaLength,
          autoplay: widget.shouldAutoPlay,
          autoplayDisableOnInteraction: true,
          /// if number of images are smaller than or equal to 1, then no need scroll
          physics: _realMediaLength <= 1 ? NeverScrollableScrollPhysics() : null,
          autoplayDelay: 10000,
          pagination: SwiperPagination(
            builder: DotSwiperPaginationBuilder(
              activeColor: utils.getVisibleColor(_thisProfile.color),
            )
          ),
          controller: _swipeController,
          itemWidth: deviceWidth,
          itemBuilder: (ctx, newIndex) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(imageRadius),
                child: !_thisProfile.video.isNullOrEmpty && newIndex == 0 ? VideoUrlThumbnail(
                  thumbnail: _thisProfile.videoThumnbnail,
                  originalUrl: _thisProfile.video,
                  videoUrl: _thisProfile.video,
                ) : ImageHolder(
                  index: _thisProfile.video.isNullOrEmpty ? newIndex : newIndex - 1,
                  width: deviceWidth,
                  name: utils.getFullName(_thisProfile),
                  userColorCode: _thisProfile.color,
                  imageList: _thisProfile.images.isEmpty && _thisProfile.video.isNullOrEmpty ? [placeholderLink] : _thisProfile.images,
                  heroKey: UniqueKey().toString(),
                  nextPage: () {
                    if (_realMediaLength > 1) {
                      _swipeController.next();
                    }
                  },
                  previousPage: () {
                    if (_realMediaLength > 1) {
                      _swipeController.previous();
                    }
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
