import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/widget/animation/open_container_wrapper.dart';
import 'package:spinner/widget/media/video/video_viewer.dart';

/// Holds an image and acts as an access point to image viewer
class VideoUrlThumbnail extends StatefulWidget {
  final String videoUrl;
  final String originalUrl;
  final String thumbnail;

  VideoUrlThumbnail({
    Key key,
    @required this.videoUrl,
    @required this.originalUrl,
    @required this.thumbnail,
  }) : super(key: key);
  @override
  _VideoUrlThumbnailState createState() => _VideoUrlThumbnailState();
}

class _VideoUrlThumbnailState extends State<VideoUrlThumbnail> with AutomaticKeepAliveClientMixin<VideoUrlThumbnail> {

  get wantKeepAlive => true;
  String videoUrl;
  String originalUrl;
  String thumbnail;

  @override
  void initState() {
    originalUrl = widget.originalUrl;
    videoUrl = widget.videoUrl;
    thumbnail = widget.thumbnail;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onTap: () {
        getRoute.navigateTo(VideoViewer(
          showAppBar: false,
          videoUrl: videoUrl,
          videoHeroKey: UniqueKey().toString(),
          originalUrl: originalUrl,
        ));
      },
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: deviceWidth,
            child: CachedNetworkImage(
              imageUrl: thumbnail,
              fit: BoxFit.cover,
            ),
          ),
          Image.asset(
            "assets/icon/play_video.png",
            cacheHeight: (deviceWidth).toInt(),
            cacheWidth: (deviceWidth).toInt(),
          ),
        ],
      ),
    );
  }
}