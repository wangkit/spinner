import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:encrypt/encrypt.dart' as en;
import 'package:spinner/app/chat/chat_page.dart';
import 'package:spinner/app/profile/view_profile_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/block_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/view/dialogs/report_dialog.dart';
import 'package:spinner/widget/animation/open_container_wrapper.dart';
import 'package:spinner/widget/button/bottom_sheet_button.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/widget/media/image/thumbnail.dart';
import 'package:spinner/widget/tag/tag.dart';

class ChatItemDisplay extends StatefulWidget {

  final ChatItem chatItem;

  ChatItemDisplay({
    Key key,
    @required this.chatItem,
  }) : super(key: key);

  @override
  ChatItemDisplayState createState() => ChatItemDisplayState();
}

class ChatItemDisplayState extends State<ChatItemDisplay> with AutomaticKeepAliveClientMixin<ChatItemDisplay> {

  ChatItem _chatItem;
  String _chatId;

  @override
  bool get wantKeepAlive => true;

//  String getDecodedMessage(String encodedMessage) {
//    return encrypter.decrypt(en.Encrypted.from64(encodedMessage), iv: iv);
//  }

  @override
  void initState() {
    _chatItem = widget.chatItem;
    _chatId = utils.getChatId([qp.id, _chatItem.targetId]);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void getChatBottomSheet() {
    getGenericBottomSheet([
      BottomSheetButton(
        label: TextData.viewProfile,
        iconData: MdiIcons.accountDetails,
        onTap: () {
          Dialogs.showLoadingDialog(title: TextData.fetchingData);
          apiManager.getProfileById(_chatItem.targetId).then((jsonResponse) {
            getRoute.pop();
            int status = jsonResponse[DataKey.status];
            if (status == StatusCode.ok) {
              apiManager.viewProfileLog(_chatItem.targetId);
              List<dynamic> listOfProfile = jsonResponse[DataKey.profile];
              List<dynamic> listOfPreference = jsonResponse[DataKey.preference];
              if (listOfProfile.length > 0 && listOfPreference.length > 0) {
                UserPreference _pref = UserPreference.generateUserPreference(listOfPreference).first;
                UserProfile _profile = UserProfile.generateUserProfile(listOfProfile).first;
                getRoute.navigateTo(
                  ViewProfilePage(
                    isMyProfile: false,
                    userProfile: _profile,
                    userPreference: _pref,
                  )
                );
              } else {
                utils.toast(TextData.incompleteProfile, isWarning: true);
              }
            } else {
              utils.toast(TextData.connectionFailed, isWarning: true);
            }
          });
        },
      ),
      BottomSheetButton(
        iconData: Icons.delete,
        label: TextData.deleteChat,
        onTap: () {
          getBinaryDialog(TextData.deleteChat, TextData.deleteChatForMyself, () {
            utils.deleteChat(true, _chatItem.targetId);
          });
        },
      ),
      BottomSheetButton(
        label: TextData.clearChat,
        iconData: Icons.clear_all,
        onTap: () {
          getBinaryDialog(TextData.deleteAllMessages, TextData.deleteAllMessagesAlert, () async {
            utils.clearMessageInChat(_chatId, _chatItem.targetId);
          });
        },
      ),
      BottomSheetButton(
        label: TextData.blockUser,
        iconData: Icons.block,
        onTap: () => getBlockDialog(_chatItem.targetId, utils.getFullNameByChat(_chatItem), _chatId, null),
      ),
      BottomSheetButton(
        label: TextData.report,
        iconData: Icons.report,
        onTap: () => getReportDialog(_chatItem.targetId, utils.getFullNameByChat(_chatItem), null),
      ),
    ]);
  }

  void setNoUnread() {
    _chatItem.hasUnread = false;
  }

  @override
  Widget build(BuildContext context) {

    super.build(context);
    return Material(
      color: appBgColor,
      child: InkWell(
        onTap: () {
          getRoute.navigateTo(ChatPage(
            targetProfile: UserProfile(
              color: _chatItem.targetColor,
              email: _chatItem.targetEmail,
              id: _chatItem.targetId,
              lastName: _chatItem.targetLastName,
              firstName: _chatItem.targetFirstName,
              images: _chatItem.targetImages,
              blockedMe: false,
            ),
            setNoUnread: setNoUnread,
          ));
        },
        onLongPress: () {
          getChatBottomSheet();
        },
        child: Container(
          /// Determines the height of each chat item
          height: 90,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 18.0, top: 8.0, bottom: 8.0),
                child: Thumbnail(
                  width: 70,
                  height: 70,
                  imageUrl: utils.getProfileThumbnail(_chatItem.targetImages),
                  text: utils.getFullNameByChat(_chatItem),
                  color: utils.getColorByHex(_chatItem.targetColor),
                ),
              ),
              Expanded(
                child: ListTile(
                  title: Row(
                    children: [
                      Text(
                        utils.getFullNameByChat(_chatItem),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: commonTextStyle.merge(
                          TextStyle(
                            color: utils.getColorByHex(_chatItem.targetColor),
                          ),
                        ),
                      ),
                      SizedBox(width: 24.0,),
                      _chatItem.hasUnread ? Icon(
                        MdiIcons.alertCircle,
                        color: utils.getColorByHex(myProfile.color),
                      ) : SizedBox(width: 0, height: 0),
                    ],
                  ),
                  trailing: _chatItem.lastMessage.isNullOrEmpty ? SizedBox(width: 0, height: 0,) : Text(
                    timeUtils.getHourMinuteFromUTCDateTime(_chatItem.updatedAt),
                    style: smallTextStyle,
                  ),
                  subtitle: !_chatItem.lastMessage.isNullOrEmpty ? Padding(
                    padding: EdgeInsets.only(top: 12.0),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Wrap(
                            children: <Widget>[
                              Text(
                                _chatItem.isLastMessageMyself ? "You: ${_chatItem.lastMessage}" : _chatItem.lastMessage,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: TextStyle(
                                  color: _chatItem.hasUnread ? mainColor : unfocusedColor,
                                  fontSize: smallFontSize,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ) : null,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


