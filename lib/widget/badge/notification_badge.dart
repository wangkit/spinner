import 'package:animate_do/animate_do.dart';
import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';

class NotificationBadge extends StatefulWidget {

  final int currentIndex;

  NotificationBadge({
    Key key,
    this.currentIndex,
  }) : super(key: key);

  @override
  NotificationBadgeState createState() => NotificationBadgeState();
}

class NotificationBadgeState extends State<NotificationBadge> {

  int count;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    count = myProfile?.notificationCount ?? 0;
    bool noNewNotification = count == 0 || myProfile == null;
    bool isSelected = widget.currentIndex == 1;

    return Swing(
      child: Badge(
        badgeColor: noNewNotification ? unfocusedColor : utils.getColorByHex(myProfile.color),
        animationType: BadgeAnimationType.scale,
        badgeContent: Text(
          /// The empty space before and after the count string is needed to sync the padding with request badge
          count < 100 ? " " + count.toString() + " " : "99+",
          style: TextStyle(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
        child: Icon(
          isSelected ? Icons.notifications_active : Icons.notifications_outlined,
          color: isSelected && noNewNotification ? utils.getColorByHex(myProfile.color) : mainColor,
        ),
      ),
    );
  }
}