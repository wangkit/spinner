import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/custom_dialog.dart';
import 'package:spinner/view/splash_factory/custom_splash_factory.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';
import 'package:spinner/widget/dialog/negative_button.dart';
import 'package:spinner/widget/dialog/positive_button.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

import 'custom_material_picker.dart';

class ColorPicker extends StatefulWidget {
  final String currentColorCode;
  final Function onPressed;
  final StateSetter customState;

  ColorPicker({
    Key key,
    @required this.onPressed,
    @required this.customState,
    @required this.currentColorCode,
  }) : super(key: key);

  @override
  ColorPickerState createState() => ColorPickerState();
}

class ColorPickerState extends State<ColorPicker> {

  Color pickerColor;
  String currentColorCode;

  @override
  void initState() {
    pickerColor = appIconColor;
    currentColorCode = widget.currentColorCode;
    if (!currentColorCode.isNullOrEmpty) {
      pickerColor = utils.getColorByHex(currentColorCode);
    }
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  void showColorPickerDialog() {
    showModal(
        context: context,
        configuration: FadeScaleTransitionConfiguration(),
        builder: (BuildContext context) {
          return CustomAlertDialog(
            title: Text(
              TextData.pickColor,
              style: TextStyle(
                color: mainColor,
              ),
            ),
            content: SingleChildScrollView(
              child: CustomMaterialPicker(
                pickerColor: pickerColor,
                onColorChanged: changeColor,
              ),
            ),
            actions: <Widget>[
              NegativeButton(text: TextData.cancel,),
              PositiveButton(text: TextData.confirm, onPressed: () {
                widget.onPressed(pickerColor, widget.customState);
              },),
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: CustomBouncingWidget(
        child: Container(
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.0),
            ),
            padding: EdgeInsets.all(1.0),
            color: pickerColor,
            onPressed: () {
              showColorPickerDialog();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  padding: EdgeInsets.all(2.0),
                  icon: Icon(
                    Icons.color_lens,
                    color: utils.getDefaultWhiteOrBlackColor(utils.isDarkColor(utils.getHexCodeByColor(pickerColor))),
                  ),
                  onPressed: null,
                ),
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.only(top: 4.0, bottom: 4.0, left: 4.0, right: 8.0,),
                    child: Text(
                      TextData.pickColor,
                      style: commonTextStyle.merge(
                        TextStyle(
                          color: utils.getDefaultWhiteOrBlackColor(utils.isDarkColor(utils.getHexCodeByColor(pickerColor))),
                        )
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}