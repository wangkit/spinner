import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/view/splash_factory/custom_splash_factory.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';

class ScrollableFooter extends StatefulWidget {
  final List<Widget> children;
  final CrossAxisAlignment crossAxisAlignment;

  ScrollableFooter({
    Key key,
    @required this.children,
    this.crossAxisAlignment = CrossAxisAlignment.center,
  }) : super(key: key);

  @override
  ScrollableFooterState createState() => ScrollableFooterState();
}

class ScrollableFooterState extends State<ScrollableFooter> {

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: constraints.copyWith(
                minHeight: constraints.maxHeight,
                maxHeight: double.infinity,
              ),
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: widget.crossAxisAlignment,
                  children: widget.children,
                ),
              ),
            ),
          );
        }
      );
  }
}