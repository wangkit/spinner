

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';

class GreyDivider extends StatelessWidget {

  final String message;
  final Color color;
  final Function onPressed;
  final Color textColor;

  GreyDivider({this.message = "", this.color, this.onPressed, this.textColor});

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: onPressed != null ? onPressed : null,
      child: Padding(
        padding: EdgeInsets.only(top: 10, bottom: 10),
        child: Container(
            height: 23,
            width: double.infinity,
            decoration: BoxDecoration(
              color: color ?? unfocusedColor,
            ),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 10.0),
                  child: Text(
                    message,
                    style: commonTextStyle.merge(
                      TextStyle(
                        fontWeight: FontWeight.bold,
                        color: textColor ?? mainColor,
                      )
                    ),
                  ),
                )
            )
        ),
      ),
    );
  }
}
