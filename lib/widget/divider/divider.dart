import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';

class GeneralDivider extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Container(
        margin: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Divider(
          color: mainColor,
        )
    );
  }
}
