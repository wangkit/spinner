import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';

class ReviewTitle extends StatefulWidget {
  final String title;

  ReviewTitle({
    Key key,
    @required this.title,
  }) : super(key: key);

  @override
  ReviewTitleState createState() => ReviewTitleState();
}

class ReviewTitleState extends State<ReviewTitle> {

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: Text(
        widget.title,
        style: bigTextStyle.merge(
          TextStyle(
            fontWeight: FontWeight.bold,
          )
        ),
      ),
    );
  }
}