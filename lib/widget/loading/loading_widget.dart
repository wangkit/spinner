import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';

class LoadingWidget extends StatelessWidget {

  LoadingWidget({this.isMargin = false});

  final bool isMargin;

  @override
  Widget build(BuildContext context) {

    double iconSize = 60;

    return Container(
      alignment: Alignment.center,
      margin: isMargin ? EdgeInsets.only(top: 50.0, bottom: 50.0) : EdgeInsets.all(0.0),
      child: ClipOval(
        child: LottieBuilder.asset(
          "assets/lottie/loading.json",
          width: iconSize,
          repeat: true,
          height: iconSize,
        ),
      ),
    );
  }
}
