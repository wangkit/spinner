import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';

class ClearTextFieldButton extends StatefulWidget {
  final TextEditingController controller;

  ClearTextFieldButton({
    Key key,
    @required this.controller,
  }) : super(key: key);

  @override
  ClearTextFieldButtonState createState() => ClearTextFieldButtonState();
}

class ClearTextFieldButtonState extends State<ClearTextFieldButton> {

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.clear,
        color: mainColor,
        size: 16.0,
      ),
      onPressed: () {
        WidgetsBinding.instance.addPostFrameCallback((_) => widget.controller?.clear());
      },
    );
  }
}