import 'package:animate_do/animate_do.dart';
import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/bottom_sheet/sale_bottom_sheet.dart';

class RefreshBadge extends StatefulWidget {
  final int currentIndex;
  final Function onRefreshClicked;
  final bool needVip;

  RefreshBadge({
    Key key,
    this.onRefreshClicked,
    this.currentIndex = 0,
    this.needVip = true,
  }) : super(key: key);

  @override
  RefreshBadgeState createState() => RefreshBadgeState();
}

class RefreshBadgeState extends State<RefreshBadge> {

  bool isRefreshedBtnEnabled = true;
  bool isManuallyDisabledButton = false;

  @override
  void initState() {
    super.initState();
  }

  void disableButton() {
    if (this.mounted && !isManuallyDisabledButton) {
      setState(() {
        isManuallyDisabledButton = true;
      });
    }
  }

  void enableButton() {
    if (this.mounted && isManuallyDisabledButton) {
      setState(() {
        isManuallyDisabledButton = false;
      });
    }
  }

  Widget _mainIcon() {
    return Badge(
      position: BadgePosition.topEnd(top: 5, end: 5),
      badgeColor: myProfile.remainingRefreshCount <= 0 ? unfocusedColor : utils.getColorByHex(myProfile.color),
      animationType: BadgeAnimationType.scale,
      showBadge: widget.needVip,
      badgeContent: Text(
        myProfile.remainingRefreshCount <= 0 ? "0" : myProfile.remainingRefreshCount.toString(),
        style: TextStyle(
          color: Colors.white,
          fontSize: 12.0,
        ),
      ),
      child: IconButton(
        tooltip: TextData.refresh,
        icon: Icon(Icons.refresh, color: isRefreshedBtnEnabled && !isManuallyDisabledButton ? mainColor : unfocusedColor,),
        onPressed: isRefreshedBtnEnabled && !isManuallyDisabledButton ? () {
          if (myProfile.isVip || !widget.needVip) {
            if (isRefreshedBtnEnabled) {
              setState(() {
                isRefreshedBtnEnabled = false;
              });
              widget?.onRefreshClicked();
              _enableBtn();
            }
          } else {
            getSaleBottomSheet();
          }
        } : null,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    if (widget.currentIndex > 2) {
      if (!isManuallyDisabledButton) {
        disableButton();
      }
    } else {
      if (isManuallyDisabledButton) {
        enableButton();
      }
    }

    return isRefreshedBtnEnabled ?
    _mainIcon() : SpinPerfect(
      infinite: true,
      child: _mainIcon(),
    );
  }

  void _enableBtn() {
    Future.delayed(Duration(seconds: 3), () {
      if (this.mounted) {
        setState(() {
          isRefreshedBtnEnabled = true;
        });
      }
    });
  }
}