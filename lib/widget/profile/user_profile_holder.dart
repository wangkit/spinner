import 'dart:async';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:expandable/expandable.dart';
import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:spinner/app/chat/chat_page.dart';
import 'package:spinner/app/main/main_page.dart';
import 'package:spinner/app/preference/view_preference_page.dart';
import 'package:spinner/app/profile/edit_profile_page.dart';
import 'package:spinner/app/profile/verify_profile_page.dart';
import 'package:spinner/app/setup/setup_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/init_dev.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/icon_set/detail_icons.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/remote_config_key.dart';
import 'package:spinner/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:spinner/utils/bottom_sheet/sale_bottom_sheet.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/gender_status.dart';
import 'package:spinner/resources/status/preferred_relationship_status.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/block_dialog.dart';
import 'package:spinner/view/dialogs/greeting_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/view/dialogs/report_dialog.dart';
import 'package:spinner/view/view.dart';
import 'package:spinner/widget/animation/open_container_wrapper.dart';
import 'package:spinner/widget/button/bottom_sheet_button.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/media/profile_media.dart';
import 'package:spinner/widget/media/video/video_download_thumbnail.dart';
import 'package:spinner/widget/tag/tag.dart';

class UserProfileHolder extends StatefulWidget {
  final UserProfile userProfile;
  final UserPreference userPreference;
  final bool needVerification;
  final bool isMyProfile;
  final Function removeProfileFromList;
  final bool isDiscoverPage;
  final bool isViewProfilePage;

  UserProfileHolder({
    Key key,
    @required this.userProfile,
    @required this.userPreference,
    @required this.removeProfileFromList,
    this.needVerification = true,
    this.isMyProfile = false,
    this.isDiscoverPage = false,
    this.isViewProfilePage = false,
  }) : super(key: key);

  @override
  UserProfileHolderState createState() => UserProfileHolderState();
}

class UserProfileHolderState extends State<UserProfileHolder> with AutomaticKeepAliveClientMixin<UserProfileHolder>, TickerProviderStateMixin<UserProfileHolder> {

  double _commonRadius;
  double _size;
  bool _needVerification;
  Color _commonColor;
  bool _isMyProfile;
  AnimationController _hideFabAnimation;
  AnimationController _discoverLikeIconAnimation;
  AnimationController _discoverGreetAnimation;
  Animation<double> _fabAnimation;
  AnimationController _fabAnimationController;
  double _miniFabSize = 40;
  ExpandableController _expandableController;
  double _miniFabIconSize = 20;
  List<String> _quickDisplayList = [];
  List<String> _controlDisplayList = [];
  Widget _fab;
  AnimationController _likeIconController;
  AnimationController _passIconController;
  int _maxListLength = 2;
  bool _isDiscoverPage;
  Map<String, List<dynamic>> _activityReport = {};
  IconData _greetIcon = MdiIcons.hand;

  TextStyle _getStyle({bool isBig = false, bool isSmall = false}) {
    if (isBig) {
      return bigTextStyle.merge(
        TextStyle(
          color: _commonColor,
        )
      );
    }
    if (isSmall) {
      return smallTextStyle.merge(
          TextStyle(
            color: _commonColor,
          )
      );
    }
    return commonTextStyle.merge(
        TextStyle(
          color: _commonColor,
        )
    );
  }

  Widget getDetailWidget(String detailName, bool isMatch) {

    Widget _getIconWidget(String value, dynamic asset) {

      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(width: 20.0,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Text(
                      _isMyProfile ? detailName.normalize + (_quickDisplayList.contains(detailName) ? " (${_quickDisplayList.indexOf(detailName) + 1})" : "") : detailName.normalize,
                      style: smallTextStyle.merge(
                        TextStyle(
                          color: _commonColor,
                          fontWeight: FontWeight.w600,
                          fontFamily: headerFontFamily,
                        )
                      ),
                    ),
                    _isMatch(detailName) ? Padding(
                      padding: EdgeInsets.only(left: 7.0),
                      child: Tooltip(
                        message: TextData.fitYourPreference,
                        showDuration: Duration(
                          seconds: 3,
                        ),
                        child: ElasticIn(
                          child: LottieBuilder.asset(
                            "assets/lottie/fire.json",
                            width: 30,
                            height: 30,
                          ),
                        ),
                      ),
                    ) : Container(),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  value.isNullOrEmpty ? TextData.empty : value,
                  key: Key(value),
                  style: commonTextStyle.merge(
                    TextStyle(
                      color: _commonColor,
                      fontSize: smallFontSize,
                    )
                  ),
                ),
                SizedBox(
                  height: 7,
                ),
              ],
            ),
          ),
          _isMyProfile ? Switch.adaptive(
            activeColor: _commonColor,
            value: _quickDisplayList.contains(detailName),
            onChanged: _quickDisplayList.length == _maxListLength && !_quickDisplayList.contains(detailName) ? null : (bool newValue) {
              if (_quickDisplayList.contains(detailName)) {
                _quickDisplayList.remove(detailName);
              } else {
                if (_quickDisplayList.length == _maxListLength) {
                  _quickDisplayList[1] = _quickDisplayList[0];
                  _quickDisplayList.replaceRange(0, 1, [detailName]);
                }
                if (_quickDisplayList.length < _maxListLength) {
                  _quickDisplayList.add(detailName);
                }
              }
              setFab();
              refreshThisPage();
            },
          ) : Container(),
          SizedBox(width: _isMyProfile ? 16.0 : 0.0,),
        ],
      );
    }

    switch (detailName) {
      case DataKey.name:
        return _getIconWidget(utils.getFullName(widget.userProfile), DetailIcons.name);
      case DataKey.zodiacSign:
        return _getIconWidget(widget.userProfile.zodiacSign, utils.getZodiacIcon(widget.userProfile.zodiacSign));
      case DataKey.distance:
        return _getIconWidget(utils.calculateDistance(myProfile.latitude, myProfile.longitude, widget.userProfile.latitude, widget.userProfile.longitude).toString() + distanceSuffix + " from you", DetailIcons.distance);
      case DataKey.religion:
        return _getIconWidget(widget.userProfile.religion, DetailIcons.religion);
      case DataKey.ethnicity:
        return _getIconWidget(widget.userProfile.ethnicity, DetailIcons.ethnicity);
      case DataKey.education:
        return _getIconWidget(widget.userProfile.education, DetailIcons.education);
      case DataKey.graduatedFrom:
        return _getIconWidget(widget.userProfile.graduatedFrom, DetailIcons.graduatedFrom);
      case DataKey.hobby:
        return _getIconWidget(widget.userProfile.hobby, DetailIcons.hobby);
      case DataKey.isGreenHead:
        return _getIconWidget(widget.userProfile.noLoveExperience ? TextData.noExperience : TextData.hasExperience, DetailIcons.isGreenHead);
      case DataKey.preferredRelationship:
        return _getIconWidget(PreferredRelationshipStatus.relationshipMap[widget.userProfile.preferredRelationship], DetailIcons.preferredRelationship);
      case DataKey.profession:
        return _getIconWidget(widget.userProfile.profession, DetailIcons.profession);
      case DataKey.country:
        return _getIconWidget(widget.userProfile.country, DetailIcons.country);
      case DataKey.city:
        return _getIconWidget(widget.userProfile.city, DetailIcons.city);
      case DataKey.age:
        return _getIconWidget(widget.userProfile.age.toString(), DetailIcons.age);
      case DataKey.personality:
        return _getIconWidget(widget.userProfile.personality, DetailIcons.personality);
      case DataKey.likeCount:
        return _getIconWidget(utils.determineNeedS(widget.userProfile.likedCount, TextData.count), "");
      case DataKey.gender:
        return _getIconWidget(GenderStatus.genderMap[widget.userProfile.gender], DetailIcons.gender);
      default:
        return Container();
    }
  }

  void _setQuickDisplayLists() {
    if (!myProfile.quickDisplayOne.isNullOrEmpty) {
      _quickDisplayList.add(myProfile.quickDisplayOne);
      _controlDisplayList.add(myProfile.quickDisplayOne);
    }
    if (!myProfile.quickDisplayTwo.isNullOrEmpty) {
      _quickDisplayList.add(myProfile.quickDisplayTwo);
      _controlDisplayList.add(myProfile.quickDisplayTwo);
    }
  }

  @override
  void initState() {
    super.initState();
    _fabAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );
    final curvedAnimation = CurvedAnimation(curve: Curves.easeInOut, parent: _fabAnimationController);
    _fabAnimation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);
    _isDiscoverPage = widget.isDiscoverPage;
    _likeIconController = AnimationController(
      vsync: this,
      duration: kThemeAnimationDuration,
    );
    _discoverGreetAnimation = AnimationController(
      vsync: this,
      duration: kThemeAnimationDuration,
    );
    _passIconController = AnimationController(
      vsync: this,
      duration: kThemeAnimationDuration,
    );
    _expandableController = ExpandableController();
    _hideFabAnimation = AnimationController(
      vsync: this,
      duration: kThemeAnimationDuration,
    );
    _discoverLikeIconAnimation = AnimationController(
      vsync: this,
      duration: kThemeAnimationDuration,
    );
    _expandableController.addListener(() {
      _hideFabAnimation.forward();
    });
    _isMyProfile = widget.isMyProfile;
    _needVerification = widget.needVerification;
    _size = 24;
    _commonRadius = commonRadius;
    _commonColor = utils.getDefaultWhiteOrBlackColor(widget.userProfile.isDarkColor);
    _setQuickDisplayLists();
    setFab();
    _activityReport[TextData.lastActive] = [timeUtils.getYearMonthDay(widget.userProfile.lastActiveAt), Icons.timelapse];
    _activityReport[TextData.greetCount] = [widget.userProfile.greetCount.toString(), _greetIcon,];
    _activityReport[TextData.matchCount] = [widget.userProfile.matchCount.toString(), MdiIcons.heartMultiple,];
    _activityReport[TextData.lastLike] = [timeUtils.getYearMonthDay(widget.userProfile.lastLikeTime), MdiIcons.handHeart,];
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.isDiscoverPage) {
        _discoverLikeIconAnimation.forward();
        if (!widget.userProfile.isGreeted) {
          _discoverGreetAnimation.forward();
        }
      } else {
        _hideFabAnimation.forward();
        if (showProfileDetailsDirectly) {
          _expandableController.toggle();
        }
      }
    });
  }

  bool _hasListChange() {
    if (_controlDisplayList.length != _quickDisplayList.length) {
      return true;
    }
    for (int i = 0; i < _controlDisplayList.length; i++) {
      if (_controlDisplayList[i] != _quickDisplayList[i]) return true;
    }
    return false;
  }

  @override
  void dispose() {
    super.dispose();
    _fabAnimationController.dispose();
    if (_discoverLikeIconAnimation != null) {
      _discoverLikeIconAnimation?.dispose();
      _discoverLikeIconAnimation = null;
    }
    if (_likeIconController != null) {
      _likeIconController?.dispose();
      _likeIconController = null;
    }
    if (_expandableController != null) {
      _expandableController?.dispose();
      _expandableController = null;
    }
    if (_discoverGreetAnimation != null) {
      _discoverGreetAnimation?.dispose();
      _discoverGreetAnimation = null;
    }
    if (_passIconController != null) {
      _passIconController?.dispose();
      _passIconController = null;
    }
    if (_hideFabAnimation != null) {
      _hideFabAnimation?.dispose();
      _hideFabAnimation = null;
    }
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  bool _isMatch(String detailName) {
    return utils.isMatch(detailName, myPreference, widget.userProfile) && !_isMyProfile;
  }

  void verifyUser() {
    widget.userProfile.isVerified = true;
    refreshThisPage();
  }

  void robotUser() {
    widget.userProfile.isRobot = true;
    refreshThisPage();
  }

  void likeProfile() async {

    void _commonHandle() {
      widget.userProfile.isPassedOrLiked = true;
      if (widget.removeProfileFromList != null) {
        widget.removeProfileFromList(true);
      } else {
        utils.toast(TextData.liked, isSuccess: true);
      }
      widget.userProfile.isLiked = !widget.userProfile.isLiked;
      if (widget.userProfile.isLiked) {
        widget.userProfile.likedCount++;
      } else {
        widget.userProfile.likedCount--;
      }
    }

    if (!widget.userProfile.isPassedOrLiked) {
      if (widget.isDiscoverPage) {
        if (myProfile.remainingDiscoverCount > 0 || myProfile.isVip) {
          _discoverLikeIconAnimation.reverse();
          myProfile.remainingDiscoverCount--;
          remainingDiscoverCount.value = myProfile.remainingDiscoverCount;
          _commonHandle();
          apiManager.likeOrUnlikeProfile(widget.userProfile.id, isDiscover: widget.isDiscoverPage).then((jsonStatus) {
            if (jsonStatus == StatusCode.exceedDiscoverLike) {
              getSaleBottomSheet(optionalMessage: TextData.unlimitedLikeByVip);
            }
          });
        } else {
          getSaleBottomSheet(optionalMessage: TextData.unlimitedLikeByVip);
        }
      } else {
        apiManager.likeOrUnlikeProfile(widget.userProfile.id, isDiscover: widget.isDiscoverPage);
        _commonHandle();
      }
    }
  }

  void passProfile() {
    if (!widget.userProfile.isPassedOrLiked) {
      apiManager.passProfile(widget.userProfile.id);
      widget.userProfile.isPassedOrLiked = true;
      if (widget.removeProfileFromList != null) {
        widget.removeProfileFromList(false);
      } else {
        utils.toast(TextData.passed, isSuccess: true);
      }
    }
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification.depth == 0) {
      if (notification is UserScrollNotification) {
        final UserScrollNotification userScroll = notification;
        switch (userScroll.direction) {
          case ScrollDirection.forward:
            if (userScroll.metrics.maxScrollExtent != userScroll.metrics.minScrollExtent) {
              _hideFabAnimation.forward();
            }
            break;
          case ScrollDirection.reverse:
            if (userScroll.metrics.maxScrollExtent != userScroll.metrics.minScrollExtent) {
              _hideFabAnimation.reverse();
            }
            break;
          case ScrollDirection.idle:
            break;
        }
      }
    }
    return false;
  }

  Widget _miniFab(IconData iconData, Function onPressed, {Color iconColor}) {
    return FloatingActionButton(
      heroTag: UniqueKey(),
      key: UniqueKey(),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      onPressed: onPressed,
      mini: true,
      backgroundColor: appBgColor,
      child: Icon(
        iconData,
        color: iconColor != null ? iconColor : mainColor,
        size: _miniFabIconSize,
      ),
    );
  }

  Widget _miniOpenContainer(Widget opened, IconData iconData, {bool checkVip = false}) {
    return !checkVip || myProfile.isVip ?  OpenContainerWrapper(
      openedWidget: opened,
      closedElevation: 6.0,
      closedColor: appBgColor,
      tappable: !checkVip || myProfile.isVip,
      closedShape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(28),
        ),
      ),
      closedWidget: _miniFab(iconData, null),
    ) : _miniFab(iconData, () {
      getSaleBottomSheet();
    });
  }

  void setFab() {
    if (!_hasListChange()) {
      _fab = FloatingActionBubble(
        items: <Bubble>[
          Bubble(
            title: TextData.profile,
            iconColor: defaultBlackColor,
            bubbleColor: defaultWhiteColor,
            icon: Icons.account_circle,
            titleStyle: commonTextStyle,
            onPress: () {
              getRoute.navigateTo(EditProfilePage());
              _fabAnimationController.reverse();
            },
          ),
          Bubble(
            title: TextData.media,
            iconColor: defaultBlackColor,
            bubbleColor: defaultWhiteColor,
            icon: Icons.image,
            titleStyle: commonTextStyle,
            onPress: () {
              getRoute.navigateTo(SetupPage(
                editProfile: true,
                notifyParent: refreshThisPage,
              ));
              _fabAnimationController.reverse();
            },
          ),
        ],
        animation: _fabAnimation,
        onPress: () => _fabAnimationController.isCompleted
            ? _fabAnimationController.reverse()
            : _fabAnimationController.forward(),
        iconColor: defaultBlackColor,
        animatedIconData: AnimatedIcons.menu_close,
        backGroundColor: defaultWhiteColor,
      );
    } else {
      _fab = FloatingActionButton.extended(
        key: UniqueKey(),
        heroTag: UniqueKey(),
        onPressed: () {
          if (_quickDisplayList.length == 0) {
            myProfile.quickDisplayOne = null;
            myProfile.quickDisplayTwo = null;
          }
          if (_quickDisplayList.length == 1) {
            myProfile.quickDisplayOne = _quickDisplayList.first;
            myProfile.quickDisplayTwo = null;
          }
          if (_quickDisplayList.length == 2) {
            myProfile.quickDisplayOne = _quickDisplayList.first;
            myProfile.quickDisplayTwo = _quickDisplayList.last;
          }
          _quickDisplayList.clear();
          _controlDisplayList.clear();
          _setQuickDisplayLists();
          apiManager.updateQuickDisplay(myProfile.quickDisplayOne, myProfile.quickDisplayTwo);
          setFab();
          refreshThisPage();
          utils.toast(TextData.updatedDisplay, isSuccess: true);
        },
        label: Text(
          TextData.done,
          style: commonTextStyle.merge(
            TextStyle(
              color: defaultBlackColor,
            )
          ),
        ),
        icon: Icon(
          Icons.done,
          color: defaultBlackColor,
        ),
      );
    }
  }

  void showGreetDialog() {
    getGreetingDialog(widget.userProfile.id, utils.getFullName(widget.userProfile,), () {
      /// On greet
      widget.userProfile.isGreeted = true;
      _discoverGreetAnimation.reverse();
    }, widget.userProfile.blockedMe);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      floatingActionButtonLocation: _isMyProfile || widget.userProfile.isPassedOrLiked ? FloatingActionButtonLocation.endFloat : FloatingActionButtonLocation.centerFloat,
      floatingActionButton: (widget.isViewProfilePage && widget.userProfile.isPassedOrLiked) || _isDiscoverPage ? null : _isMyProfile ? AnimatedSwitcher(
        transitionBuilder: (Widget child, Animation<double> animation) {
          return FadeTransition(
            opacity: animation,
            child: child,
          );
        },
        duration: Duration(seconds: 0),
        child: _fab,
      ) : widget.userProfile.isPassedOrLiked ? FloatingActionButton(
        key: UniqueKey(),
        heroTag: UniqueKey(),
        onPressed: () {
          if (widget.removeProfileFromList != null) {
            widget.removeProfileFromList(false);
          }
        },
        child: Icon(
          Icons.arrow_downward,
          color: mainColor,
        ),
      ) : ScaleTransition(
        scale: _hideFabAnimation,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            myProfile.isVip && !_isMyProfile ? _miniOpenContainer(ChatPage(
              targetProfile: widget.userProfile,
            ), MdiIcons.chat,) : _miniFab(
              _greetIcon,
              () {
                showGreetDialog();
              },
            ),
            SizedBox(width: 12,),
            Container(
              width: 180.0,
              height: 56.0,
              decoration: BoxDecoration(
                color: appBgColor,
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(12.0),
                color: appBgColor,
                elevation: 6.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Flexible(
                      child: InkWell(
                        onTap: () => likeProfile(),
                        highlightColor: Colors.pinkAccent[100],
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          bottomLeft: Radius.circular(12.0),
                        ),
                        onTapDown: (details) {
                          _likeIconController.reverse();
                        },
                        onTapCancel: () {
                          _likeIconController.forward();
                        },
                        child: Container(
                          height: 56,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              JelloIn(
                                controller: (controller) => _likeIconController = controller,
                                manualTrigger: true,
                                child: Icon(
                                  widget.userProfile.likedMe ? MdiIcons.heartPulse : MdiIcons.heart,
                                  color: Colors.pink,
                                  size: 24,
                                ),
                              ),
                              Text(
                                TextData.like,
                                style: smallTextStyle,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    VerticalDivider(
                      color: unfocusedColor,
                      width: 1,
                    ),
                    Flexible(
                      child: InkWell(
                        onTap: () => passProfile(),
                        highlightColor: Colors.lightBlueAccent[100],
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(12.0),
                          bottomRight: Radius.circular(12.0),
                        ),
                        onTapDown: (details) {
                          _passIconController.reverse();
                        },
                        onTapCancel: () {
                          _passIconController.forward();
                        },
                        child: Container(
                          height: 56,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              JelloIn(
                                controller: (controller) => _passIconController = controller,
                                manualTrigger: true,
                                child: Icon(
                                  Icons.close,
                                  color: Colors.lightBlue,
                                  size: 24,
                                ),
                              ),
                              Text(
                                TextData.pass,
                                style: smallTextStyle,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(width: 12,),
            !widget.userProfile.isVerified && _needVerification && !_isMyProfile && widget.userProfile.verificationImage != placeholderLink && !widget.userProfile.verificationImage.isNullOrEmpty ? _miniOpenContainer(VerifyProfilePage(
              userProfile: widget.userProfile,
              userPreference: widget.userPreference,
              verifyUser: verifyUser,
              robotUser: robotUser,
              likeProfile: likeProfile,
            ), MdiIcons.accountQuestion,
            ) : !_isMyProfile && widget.userPreference != null ? _miniOpenContainer(ViewPreferencePage(
              userProfile: widget.userProfile,
              userPreference: widget.userPreference,
              isMyPreference: _isMyProfile,
            ), MdiIcons.alphaRBox, checkVip: true,
            ) : widget.userProfile.isVerified && !_isMyProfile ? _miniFab(Icons.verified_user, null, iconColor: success)
                : widget.userProfile.isRobot && !_isMyProfile ? _miniFab(MdiIcons.robot, null, iconColor: errorColor)
                : SizedBox(width: _miniFabSize, height: _miniFabSize,),
          ],
        ),
      ),
      body: widget.userProfile.isPassedOrLiked && !widget.isViewProfilePage && !widget.isDiscoverPage ? Center(
        child: Padding(
          padding: EdgeInsets.all(24.0),
          child: Text(
            TextData.alreadyLikedOrPassed,
            /// Don't use getStyle here
            style: commonTextStyle,
          ),
        ),
      ) : widget.isDiscoverPage ? Card(
        elevation: 6.0,
        color: utils.getColorByHex(widget.userProfile.color),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(commonRadius)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ProfileMedia(
              key: Key(widget.userProfile.images.toString()),
              userProfile: widget.userProfile,
              shouldAutoPlay: false,
            ),
            SizedBox(
              height: 10,
            ),
            Flexible(
              child: InkWell(
                borderRadius: BorderRadius.circular(commonRadius),
                onTap: () {
                  getGenericBottomSheet([
                    BottomSheetButton(
                      label: TextData.blockUser,
                      iconData: Icons.block,
                      onTap: () => getBlockDialog(widget.userProfile.id, utils.getFullName(widget.userProfile), utils.getChatId([widget.userProfile.id, myProfile.id]), null),
                    ),
                    BottomSheetButton(
                      label: TextData.report,
                      iconData: Icons.report,
                      onTap: () => getReportDialog(widget.userProfile.id, utils.getFullName(widget.userProfile), null),
                    ),
                  ]);
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 14.0),
                  child: ListTile(
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ScaleTransition(
                          scale: _discoverLikeIconAnimation,
                          child: FloatingActionButton(
                            key: UniqueKey(),
                            heroTag: UniqueKey(),
                            elevation: 0,
                            focusElevation: 0,
                            highlightElevation: 0,
                            hoverElevation: 0,
                            splashColor: Colors.pinkAccent[100],
                            onPressed: () => likeProfile(),
                            child: Icon(
                              MdiIcons.heart,
                              color: Colors.pink,
                              size: 32,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        ScaleTransition(
                          scale: _discoverGreetAnimation,
                          child: FloatingActionButton(
                            key: UniqueKey(),
                            heroTag: UniqueKey(),
                            elevation: 0,
                            focusElevation: 0,
                            highlightElevation: 0,
                            hoverElevation: 0,
                            splashColor: likeIconSplash,
                            onPressed: () => showGreetDialog(),
                            child: Icon(
                              _greetIcon,
                              color: likeIconGlow,
                              size: 32,
                            ),
                          ),
                        ),
                      ],
                    ),
                    title: Text(
                      "${utils.getProfileValue(widget.userProfile.quickDisplayOne, widget.userProfile, 0)}, ${utils.getProfileValue(widget.userProfile.quickDisplayTwo, widget.userProfile, 1)}",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: commonTextStyle.merge(
                        TextStyle(
                          fontWeight: FontWeight.w600,
                          color: _commonColor,
                        )
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ) : Container(
        color: utils.getColorByHex(widget.userProfile.color),
        child: NotificationListener<ScrollNotification>(
          onNotification: _handleScrollNotification,
          child: ListView(
            children: [
              Stack(
                children: [
                  ProfileMedia(
                    userProfile: widget.userProfile,
                    shouldAutoPlay: false,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 12.0, bottom: 8.0,),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        widget.userProfile.isVip ? Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8.0,),
                          child: Icon(
                            MdiIcons.crown,
                            color: utils.getColorByHex(widget.userProfile.color),
                          ),
                        ) : Container(),
                      ],
                    ),
                  ),
                ],
              ),
              _isDiscoverPage ? SizedBox(
                width: 0,
                height: 0,
              ) : ExpandableNotifier(
                controller: _expandableController,
                child: Expandable(
                  collapsed: ExpandableButton(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 18.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(
                                  "${utils.getProfileValue(widget.userProfile.quickDisplayOne, widget.userProfile, 0)}, ${utils.getProfileValue(widget.userProfile.quickDisplayTwo, widget.userProfile, 1)}",
                                  key: Key(utils.getProfileValue(widget.userProfile.quickDisplayOne, widget.userProfile, 0) + utils.getProfileValue(widget.userProfile.quickDisplayTwo, widget.userProfile, 1)),
                                  style: commonTextStyle.merge(
                                    TextStyle(
                                      color: _commonColor,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: headerFontFamily,
                                    )
                                  ),
                                ),
                              ),
                              Tooltip(
                                message: TextData.clickMeToShow,
                                showDuration: Duration(
                                  seconds: 3,
                                ),
                                child: Icon(
                                  Icons.info_rounded,
                                  color: _commonColor,
                                  size: 20.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 3.0),
                          child: RichText(
                            text: TextSpan(
                              text: widget.userProfile.personality.isNullOrEmpty ? remoteConfig.getString(RemoteConfigKey.newUserPersonality) : widget.userProfile.personality,
                              style: _getStyle(isSmall: true),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          width: deviceWidth,
                          height: deviceWidth / 2.5,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 3.0),
                            child: GridView.builder(
                              itemCount: _activityReport.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: 2.5,
                              ),
                              itemBuilder: (ctx, index) {
                                return Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                      child: Text(
                                        _activityReport.keys.toList()[index],
                                        style: _getStyle(isSmall: true),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 7,
                                    ),
                                    Flexible(
                                      child: Row(
                                        children: [
                                          Icon(
                                            _activityReport.values.toList()[index][1],
                                            color: _commonColor,
                                            size: 20,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Flexible(
                                            child: Text(
                                              myProfile.isVip ? _activityReport.values.toList()[index][0] : TextData.locked,
                                              style: _getStyle(isSmall: true),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        ),
                        !myProfile.isVip ? Center(
                          child: LongElevatedButton(
                            height: 45,
                            width: 100,
                            textColor: defaultWhiteColor,
                            color: unfocusedColor,
                            title: TextData.unlock,
                            onPressed: () {
                              getSaleBottomSheet();
                            },
                          ),
                        ) : Container(),
                        SizedBox(
                          height: 20,
                        ),
                        !_isMyProfile ? Column(
                          children: [
                            _blockDivider(),
                            _blockAndReport(false),
                            _blockDivider(),
                            _blockAndReport(true),
                            _blockDivider(),
                          ],
                        ) : Container(),
                        SizedBox(
                          height: (deviceHeight - kToolbarHeight - kBottomNavigationBarHeight - deviceWidth) * 0.2,
                        ),
                      ],
                    ),
                  ),
                  expanded: ExpandableButton(
                    child: Padding(
                      padding: EdgeInsets.only(bottom: _isMyProfile ? 50.0 : 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: List.generate(profileDetailsOrderingList.length, (i) {
                          return Padding(
                            padding: EdgeInsets.symmetric(vertical: 16),
                            child: getDetailWidget(
                              profileDetailsOrderingList[i],
                              _isMatch(profileDetailsOrderingList[i]),
                            ),
                          );
                        }),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _blockDivider() {
    return Divider(
      color: _commonColor,
    );
  }

  Widget _blockAndReport(bool isBlock) {
    return Material(
      color: utils.getColorByHex(widget.userProfile.color),
      child: InkWell(
        onTap: () {
          if (isBlock) {
            getBlockDialog(widget.userProfile.id, utils.getFullName(widget.userProfile), utils.getChatId([widget.userProfile.id, myProfile.id]), () {
              passProfile();
            });
          } else {
            getReportDialog(widget.userProfile.id, utils.getFullName(widget.userProfile), () {
              passProfile();
            });
          }
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              onPressed: null,
              child: Text(
                isBlock ? TextData.block : TextData.report,
                style: commonTextStyle.merge(
                    TextStyle(
                      color: _commonColor,
                    )
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
