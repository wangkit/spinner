import 'dart:async';
import 'dart:io';
import 'package:confetti/confetti.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/rendering.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:flutter/material.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/widget/profile/user_profile_holder.dart';

class ProfileList extends StatefulWidget {

  final List<UserProfile> userProfiles;
  final List<UserPreference> userPreferences;
  final Function getIsProfileEmpty;
  final bool isDiscoverPage;
  
  ProfileList({
    Key key,
    @required this.userProfiles,
    @required this.userPreferences,
    @required this.getIsProfileEmpty,
    this.isDiscoverPage = false,
  }) : super(key: key);

  @override
  ProfileListState createState() => ProfileListState();
}

class ProfileListState extends State<ProfileList> with AutomaticKeepAliveClientMixin<ProfileList>, TickerProviderStateMixin {
  
  PageController _pageController;
  ConfettiController _controllerCenter;

  @override
  void initState() {
    _controllerCenter = ConfettiController(duration: Duration(seconds: 1));
    _pageController = PageController();
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
  
  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _item(int index) {
    return UserProfileHolder(
      key: UserProfile().getProfileWidgetKey(widget.userProfiles[index]),
      isDiscoverPage: widget.isDiscoverPage,
      userProfile: widget.userProfiles[index],
      userPreference: _findCorrespondingPreference(widget.userProfiles[index]),
      removeProfileFromList: (bool isLiked) {
        if (!widget.isDiscoverPage) {
          if (widget.userProfiles.length > index + 1) {
            _pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
          } else {
            widget.getIsProfileEmpty(index == widget.userProfiles.length - 1);
          }
        }
        if (isLiked) {
          _controllerCenter?.play();
        }
        widget.userProfiles[index].isPassedOrLiked = true;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [
        widget.isDiscoverPage ? ListView.separated(
          padding: EdgeInsets.symmetric(horizontal: 3, vertical: 5),
          separatorBuilder: (ctx, index) => Divider(
            color: transparent,
          ),
          itemCount: widget.userProfiles.length,
          itemBuilder: (ctx, index) {
            return Container(
              height: utils.getProfileMediaHeight() + kToolbarHeight + kToolbarHeight,
              child: _item(index),
            );
          }
        ) : PageView.builder(
          itemCount: widget.userProfiles.length,
          controller: _pageController,
          scrollDirection: Axis.vertical,
          pageSnapping: !widget.isDiscoverPage,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (ctx, index) {
            return _item(index);
          },
        ),
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            emissionFrequency: 0.2,
            confettiController: _controllerCenter,
            blastDirectionality: BlastDirectionality.explosive,
            shouldLoop: false,
            colors: [
              Colors.green,
              Colors.blue,
              Colors.pinkAccent,
              Colors.tealAccent,
              Colors.deepPurpleAccent,
              Colors.lightGreenAccent,
              Colors.lightBlueAccent,
              Colors.amberAccent,
            ], // manually specify the colors to be used
          ),
        ),
      ],
    );
  }

  UserPreference _findCorrespondingPreference(UserProfile profile) {
    for (int i = 0; i < widget.userPreferences.length; i++) {
      if (widget.userPreferences[i].id == profile.id) {
        return widget.userPreferences[i];
      }
    }
    return null;
  }

  @override
  bool get wantKeepAlive => true;
}
