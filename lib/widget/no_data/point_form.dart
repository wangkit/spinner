import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';

class PointForm extends StatefulWidget {

  final Widget icon;
  final String title;

  PointForm({
    Key key,
    this.icon,
    this.title,
  }) : super(key: key);

  @override
  _PointFormState createState() => _PointFormState();
}

class _PointFormState extends State<PointForm> {

  @override
  Widget build(BuildContext context) {

    return ListTile(
      leading: BounceInLeft(
        child: widget.icon,
      ),
      title: Text(
        widget.title,
        style: TextStyle(
          color: mainColor,
        ),
      ),
    );
  }
}