import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:spinner/utils/bottom_sheet/sale_bottom_sheet.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/button/toggle_button/custom_toggle_buttons.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/media/image/image_holder.dart';
import 'package:spinner/widget/no_data/point_form.dart';
import 'package:spinner/widget/smart_text/smart_text.dart';
import 'package:spinner/widget/tag/tag.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

class PromptSubscribe extends StatefulWidget {

  PromptSubscribe({
    Key key,
  }) : super(key: key);

  @override
  _PromptSubscribeState createState() => _PromptSubscribeState();
}

class _PromptSubscribeState extends State<PromptSubscribe> {

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/icon/love_birds.png",
                  width: deviceWidth * 2 / 3,
                  height: deviceWidth * 2 / 3,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 10.0, bottom: 10.0),
                  child: Text(
                    TextData.dontWorryProfile,
                    textAlign: TextAlign.center,
                    style: commonTextStyle.merge(
                      TextStyle(
                        fontWeight: FontWeight.w600,
                      )
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 18.0, bottom: 0.0),
                  child: Text(
                    TextData.becomeVip,
                    textAlign: TextAlign.center,
                    style: smallTextStyle,
                  ),
                ),
              ],
            ),
          ),
          JelloIn(
            child: LongElevatedButton(
              title: TextData.learnMore,
              elevation: 0.0,
              width: deviceWidth * 0.95,
              onPressed: () {
                getSaleBottomSheet();
              },
              isReverseColor: false,
            ),
          ),
          SizedBox(height: 30,),
        ],
      ),
    );
  }
}
