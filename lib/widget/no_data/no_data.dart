import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';

class NoData extends StatefulWidget {
  final String text;
  final String asset;

  NoData({
    Key key,
    @required this.text,
    @required this.asset,
  }) : super(key: key);

  @override
  _NoDataState createState() => _NoDataState();
}

class _NoDataState extends State<NoData> {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            widget.asset,
            width: deviceWidth * 0.6,
            height: deviceWidth * 0.6,
          ),
          Padding(
            padding: EdgeInsets.all(24.0),
            child: Text(
              widget.text,
              style: commonTextStyle,
            ),
          ),
        ],
      ),
    );
  }
}
