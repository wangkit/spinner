import 'dart:async';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:spinner/app/profile/verify_profile_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/icon_set/detail_icons.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/gender_status.dart';
import 'package:spinner/resources/status/preferred_relationship_status.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/media/image/image_holder.dart';
import 'package:spinner/widget/media/video/video_download_thumbnail.dart';

// ignore: must_be_immutable
class UserPreferenceHolder extends StatefulWidget {
  final UserPreference userPreference;
  final UserProfile userProfile;
  StrictPreference userStrictPreference;
  final bool isMyPreference;
  final ScrollController scrollController;
  final Function setExtended;

  UserPreferenceHolder({
    Key key,
    @required this.userPreference,
    @required this.userProfile,
    @required this.userStrictPreference,
    this.isMyPreference = false,
    this.setExtended,
    this.scrollController,
  }) : super(key: key);

  @override
  UserPreferenceHolderState createState() => UserPreferenceHolderState();
}

class UserPreferenceHolderState extends State<UserPreferenceHolder> with AutomaticKeepAliveClientMixin<UserPreferenceHolder> {

  UserPreference _thisPreference;
  UserProfile _thisProfile;
  bool _isMyPreference;
  Color _commonColor;

  @override
  void initState() {
    _isMyPreference = widget.isMyPreference;
    _thisProfile = widget.userProfile;
    _thisPreference = widget.userPreference;
    _commonColor = utils.getDefaultWhiteOrBlackColor(_thisProfile.isDarkColor);
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  bool _getStrictPreference(String detailName) {
    switch (detailName) {
      case DataKey.zodiacSign:
        return widget.userStrictPreference.zodiacSign;
      case DataKey.distance:
        return widget.userStrictPreference.distance;
      case DataKey.religion:
        return widget.userStrictPreference.religion;
      case DataKey.ethnicity:
        return widget.userStrictPreference.ethnicity;
      case DataKey.education:
        return widget.userStrictPreference.education;
      case DataKey.hobby:
        return widget.userStrictPreference.hobby;
      case DataKey.isGreenHead:
        return widget.userStrictPreference.isGreenHead;
      case DataKey.preferredRelationship:
        return widget.userStrictPreference.preferredRelationship;
      case DataKey.age:
        return widget.userStrictPreference.age;
      case DataKey.likeCount:
        return widget.userStrictPreference.likeCounts;
      case DataKey.gender:
        return widget.userStrictPreference.gender;
      default:
        return false;
    }
  }

  bool _isMatch(String detailName) {
    return utils.isMatch(detailName, _thisPreference, myProfile, isUsingMyPreference: false, targetProfile: _thisProfile) && !_isMyPreference;
  }

  bool _isSameStrictPreference() {
    return widget.userStrictPreference.zodiacSign == myStrictPreference.zodiacSign &&
    widget.userStrictPreference.distance == myStrictPreference.distance &&
    widget.userStrictPreference.religion == myStrictPreference.religion &&
    widget.userStrictPreference.ethnicity == myStrictPreference.ethnicity &&
    widget.userStrictPreference.education == myStrictPreference.education &&
    widget.userStrictPreference.hobby == myStrictPreference.hobby &&
    widget.userStrictPreference.isGreenHead == myStrictPreference.isGreenHead &&
    widget.userStrictPreference.preferredRelationship == myStrictPreference.preferredRelationship &&
    widget.userStrictPreference.age == myStrictPreference.age &&
    widget.userStrictPreference.likeCounts == myStrictPreference.likeCounts &&
    widget.userStrictPreference.gender == myStrictPreference.gender;
  }

  Widget _preferenceWidget(String detailName) {

    Widget _getIconWidget(String value, IconData icon) {
      return !_isMyPreference ? ListTile(
        trailing: _isMatch(detailName) ? Text(
          TextData.match,
          style: smallTextStyle.merge(
              TextStyle(
                color: success,
              )
          ),
        ) : SizedBox(width: 0, height: 0,),
        title: Text(
          detailName.normalize,
          style: smallTextStyle.merge(
              TextStyle(
                color: _commonColor,
                fontWeight: FontWeight.w600,
              )
          ),
        ),
        subtitle: Text(
          value.isNullOrEmpty ? TextData.noPreference : value,
          style: smallTextStyle.merge(
              TextStyle(
                color: _commonColor,
              )
          ),
        ),
      ) : SwitchListTile.adaptive(
        activeColor: _commonColor,
        value: _getStrictPreference(detailName),
        onChanged: (bool newValue) {
          switch (detailName) {
            case DataKey.zodiacSign:
              widget.userStrictPreference.zodiacSign = newValue;
              break;
            case DataKey.distance:
              widget.userStrictPreference.distance = newValue;
              break;
            case DataKey.religion:
              widget.userStrictPreference.religion = newValue;
              break;
            case DataKey.ethnicity:
              widget.userStrictPreference.ethnicity = newValue;
              break;
            case DataKey.education:
              widget.userStrictPreference.education = newValue;
              break;
            case DataKey.hobby:
              widget.userStrictPreference.hobby = newValue;
              break;
            case DataKey.isGreenHead:
              widget.userStrictPreference.isGreenHead = newValue;
              break;
            case DataKey.preferredRelationship:
              widget.userStrictPreference.preferredRelationship = newValue;
              break;
            case DataKey.age:
              widget.userStrictPreference.age = newValue;
              break;
            case DataKey.likeCount:
              widget.userStrictPreference.likeCounts = newValue;
              break;
            case DataKey.gender:
              widget.userStrictPreference.gender = newValue;
              break;
          }
          widget.setExtended(_isSameStrictPreference());
          refreshThisPage();
        },
        title: Text(
          detailName.normalize,
          style: smallTextStyle.merge(
              TextStyle(
                color: _commonColor,
                fontWeight: FontWeight.w600,
              )
          ),
        ),
        subtitle: Text(
          value.isNullOrEmpty ? TextData.noPreference : value,
          style: smallTextStyle.merge(
              TextStyle(
                color: _commonColor,
              )
          ),
        ),
      );
    }

    String _getPrefGenderString() {
      String _prefGender = "";
      _thisPreference.gender.forEach((g) {
        _prefGender += ("${GenderStatus.genderMap[g]}, ");
      });
      return _prefGender.isNullOrEmpty ? _prefGender : _prefGender.substring(0, _prefGender.length - 2);
    }

    switch (detailName) {
      case DataKey.zodiacSign:
        return _getIconWidget(_thisPreference.zodiacSign.toString().normalizeList, DetailIcons.zodiacSign);
      case DataKey.distance:
        return _getIconWidget(_thisPreference.distance.round().toString() + distanceSuffix, DetailIcons.distance);
      case DataKey.religion:
        return _getIconWidget(_thisPreference.religion.toString().normalizeList, DetailIcons.religion);
      case DataKey.ethnicity:
        return _getIconWidget(_thisPreference.ethnicity.toString().normalizeList, DetailIcons.ethnicity);
      case DataKey.education:
        return _getIconWidget(_thisPreference.education.toString().normalizeList, DetailIcons.education);
      case DataKey.hobby:
        return _getIconWidget(_thisPreference.hobby.toString().normalizeList, DetailIcons.hobby);
      case DataKey.isGreenHead:
        return _getIconWidget(_thisPreference?.isGreenHead == null ? TextData.noPreference : _thisPreference.isGreenHead ? TextData.prefNoExperience : TextData.prefHasExperience, DetailIcons.isGreenHead);
      case DataKey.preferredRelationship:
        return _getIconWidget(PreferredRelationshipStatus.relationshipMap[_thisPreference.preferredRelationship], DetailIcons.preferredRelationship);
      case DataKey.age:
        return _getIconWidget(_thisPreference.age.first.toString() + " - " + _thisPreference.age.last.toString(), DetailIcons.age);
      case DataKey.likeCount:
        return _getIconWidget(_thisPreference.likeCounts.first.toString() + " - " + _thisPreference.likeCounts.last.toString(), DetailIcons.likeCount);
      case DataKey.gender:
        return _getIconWidget(_getPrefGenderString(), DetailIcons.gender);
      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: utils.getColorByHex(_thisProfile.color),
      child: ListView.builder(
        controller: widget.scrollController,
        scrollDirection: Axis.vertical,
        itemCount: preferenceDetailsOrderingList.length,
        itemBuilder: (ctx, index) {
          return _isMyPreference && index == 0 ? Column(
            children: [
              Padding(
                padding: EdgeInsets.all(18.0),
                child: Text(
                  TextData.strictPreferenceExplanation,
                  style: bigTextStyle.merge(
                      TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: bigFontSize,
                        color: _commonColor,
                      )
                  ),
                ),
              ),
              _preferenceWidget(preferenceDetailsOrderingList[index]),
            ],
          ) : (index == preferenceDetailsOrderingList.length - 1) && _isMyPreference ? Column(
            children: [
              _preferenceWidget(preferenceDetailsOrderingList[index]),
              SizedBox(
                height: 80,
              ),
            ],
          ) : _preferenceWidget(preferenceDetailsOrderingList[index]);
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
