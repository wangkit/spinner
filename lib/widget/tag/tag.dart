import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';

class Tag extends StatefulWidget {
  final String text;
  final String colorCode;
  final Function onPressed;
  final Icon icon;
  final Icon leadingIcon;
  final double fontSize;
  final Color textColor;
  final double margin;
  final double padding;
  final double elevation;
  final double radius;
  final Widget child;

  Tag({
    Key key,
    this.text,
    this.colorCode,
    this.onPressed,
    this.leadingIcon,
    this.radius = 0,
    this.fontSize,
    this.child,
    this.elevation = 1,
    this.textColor,
    this.margin = 0,
    this.padding = 0,
    this.icon,
  }) : super(key: key);

  @override
  TagState createState() => TagState();
}

class TagState extends State<Tag> {

  String text;
  String colorCode;
  double padding;
  double fontSize;
  double margin;
  double radius;
  double elevation;
  Icon icon;
  Icon leadingIcon;

  @override
  void initState() {
    leadingIcon = widget.leadingIcon;
    radius = widget.radius;
    elevation = widget.elevation;
    padding = widget.padding;
    margin = widget.margin;
    fontSize = widget.fontSize;
    text = widget.text;
    icon = widget.icon;
    colorCode = widget.colorCode;
    if (radius == 0) radius = commonRadius;
    if (padding == 0) padding = null;
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomBouncingWidget(
      enabledBouncing: widget.onPressed != null,
      child: GestureDetector(
        onTap: widget.onPressed,
        child: Bubble(
          margin: BubbleEdges.all(margin),
          padding: BubbleEdges.all(padding),
          radius: Radius.circular(radius),
          elevation: elevation,
          color: utils.getColorByHex(colorCode) ?? utils.getVisibleColor(myProfile.color),
          child: widget.child != null ? widget.child : Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              leadingIcon == null ? Container() : Padding(
                padding: EdgeInsets.only(right: 2.0,),
                child: leadingIcon,
              ),
              text != null ? Text(
                text,
                style: fontSize != null || widget.textColor != null ? commonTextStyle.merge(
                    TextStyle(
                      fontSize: fontSize,
                      color: widget.textColor,
                    )
                ) : commonTextStyle,
              ) : Container(),
              icon == null ? Container() : Padding(
                padding: EdgeInsets.only(left: 2.0),
                child: icon,
              ),
            ],
          ),
        ),
      ),
    );
  }
}