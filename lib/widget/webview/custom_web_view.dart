import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/decoration.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomWebView extends StatefulWidget {
  final String link;

  CustomWebView({Key key, this.link}) : super(key: key);

  @override
  _CustomWebViewState createState() => _CustomWebViewState();
}

class _CustomWebViewState extends State<CustomWebView> {

  WebViewController _webViewController;
  String link;

  String _getAppbarTitle() {
    var appbarTitle = widget.link.split("://").last;
    if (appbarTitle.contains("www.")) {
      appbarTitle = appbarTitle.replaceFirst("www.", "");
      if (appbarTitle.contains(".")) {
        appbarTitle = appbarTitle.split(".").first;
      }
    }
    return appbarTitle;
  }

  JavascriptMode _getJsMode() {
    if (_getAppbarTitle() == "latimes") {
      return JavascriptMode.disabled;
    } else {
      return JavascriptMode.unrestricted;
    }
  }

  @override initState() {
    link = widget.link;
    if (link.startsWith("www.")) link = "https://" + link;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        accentColor: appTheme,
      ),
      child: Container(
        decoration: getBackgroundBox(),
        child: WillPopScope(
          onWillPop: () async {
            return true;
          },
          child: Scaffold(
            appBar: MyAppBar(
                actions: [
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: IconButton(
                      tooltip: TextData.copyLink,
                      icon: Icon(Icons.refresh, color: mainColor,),
                      onPressed: () {
                        _webViewController.reload();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: IconButton(
                      tooltip: TextData.copyLink,
                      icon: Icon(Icons.content_copy, color: mainColor,),
                      onPressed: () {
                        Clipboard.setData(ClipboardData(text: widget.link));
                        utils.toast(TextData.copiedSuccessfully, bgColor: success, iconData: successIcon);
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: IconButton(
                      tooltip: TextData.openInBrowser,
                      icon: Icon(Icons.open_in_browser, color: mainColor,),
                      onPressed: () async {
                        await utils.launchURL(widget.link);
                      },
                    ),
                  ),
                ]
            ),
            backgroundColor: transparent,
            body: WebView(
              onWebViewCreated: (controller) => _webViewController = controller,
              initialUrl: link,
              javascriptMode: _getJsMode(),
            ),
          ),
        ),
      ),
    );
  }
}