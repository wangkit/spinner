import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/widget/smart_text/smart_text.dart';
import 'package:spinner/widget/webview/custom_web_view.dart';
import 'package:url_launcher/url_launcher.dart';

/// To display text in post title, reply content with the tagged user name colored and clickable
class TagNameDisplay extends StatefulWidget {
  final String text;
  final bool isPostTitleStyle;
  final TextStyle style;
  final int maxLines;
  final bool isOverflow;
  final bool isReplyContent;

  TagNameDisplay({Key key, this.isReplyContent = false, this.isOverflow = false, @required this.text, this.isPostTitleStyle = false, this.maxLines, this.style}) : super(key: key);

  @override
  _TagNameDisplayState createState() => _TagNameDisplayState();
}

class _TagNameDisplayState extends State<TagNameDisplay> with AutomaticKeepAliveClientMixin<TagNameDisplay> {
  String text;
  List<String> splitForUser;
  List<String> splitForCorner;
  List<String> hashTags;
  List<String> doubleStrokes;
  List<UserProfile> _userData = [];
  Future<void> _loading;
  bool isPostTitle;
  int maxLines;
  TextStyle style;
  bool isReplyContent;
  bool isOverflow;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    isReplyContent = widget.isReplyContent;
    isOverflow = widget.isOverflow;
    style = widget.style;
    isPostTitle = widget.isPostTitleStyle;
    text = widget.text;
    maxLines = widget.maxLines;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SmartText(
      isOverflow: isOverflow,
      isPostTitle: isPostTitle,
      style: style ?? utils.getNameTextStyle(mainColor, isPostTitle),
      text: text,
      maxLines: maxLines,
      onUrlClick: (url) {
        getRoute.navigateTo(
            CustomWebView(link: url,)
        );
      },
      onNormalTagClick: (tag) {
        debugPrint("Normal click");
      },
      onUserTagClick: (tag) {
        debugPrint("User click");
      },
    );
  }
}