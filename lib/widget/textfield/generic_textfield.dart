import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/button/clear_text_field.dart';
import 'package:validators/validators.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

class GenericTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final String hint;
  final String helper;
  final bool canEmpty;
  final bool canSpace;
  final TextInputType textInputType;
  final List<String> autofillHints;
  final int maxLines;
  final int maxLength;
  final Function onSubmitted;
  final Function validation;
  final List<TextInputFormatter> inputFormatter;
  final TextCapitalization textCapitalization;
  final bool showLabel;
  final String label;

  GenericTextField({
    Key key, 
    @required this.controller, 
    @required this.focusNode,
    this.validation,
    this.autofillHints,
    this.textCapitalization = TextCapitalization.sentences,
    this.maxLines,
    this.maxLength,
    this.canSpace = true,
    this.inputFormatter,
    this.textInputType,
    this.nextFocusNode, 
    this.hint,
    this.onSubmitted,
    this.helper, 
    this.canEmpty = false,
    this.showLabel = false,
    this.label = "",
  }) : super(key: key);

  @override
  _GenericTextFieldState createState() => _GenericTextFieldState();
}

class _GenericTextFieldState extends State<GenericTextField> {

  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;
  String hint;
  String helper;
  bool canEmpty;
  bool isVisible;
  bool canSpace;
  TextInputType textInputType;
  List<String> autofillHints;
  int maxLines;
  int maxLength;

  @override
  void initState() {
    isVisible = false;
    maxLength = widget.maxLength;
    maxLines = widget.maxLines;
    canSpace = widget.canSpace;
    autofillHints = widget.autofillHints;
    textInputType = widget.textInputType;
    hint = widget.hint;
    focusNode = widget.focusNode;
    focusNode?.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    nextFocusNode = widget.nextFocusNode;
    controller = widget.controller;
    helper = widget.helper;
    canEmpty = widget.canEmpty;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: TextFormField(
        focusNode: focusNode,
        inputFormatters: widget.inputFormatter,
        style: commonTextStyle,
        onFieldSubmitted: widget.onSubmitted ?? (term) {
          if (focusNode != null && nextFocusNode != null) {
            utils.fieldFocusChange(focusNode, nextFocusNode);
          }
        },
        autofillHints: autofillHints,
        controller: controller,
        validator: widget.validation ?? (value) {
          if (value.isNullOrEmpty && !canEmpty) {
            return "${hint.capitalize} must not be empty";
          }
          return null;
        },
        textCapitalization: widget.textCapitalization,
        keyboardType: textInputType,
        maxLines: maxLines,
        maxLength: maxLength ?? 150,
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
          helperText: helper,
          counterText: maxLength != null ? null : "",
          helperMaxLines: 3,
          hintMaxLines: 3,
          errorMaxLines: 3,
          labelText: widget.showLabel ? widget.label.length == 0 ? hint : widget.label : null,
          labelStyle: TextStyle(
            color: unfocusedColor,
          ),
          helperStyle: TextStyle(
            color: mainColor,
          ),
          hintText: hint,
          hintStyle: TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: textFieldOpacityAnimationDuration,
            child: ClearTextFieldButton(controller: widget.controller),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
        ),
      ),
    );
  }
}