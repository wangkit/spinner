import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/widget/button/clear_text_field.dart';

class LinkTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final bool canEmpty;
  final bool isNewContent;

  LinkTextField({Key key, @required this.isNewContent, @required this.controller, this.focusNode, this.nextFocusNode, this.canEmpty = true}) : super(key: key);

  @override
  _LinkTextFieldState createState() => _LinkTextFieldState();
}

class _LinkTextFieldState extends State<LinkTextField> {

  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;
  bool canEmpty;
  bool isNewContent;
  bool isVisible;

  @override
  void initState() {
    isVisible = false;
    isNewContent = widget.isNewContent;
    focusNode = widget.focusNode;
    nextFocusNode = widget.nextFocusNode;
    controller = widget.controller;
    canEmpty = widget.canEmpty;
    focusNode.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      style: commonTextStyle,
      onFieldSubmitted: (term) {
        if (focusNode != null && nextFocusNode != null) {
         utils.fieldFocusChange(focusNode, nextFocusNode);
        }
      },
      controller: controller,
      validator: (value) {
        if (!value.isUrl && !canEmpty) {
          return "Invalid URL";
        }
        if (value.isEmpty && !canEmpty) {
          return "URL must not be empty";
        }
        if (value.isNotEmpty && !value.isUrl) {
          return "Invalid URL";
        }
        return null;
      },
      textInputAction: TextInputAction.newline,
      keyboardType: TextInputType.url,
      decoration: InputDecoration(
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: mainColor,
          ),
        ),
        counterText: "",
        helperMaxLines: 3,
        hintMaxLines: 3,
        errorMaxLines: 3,
        helperStyle: TextStyle(
          color: mainColor,
        ),
        hintText: TextData.link,
        hintStyle: TextStyle(
          color: unfocusedColor,
        ),
        prefixIcon: AnimatedOpacity(
          opacity: isVisible ? 1.0 : 0.0,
          duration: textFieldOpacityAnimationDuration,
          child: IconButton(
            icon: Icon(
              MdiIcons.text,
              color: mainColor,
              size: 16.0,
            ),
            onPressed: () {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (!controller.text.contains(urlPrefix)) {
                  controller.text = urlPrefix + controller.text;
                }
              });
            },
          ),
        ),
        suffixIcon: AnimatedOpacity(
          opacity: isVisible ? 1.0 : 0.0,
          duration: textFieldOpacityAnimationDuration,
          child: ClearTextFieldButton(controller: widget.controller),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: mainColor,
          ),
        ),
      ),
    );
  }
}