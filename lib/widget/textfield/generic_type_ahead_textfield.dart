import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/button/clear_text_field.dart';
import 'package:validators/validators.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import  'package:string_similarity/string_similarity.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

class GenericTypeAheadTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final String hint;
  final String helper;
  final bool canEmpty;
  final bool canSpace;
  final TextInputType textInputType;
  final List dataList;
  final int maxLines;
  final int maxLength;
  final Function onSubmitted;
  final Widget trailing;
  final bool showLabel;
  final String label;

  GenericTypeAheadTextField({
    Key key,
    @required this.controller,
    @required this.focusNode,
    this.canSpace = true,
    this.maxLines,
    this.maxLength,
    this.onSubmitted,
    this.trailing,
    this.textInputType,
    @required this.dataList,
    this.nextFocusNode,
    this.hint,
    this.helper,
    this.canEmpty = false,
    this.label = "",
    this.showLabel = false,
  }) : super(key: key);

  @override
  _GenericTypeAheadTextFieldState createState() => _GenericTypeAheadTextFieldState();
}

class _GenericTypeAheadTextFieldState extends State<GenericTypeAheadTextField> {

  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;
  String hint;
  String helper;
  bool canEmpty;
  bool isVisible;
  bool canSpace;
  TextInputType textInputType;
  int maxLines;
  int maxLength;

  @override
  void initState() {
    isVisible = false;
    maxLines = widget.maxLines;
    maxLength = widget.maxLength;
    canSpace = widget.canSpace;
    textInputType = widget.textInputType;
    hint = widget.hint;
    focusNode = widget.focusNode;
    focusNode?.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    nextFocusNode = widget.nextFocusNode;
    controller = widget.controller;
    helper = widget.helper;
    canEmpty = widget.canEmpty;
    super.initState();
  }

  FadeTransition suggestionTransition(Widget suggestionsBox, AnimationController animationController) {
    return FadeTransition(
      child: suggestionsBox,
      opacity: CurvedAnimation(
          parent: animationController,
          curve: Curves.fastOutSlowIn
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: TypeAheadFormField(
        validator: (value) {
          if (value.isNullOrEmpty && !canEmpty) {
            return "${hint.capitalize} must not be empty";
          }
          return null;
        },
        keepSuggestionsOnSuggestionSelected: false,
        autoFlipDirection: true,
        suggestionsCallback: (String currentString) {
          List<String> _tempList = new List.from(widget.dataList);
          List<double> _ratingList = [];
          widget.dataList.forEach((data) {
            double rating = StringSimilarity.compareTwoStrings(currentString.capitalize, data);
            _ratingList.add(rating);
          });
          final Map<double, String> mappings = {
            for (int i = 0; i < _ratingList.length; i++)
              _ratingList[i]: _tempList[i]
          };
          _ratingList.sort((b, a) => a.compareTo(b));
          _tempList = [for (double rating in _ratingList) if (rating > 0.1) mappings[rating]];
          _tempList = _tempList.toSet().toList();
          return _tempList;
        },
        hideOnEmpty: true,
        hideOnError: true,
        itemBuilder: (context, suggestion) {
          return Padding(
            padding: EdgeInsets.only(top: 14.0, left: 8.0, bottom: 14.0,),
            child: Text(
              suggestion,
              style: commonTextStyle,
            ),
          );
        },
        transitionBuilder: (context, suggestionsBox, animationController) => suggestionTransition(suggestionsBox, animationController),
        onSuggestionSelected: (String suggestion) {
          controller.text = suggestion;
        },
        textFieldConfiguration: TextFieldConfiguration(
          focusNode: focusNode,
          style: commonTextStyle,
          maxLength: maxLength ?? 150,
          maxLines: maxLines,
          textCapitalization: TextCapitalization.sentences,
          onSubmitted: widget.onSubmitted ?? (term) {
            if (focusNode != null && nextFocusNode != null) {
              utils.fieldFocusChange(focusNode, nextFocusNode);
            }
          },     
          controller: controller,
          decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: mainColor,
              ),
            ),
            helperText: helper,
            counterText: "",
            helperMaxLines: 3,
            hintMaxLines: 3,
            errorMaxLines: 3,
            helperStyle: TextStyle(
              color: mainColor,
            ),
            hintText: hint,
            hintStyle: TextStyle(
              color: unfocusedColor,
            ),
            labelText: widget.showLabel ? widget.label.length == 0 ? hint : widget.label : null,
            labelStyle: TextStyle(
              color: unfocusedColor,
            ),
            suffixIcon: AnimatedOpacity(
              opacity: isVisible ? 1.0 : 0.0,
              duration: textFieldOpacityAnimationDuration,
              child: widget.trailing ?? ClearTextFieldButton(controller: widget.controller),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: mainColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}