import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/button/clear_text_field.dart';
import 'package:validators/validators.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

class EmailTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;

  EmailTextField({Key key, @required this.controller, this.focusNode, this.nextFocusNode,}) : super(key: key);

  @override
  _EmailTextFieldState createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {

  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;
  bool isVisible;

  @override
  void initState() {
    isVisible = false;
    focusNode = widget.focusNode;
    focusNode.addListener(() {
      if (this.mounted) {
        setState(() {
          isVisible = !isVisible;
        });
      }
    });
    nextFocusNode = widget.nextFocusNode;
    controller = widget.controller;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: TextFormField(
        focusNode: focusNode,
        style: commonTextStyle,
        onFieldSubmitted: (term) {
          if (focusNode != null && nextFocusNode != null) {
            utils.fieldFocusChange(focusNode, nextFocusNode);
          }
        },
        autofillHints: [AutofillHints.email],
        controller: controller,
        validator: (value) {
          if (value.isNullOrEmpty) {
            return "Email must not be empty";
          }
          if (value.contains(" ")) {
            return "Email must not contain white space";
          }
          if (!isEmail(value)) {
            return "Invalid email format";
          }
          return null;
        },
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
          counterText: "",
          helperMaxLines: 3,
          hintMaxLines: 3,
          errorMaxLines: 3,
          helperStyle: TextStyle(
            color: mainColor,
          ),
          hintText: TextData.emailAddress,
          hintStyle: TextStyle(
            color: unfocusedColor,
          ),
          suffixIcon: AnimatedOpacity(
            opacity: isVisible ? 1.0 : 0.0,
            duration: textFieldOpacityAnimationDuration,
            child: ClearTextFieldButton(controller: widget.controller),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: mainColor,
            ),
          ),
        ),
      ),
    );
  }
}