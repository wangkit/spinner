import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';

class GenericStringDropDownField extends StatefulWidget {
  final Function onChanged;
  final String text;
  final String value;
  final List list;
  final double width;

  GenericStringDropDownField({
    Key key,
    @required this.onChanged,
    @required this.text,
    @required this.value,
    @required this.list,
    this.width,
  }) : super(key: key);

  @override
  GenericStringDropDownFieldState createState() => GenericStringDropDownFieldState();
}

class GenericStringDropDownFieldState extends State<GenericStringDropDownField> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width ?? deviceWidth * 0.9,
      child: InputDecorator(
        decoration: InputDecoration(
            labelText: widget.text,
            labelStyle: TextStyle(
              color: mainColor,
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: mainColor,
              ),
              borderRadius: BorderRadius.circular(6.0),
            )
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            value: widget.value,
            isDense: true,
            dropdownColor: useBlackTheme ? defaultBlackColor : defaultWhiteColor,
            onChanged: widget.onChanged,
            items: widget.list.map((value) {
              return DropdownMenuItem(
                value: value,
                child: Text(
                  value,
                  style: TextStyle(
                    color: mainColor,
                  ),
                ),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}