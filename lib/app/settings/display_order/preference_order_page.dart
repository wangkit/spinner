import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

class PreferenceOrderPage extends StatefulWidget {

  PreferenceOrderPage({Key key,}) : super(key: key);

  @override
  _PreferenceOrderPageState createState() => _PreferenceOrderPageState();
}

class _PreferenceOrderPageState extends State<PreferenceOrderPage> {

  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onReorder(int oldIndex, int newIndex) {
    if (newIndex != oldIndex) {
      if (newIndex > oldIndex) {
        newIndex -= 1;
      }
      final String item = preferenceDetailsOrderingList.removeAt(oldIndex);
      preferenceDetailsOrderingList.insert(newIndex, item);
      utils.saveStrList(PrefKey.preferenceOrderValueKey, preferenceDetailsOrderingList);
      refreshThisPage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: TextData.preferenceOrder,
      ),
      body: ReorderableListView(
        header: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            TextData.dragToReorder,
            textAlign: TextAlign.left,
            style: commonTextStyle,
          ),
        ),
        onReorder: _onReorder,
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.symmetric(vertical: 4.0),
        children: List.generate(preferenceDetailsOrderingList.length, (index) {
          return Card(
            key: UniqueKey(),
            color: Colors.white,
            child: ListTile(
              leading: Text(
                (index + 1).toString(),
                style: commonTextStyle,
              ),
              trailing: Icon(
                Icons.drag_handle,
                color: mainColor,
              ),
              title: Text(
                preferenceDetailsOrderingList[index].normalize,
                style: commonTextStyle,
              ),
            ),
          );
        },
        ),
      ),
    );
  }
}
