import 'dart:async';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/app/settings/display_order/profile_order_page.dart';
import 'package:spinner/app/start/start_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/details/review_card.dart';
import 'package:spinner/widget/details/review_title.dart';
import 'package:spinner/widget/restart/restart_widget.dart';

import 'display_order/preference_order_page.dart';

class UserExperienceSettings extends StatefulWidget {

  UserExperienceSettings({Key key,}) : super(key: key);

  @override
  _UserExperienceSettingsState createState() => _UserExperienceSettingsState();
}

class _UserExperienceSettingsState extends State<UserExperienceSettings> {


  @override
  void initState() {
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _header(String title) {
    return Container(
      width: deviceWidth,
      color: useBlackTheme ? grey_black : alice_blue,
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline5.merge(
            TextStyle(color: mainColor),
          ),
        ),
      ),
    );
  }

  Widget _item(String value, {Widget child, Function onPressed,}) {
    return InkWell(
      onTap: onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 12.0, top: 20.0, bottom: 20.0),
              child: Text(
                value,
                maxLines: 4,
                style: commonTextStyle,
              ),
            ),
          ),
          child != null ? child : Container(),
          onPressed == null ? Container() : Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Icon(
                Icons.navigate_next,
                color: mainColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        elevation: 0,
        title: TextData.settings,
        actions: [
          IconButton(
            icon: Icon(
              Icons.redo,
              color: defaultBlackColor,
            ),
            onPressed: () {
              useBlackTheme = false;
              showCountDown = true;
              canPushNotifications = true;
              showProfileDetailsDirectly = false;
              utils.saveBoo(PrefKey.useBlackThemeKey, useBlackTheme);
              utils.saveBoo(PrefKey.showCountDownKey, showCountDown);
              utils.saveBoo(PrefKey.canPushKey, canPushNotifications);
              utils.saveBoo(PrefKey.showProfileDetailsDirectlyKey, showProfileDetailsDirectly);
              refreshThisPage();
              apiManager.flipIsPush();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 10),
            ReviewTitle(title: TextData.ordering),
            ReviewCard(
              children: [
                _item(TextData.profileOrder, onPressed: () {
                  getRoute.navigateTo(ProfileOrderPage());
                }),
                _item(TextData.preferenceOrder, onPressed: () {
                  getRoute.navigateTo(PreferenceOrderPage());
                },),
              ],
            ),
            ReviewTitle(title: TextData.theme),
            ReviewCard(
              children: [
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) {
                    getBinaryDialog(TextData.changeTheme, TextData.areYouSure, () {
                      useBlackTheme = !useBlackTheme;
                      utils.saveBoo(PrefKey.useBlackThemeKey, useBlackTheme);
                      refreshThisPage();
                      utils.toast(TextData.restartApp, isWarning: true);
                    });
                  },
                  activeColor: utils.getColorByHex(myProfile.color),
                  value: useBlackTheme,
                  title: Text(
                    TextData.useBlackTheme,
                    style: commonTextStyle,
                  ),
                ),
              ],
            ),
            ReviewTitle(title: TextData.pushNotifications),
            ReviewCard(
              children: [
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) {
                    canPushNotifications = newValue;
                    utils.saveBoo(PrefKey.canPushKey, canPushNotifications);
                    refreshThisPage();
                    apiManager.flipIsPush();
                  },
                  activeColor: utils.getColorByHex(myProfile.color),
                  value: canPushNotifications,
                  title: Text(
                    TextData.canPushNotifications,
                    style: commonTextStyle,
                  ),
                ),
              ],
            ),
            ReviewTitle(title: TextData.display),
            ReviewCard(
              children: [
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) {
                    showProfileDetailsDirectly = newValue;
                    utils.saveBoo(PrefKey.showProfileDetailsDirectlyKey, showProfileDetailsDirectly);
                    refreshThisPage();
                  },
                  activeColor: utils.getColorByHex(myProfile.color),
                  value: showProfileDetailsDirectly,
                  title: Text(
                    TextData.showProfileDetailsDirectly,
                    style: commonTextStyle,
                  ),
                ),
                SwitchListTile.adaptive(
                  onChanged: (bool newValue) {
                    showCountDown = newValue;
                    utils.saveBoo(PrefKey.showCountDownKey, showCountDown);
                    refreshThisPage();
                  },
                  activeColor: utils.getColorByHex(myProfile.color),
                  value: showCountDown,
                  title: Text(
                    TextData.showCountDownTimer,
                    style: commonTextStyle,
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                getBinaryDialog(TextData.logout, TextData.areYouSure, () {
                  /// No need to remove uniqueid and secret from pref
                  utils.logoutClearData();
                  getRoute.navigateToAndPopAll(StartPage());
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: null,
                    child: Text(
                      TextData.logout,
                      style: commonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
