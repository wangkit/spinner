import 'dart:async';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/app/preference/view_preference_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/first_verify_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/divider/separation_dot.dart';
import 'package:spinner/widget/media/image/image_holder.dart';
import 'package:spinner/widget/media/image/image_viewer.dart';

class VerifyProfilePage extends StatefulWidget {
  final UserProfile userProfile;
  final UserPreference userPreference;
  final Function verifyUser;
  final Function robotUser;
  final Function likeProfile;

  VerifyProfilePage({
    Key key,
    @required this.userProfile,
    @required this.userPreference,
    @required this.verifyUser,
    @required this.robotUser,
    @required this.likeProfile,
  }) : super(key: key);

  @override
  VerifyProfilePageState createState() => VerifyProfilePageState();
}

class VerifyProfilePageState extends State<VerifyProfilePage> with AutomaticKeepAliveClientMixin<VerifyProfilePage> {

  UserProfile _userProfile;
  UserPreference _userPreference;
  bool _isBlurred;

  @override
  void initState() {
    _userPreference = widget.userPreference;
    _isBlurred = true;
    _userProfile = widget.userProfile;
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (!hasShownVerifyDialog) {
        getFirstVerifyDialog();
      }
    });
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: MyAppBar(
        title: utils.getFullName(_userProfile),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              title: Text(
                _userProfile.verificationInstruction,
                style: commonTextStyle,
              ),
            ),
            GestureDetector(
              onTap: () {
                if (_isBlurred) {
                  getBinaryDialog(TextData.showImage, TextData.removeImageProtection, () {
                    _isBlurred = false;
                    refreshThisPage();
                  });
                } else {
                  getRoute.navigateTo(ImageViewer(
                    color: _userProfile.color,
                    text: utils.getFullName(_userProfile),
                    imageUrls: [_userProfile.verificationImage],
                    initialIndex: 0,
                    heroKey: UniqueKey().toString(),
                  ));
                }
              },
              child: Container(
                width: deviceWidth,
                height: deviceWidth,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(_userProfile.verificationImage),
                    fit: BoxFit.cover,
                  ),
                ),
                child: ClipRRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: _isBlurred ? 10 : 0, sigmaY: _isBlurred ? 10 : 0),
                    child: !_isBlurred ? Container() : Container(
                      alignment: Alignment.center,
                      color: Colors.grey.withOpacity(0.1),
                      child: Text(
                        TextData.clickToViewImage,
                        textAlign: TextAlign.center,
                        style: bigTextStyle.merge(
                            TextStyle(
                              color: Colors.white,
                            )
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                LongElevatedButton(
                  width: deviceWidth * 0.45,
                  icon: Padding(
                    padding: EdgeInsets.only(right: 12.0),
                    child: Icon(
                      MdiIcons.robot,
                      color: errorColor,
                    ),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  title: TextData.robot,
                  isBackgroundDarkColor: _userProfile.isDarkColor,
                  color: unfocusedColor,
                  onPressed: () {
                    if (_isBlurred) {
                      utils.toast(TextData.pleaseViewImageFirst, isWarning: true);
                    } else {
                      getBinaryDialog(TextData.robot, TextData.areYouSureThisUserIsARobot, () async {
                        Dialogs.showLoadingDialog();
                        await apiManager.robotUser(_userProfile.id).then((_) {
                          _userProfile.isRobot = true;
                          widget.robotUser();
                          getRoute.pop();
                          getRoute.pop();
                        });
                      });
                    }
                  },
                ),
                LongElevatedButton(
                  width: deviceWidth * 0.45,
                  icon: Padding(
                    padding: EdgeInsets.only(right: 12.0),
                    child: Icon(
                      Icons.verified_user,
                      color: success,
                    ),
                  ),
                  isBackgroundDarkColor: _userProfile.isDarkColor,
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  title: TextData.human,
                  color: utils.getColorByHex(_userProfile.color),
                  onPressed: () {
                    if (_isBlurred) {
                      utils.toast(TextData.pleaseViewImageFirst, isWarning: true);
                    } else {
                      getBinaryDialog(TextData.human, TextData.areYouSureThisUserIsAHuman, () async {
                        Dialogs.showLoadingDialog();
                        await apiManager.verifyUser(_userProfile.id).then((_) {
                          getRoute.pop();
                          getRoute.pop();
                          _userProfile.isVerified = true;
                          widget.verifyUser();
                        });
                      });
                    }
                  },
                ),
              ],
            ),

          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
