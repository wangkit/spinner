import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/widget/animation/open_container_wrapper.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/color_picker/color_picker.dart';
import 'package:spinner/widget/drop_down/drop_down_field.dart';
import 'package:spinner/widget/textfield/generic_textfield.dart';
import 'package:spinner/widget/textfield/generic_type_ahead_textfield.dart';

class EditProfilePage extends StatefulWidget {

  EditProfilePage({
    Key key,
  }) : super(key: key);

  @override
  EditProfilePageState createState() => EditProfilePageState();
}

class EditProfilePageState extends State<EditProfilePage> with AutomaticKeepAliveClientMixin<EditProfilePage> {

  TextEditingController _lastNameController;
  TextEditingController _firstNameController;
  TextEditingController _countryController;
  TextEditingController _cityController;
  TextEditingController _professionController;
  TextEditingController _hobbyController;
  TextEditingController _personalityController;
  TextEditingController _schoolController;
  FocusNode _lastNameNode;
  FocusNode _firstNameNode;
  FocusNode _countryNode;
  FocusNode _cityNode;
  FocusNode _professionNode;
  FocusNode _hobbyNode;
  FocusNode _personalityNode;
  FocusNode _schoolNode;
  String _myReligion;
  String _myEthnicity;
  String _myEducationLevel;
  String _currentColorCode;
  double _longitude;
  double _latitude;
  GlobalKey<FormState> _formKey = GlobalKey();

  void onSelectColor(Color pickerColor, StateSetter customState) {
    _currentColorCode = utils.getHexCodeByColor(pickerColor);
    customState(() {});
  }

  @override
  void initState() {
    _firstNameController = TextEditingController(
      text: myProfile.firstName,
    );
    _lastNameController = TextEditingController(
      text: myProfile.lastName,
    );
    _countryController = TextEditingController(
      text: myProfile.country,
    );
    _cityController = TextEditingController(
      text: myProfile.city,
    );
    _professionController = TextEditingController(
      text: myProfile.profession,
    );
    _hobbyController = TextEditingController(
      text: myProfile.hobby,
    );
    _personalityController = TextEditingController(
      text: myProfile.personality,
    );
    _schoolController = TextEditingController(
      text: myProfile.graduatedFrom,
    );
    _firstNameNode = FocusNode();
    _lastNameNode = FocusNode();
    _countryNode = FocusNode();
    _cityNode = FocusNode();
    _professionNode = FocusNode();
    _hobbyNode = FocusNode();
    _personalityNode = FocusNode();
    _schoolNode = FocusNode();
    _currentColorCode = myProfile.color;
    _myReligion = myProfile.religion;
    _myEthnicity = myProfile.ethnicity;
    _myEducationLevel = myProfile.education;
    _longitude = myProfile.longitude;
    _latitude = myProfile.latitude;
    super.initState();
  }

  @override
  void dispose() {
    _lastNameController.dispose();
    _firstNameController.dispose();
    _countryController.dispose();
    _cityController.dispose();
    _professionController.dispose();
    _hobbyController.dispose();
    _personalityController.dispose();
    _schoolController.dispose();
    _lastNameNode.dispose();
    _firstNameNode.dispose();
    _countryNode.dispose();
    _cityNode.dispose();
    _professionNode.dispose();
    _hobbyNode.dispose();
    _personalityNode.dispose();
    _schoolNode.dispose();
    super.dispose();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: MyAppBar(
        title: TextData.editProfile,
        actions: [
          TextButton(
            onPressed: () async {
              if (_longitude == null || _latitude == null || _cityController.text.isNullOrEmpty || _countryController.text.isNullOrEmpty) {
                utils.toast(TextData.cantGetInformation);
                return;
              }
              Dialogs.showLoadingDialog(title: TextData.editingProfile);
              await apiManager.updateProfile(
                _firstNameController.text.trim().capitalize,
                _lastNameController.text.trim().capitalize,
                myProfile.email,
                _countryController.text.trim(),
                _cityController.text.trim(),
                _longitude,
                _latitude,
                myProfile.gender,
                _currentColorCode,
                myProfile.birthday,
                myProfile.age,
                _myEthnicity ?? "",
                _myReligion ?? "",
                myProfile.zodiacSign,
                _professionController.text?.trim() ?? "",
                _schoolController.text?.trim() ?? "",
                _myEducationLevel ?? "",
                _personalityController.text?.trim() ?? "",
                myProfile.preferredRelationship,
                myProfile.noLoveExperience,
                _hobbyController.text?.trim() ?? "",
                myProfile.verificationInstruction,
              );
              await apiManager.getMyProfileAndPreference();
              getRoute.pop();
              getRoute.pop();
              utils.toast(TextData.editedProfile, isSuccess: true);
            },
            child: Text(
              TextData.done,
              style: commonTextStyle,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: StatefulBuilder(
          builder: (ctx, customState) {
            return Form(
              key: _formKey,
              child: Column(
                children: [
                  GenericTextField(
                    controller: _firstNameController,
                    focusNode: _firstNameNode,
                    nextFocusNode: _lastNameNode,
                    hint: TextData.firstName,
                    textInputType: TextInputType.name,
                    showLabel: true,
                    textCapitalization: TextCapitalization.words,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                    ],
                    autofillHints: [AutofillHints.givenName],
                    validation: (String newValue) {
                      if (newValue.isNullOrEmpty) {
                        return "${TextData.name} must not be empty";
                      }
                      if (newValue.contains(" ")) {
                        return "${TextData.name} must not contain white space";
                      }
                      return null;
                    },
                  ),
                  GenericTextField(
                    controller: _lastNameController,
                    focusNode: _lastNameNode,
                    nextFocusNode: _professionNode,
                    hint: TextData.lastName,
                    textInputType: TextInputType.name,
                    showLabel: true,
                    textCapitalization: TextCapitalization.words,
                    inputFormatter: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                    ],
                    autofillHints: [AutofillHints.familyName],
                    validation: (String newValue) {
                      if (newValue.isNullOrEmpty) {
                        return "${TextData.name} must not be empty";
                      }
                      if (newValue.contains(" ")) {
                        return "${TextData.name} must not contain white space";
                      }
                      return null;
                    },
                  ),
                  Center(
                    child: ColorPicker(
                      onPressed: onSelectColor,
                      customState: customState,
                      currentColorCode: _currentColorCode,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GenericStringDropDownField(
                    text: TextData.religion,
                    onChanged: (newValue) {
                      customState(() {
                        _myReligion = newValue;
                      });
                    },
                    list: religionList,
                    value: _myReligion,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GenericStringDropDownField(
                    text: TextData.ethnicity,
                    onChanged: (newValue) {
                      customState(() {
                        _myEthnicity = newValue;
                      });
                    },
                    list: ethnicityList,
                    value: _myEthnicity,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GenericStringDropDownField(
                    text: TextData.educationLevel,
                    onChanged: (newValue) {
                      customState(() {
                        _myEducationLevel = newValue;
                      });
                    },
                    list: educationLevelList,
                    value: _myEducationLevel,
                  ),
                  GenericTypeAheadTextField(
                    controller: _professionController,
                    focusNode: _professionNode,
                    nextFocusNode: _schoolNode,
                    hint: TextData.profession,
                    dataList: professionList,
                    showLabel: true,
                  ),
                  GenericTypeAheadTextField(
                    controller: _schoolController,
                    focusNode: _schoolNode,
                    nextFocusNode: _hobbyNode,
                    hint: TextData.nameOfSchool,
                    dataList: schoolList,
                    showLabel: true,
                  ),
                  GenericTypeAheadTextField(
                    controller: _hobbyController,
                    focusNode: _hobbyNode,
                    nextFocusNode: _personalityNode,
                    hint: TextData.hobby,
                    showLabel: true,
                    dataList: hobbyList,
                  ),
                  GenericTextField(
                    controller: _personalityController,
                    focusNode: _personalityNode,
                    nextFocusNode: _firstNameNode,
                    hint: TextData.personality,
                    showLabel: true,
                    textInputType: TextInputType.multiline,
                    maxLines: null,
                    maxLength: 250,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
