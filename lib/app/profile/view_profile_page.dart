import 'dart:async';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/widget/animation/open_container_wrapper.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/button/refresh_badge.dart';
import 'package:spinner/widget/divider/separation_dot.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/media/image/image_holder.dart';
import 'package:spinner/widget/media/image/image_viewer.dart';
import 'package:spinner/widget/profile/user_profile_holder.dart';

class ViewProfilePage extends StatefulWidget {
  final UserProfile userProfile;
  final UserPreference userPreference;
  final bool isMyProfile;

  ViewProfilePage({
    Key key,
    this.userProfile,
    this.userPreference,
    this.isMyProfile = false,
  }) : super(key: key);

  @override
  ViewProfilePageState createState() => ViewProfilePageState();
}

class ViewProfilePageState extends State<ViewProfilePage> with AutomaticKeepAliveClientMixin<ViewProfilePage> {

  bool _showLoading;
  Future<void> _loading;
  bool _isMyProfile;
  UserProfile _thisProfile;
  UserPreference _thisPreference;

  @override
  void initState() {
    _thisPreference = widget.userPreference;
    _isMyProfile = widget.isMyProfile;
    _thisProfile = widget.userProfile;
    _showLoading = false;
    _loading =  _emptyCall();
    if (_isMyProfile) {
      _thisProfile = myProfile;
      _thisPreference = myPreference;
    }
    super.initState();
  }

  Future<void> _emptyCall() async {}

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: MyAppBar(
        title: _isMyProfile ? TextData.myProfile : utils.getFullName(_thisProfile),
        actions: _isMyProfile ? [
          IconButton(
            onPressed: () {
              getBinaryDialog(
                TextData.quickDisplay,
                TextData.quickDisplayMessage,
                    () {},
                needNegativeButton: false,
                positiveText: TextData.ok,
              );
            },
            icon: Icon(
              Icons.info,
              color: mainColor,
            ),
          ),
          RefreshBadge(
            needVip: false,
            onRefreshClicked: () async {
              _showLoading = true;
              refreshThisPage();
              if (_isMyProfile) {
                await apiManager.getMyProfileAndPreference().then((_) {
                  _thisProfile = myProfile;
                  _thisPreference = myPreference;
                });
              } else {
                await apiManager.getProfileById(_thisProfile.id).then((jsonResponse) {
                  List<dynamic> listOfProfile = jsonResponse[DataKey.profile];
                  List<dynamic> listOfPreference = jsonResponse[DataKey.preference];
                  if (listOfProfile.length > 0 && listOfPreference.length > 0) {
                    _thisPreference = UserPreference.generateUserPreference(listOfPreference).first;
                    _thisProfile = UserProfile.generateUserProfile(listOfProfile).first;
                  }
                });
              }
              _showLoading = false;
              refreshThisPage();
            },
          ),
        ] : [],
      ),
      body: FutureBuilder(
        future: _loading,
        builder: (ctx, snapshot) {
          return snapshot.connectionState != ConnectionState.done || _showLoading ? LoadingWidget() :
          UserProfileHolder(
            key: UserProfile().getProfileWidgetKey(_thisProfile),
            userProfile: _thisProfile,
            userPreference: _thisPreference,
            needVerification: !_isMyProfile,
            isMyProfile: _isMyProfile,
            isViewProfilePage: true,
            removeProfileFromList: null,
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
