import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:spinner/resources/models/env.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/gender_status.dart';
import 'package:spinner/resources/status/preferred_relationship_status.dart';
import 'package:spinner/resources/values/analysis.dart';
import 'package:spinner/background/background_task.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/remote_config_key.dart';
import 'package:spinner/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:spinner/app/main/main_page.dart';
import 'package:spinner/app/start/start_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/color_picker/color_picker.dart';
import 'package:spinner/widget/custom_date_picker/custom_date_picker.dart' as customDate;
import 'package:spinner/widget/details/review_card.dart';
import 'package:spinner/widget/details/review_title.dart';
import 'package:spinner/widget/divider/grey_divider.dart';
import 'package:spinner/widget/drop_down/drop_down_field.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/media/image/video_url_thumbnail.dart';
import 'package:spinner/widget/scrollable_footer/scrollable_footer.dart';
import 'package:spinner/widget/tag/tag.dart';
import 'package:spinner/widget/textfield/email_textfield.dart';
import 'package:spinner/widget/textfield/generic_textfield.dart';
import 'package:spinner/widget/textfield/generic_type_ahead_textfield.dart';
import 'package:spinner/widget/textfield/otp_textfield.dart';
import 'package:spinner/widget/media/video/video_download_thumbnail.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:workmanager/workmanager.dart';

class SetupPage extends StatefulWidget {
  final bool isExistingMember;
  final String prefillEmail;
  final bool navigateToPref;
  final bool editProfile;
  final bool editPref;
  final Function notifyParent;
  final bool isNewRegister;

  SetupPage({
    Key key,
    this.isExistingMember = false,
    this.prefillEmail,
    this.navigateToPref = false,
    this.editProfile = false,
    this.editPref = false,
    this.isNewRegister = false,
    this.notifyParent,
  }) : super(key: key);

  @override
  _SetupPageState createState() => _SetupPageState();
}

class _SetupPageState extends State<SetupPage> {

  List<Widget> _pages;
  TextEditingController _emailController;
  TextEditingController _otpController;
  TextEditingController _lastNameController;
  TextEditingController _firstNameController;
  TextEditingController _countryController;
  TextEditingController _cityController;
  TextEditingController _professionController;
  TextEditingController _schoolController;
  TextEditingController _hobbyController;
  TextEditingController _personalityController;
  TextEditingController _prefHobbyController;
  FocusNode _hobbyFocusNode;
  FocusNode _personalityFocusNode;
  FocusNode _schoolFocusNode;
  FocusNode _countryFocusNode;
  FocusNode _cityFocusNode;
  FocusNode _professionFocusNode;
  FocusNode _lastNameFocusNode;
  FocusNode _firstNameFocusNode;
  FocusNode _emailFocusNode;
  FocusNode _otpFocusNode;
  FocusNode _prefHobbyFocusNode;
  double _longitude;
  double _latitude;
  double _preferredDistanceValue;
  RangeValues _preferredAgeValue;
  RangeValues _preferredLikeCountValue;
  bool navigateToPref;
  PageController _pageController;
  List<int> _preferredGender = [];
  List<int> _preferredAge;
  List<int> _preferredLikeCount;
  String _tempEthnicity;
  String _tempZodiacSign;
  String _tempReligion;
  String _tempEducationLevel;
  List<String> _preferredEthnicity = [];
  List<String> _preferredReligion = [];
  List<String> _preferredZodiacSign = [];
  List<String> _preferredEducation = [];
  List<String> _preferredHobby = [];
  bool _preferGreenHead = false;
  bool _preferNonGreenHead = false;
  bool _preferredIsGreenHead;
  int _preferredDistance;
  String _videoLink;
  List<String> _imageLinks = [];
  String prefillEmail;
  bool isExistingMember;
  int _preferredRelationship;
  Widget _nextPageArrow;
  Widget _previousPageArrow;
  bool _isMale = false;
  bool _isFemale = false;
  bool _isTransgender = false;
  bool _isGenderqueer = false;
  bool _isAgender = false;
  bool _isPrefMale = false;
  bool _isPrefFemale = false;
  bool _isPrefTransgender = false;
  bool _isPrefGenderqueer = false;
  bool _isPrefAgender = false;
  bool _hasExperienceBefore = false;
  bool _noExperienceBefore = false;
  bool _isPrefMarriage = false;
  bool _isPrefLongTerm = false;
  bool _isPrefCasual = false;
  bool _isPrefWhatever = false;
  int _myGender;
  bool _noLoveExperience;
  String _myZodiacSign;
  String _myEthnicity;
  String _myEducationLevel;
  String _myReligion;
  int _myAge;
  DateTime _myBDay;
  String _birthday;
  Duration _animationDuration = Duration(milliseconds: 300);
  Curve _animationCurve = Curves.easeInToLinear;
  final _formOneKey = GlobalKey<FormState>();
  final _resendBtnKey = GlobalKey<LongElevatedButtonState>();
  final _formTwoKey = GlobalKey<FormState>();
  final _formThreeKey = GlobalKey<FormState>();
  final _formFourKey = GlobalKey<FormState>();
  final _formFiveKey = GlobalKey<FormState>();
  final _formSixKey = GlobalKey<FormState>();
  final _formSevenKey = GlobalKey<FormState>();
  String currentColorCode;
  List<File> _imageFiles;
  File _videoFile;
  File _verificationImage;
  String _verificationInstruction;
  bool _alreadyHasPreference;
  static const int _emailPage = 0;
  static const int _otpPage = 1;
  static const int _profileBeginPage = _profileImagePage;
  static const int _profileNamePage = 3;
  static const int _profileLocationPage = 4;
  static const int _profileGenderPage = 5;
  static const int _profileBdayPage = 6;
  static const int _profileGreenHeadPage = 7;
  static const int _profileEthnicityAndReligionPage = 8;
  static const int _profileEducationPage = 9;
  static const int _profileProfessionPage = 10;
  static const int _profilePersonalPage = 11;
  static const int _profileColorPage = 12;
  static const int _profileImagePage = 13;
  static const int _profileVideoPage = 14;
  static const int _profileVerificationPage = 15;
  static const int _profileReviewPage = 16;
  static const int _profileEndPage = 17;
  static const int _prefBeginPage = 18;
  static const int _prefDistancePage = 19;
  static const int _prefAgePage = 20;
  static const int _prefEthnicityPage = 21;
  static const int _prefReligionPage = 22;
  static const int _prefZodiacSignPage = 23;
  static const int _prefEducationPage = 24;
  static const int _prefHobbyPage = 25;
  static const int _prefGreenHeadPage = 26;
  static const int _prefLikeCountPage = 27;
  static const int _prefEndPage = 28;
  bool _editProfile;
  bool _editPref;
  bool _isNewRegister;
  ImagePicker _picker;
  FirebaseVisionImage _visionImage;
  FaceDetector _faceDetector;
  UserProfile _tempProfile;
  DateTime _prefillBDay = DateTime.now().subtract(Duration(days: 365 * 18));

  void onSelectColor(Color pickerColor, StateSetter customState) {
    currentColorCode = utils.getHexCodeByColor(pickerColor);
    customState(() {});
  }

  void _updateMediaInBackground() {
    /// Only use background service for creating profile
    BackgroundTask.registerUploadMedia(_imageFiles, !_editProfile ? [] : _tempProfile?.images, _editProfile, _editProfile ? qp.email : _emailController.text, uploader.getUploadPath(), apiBaseUrl, qp.secret, qp.uniqueId);
  }

  Future<void> _updateMedia() async {
    /// Use main isolates for editing profile since there is no need to use background service for updating
    await _uploadImages().then((_) {
      apiManager.updateImagesPath(_imageLinks, _editProfile ? qp.email : _emailController.text, false);
    });
    if (_verificationImage != null) {
      uploader.uploadFileToFirebase(_verificationImage, false).then((String verImageLink) {
        if (!verImageLink.isNullOrEmpty) {
          apiManager.updateImagesPath(verImageLink, _editProfile ? qp.email : _emailController.text, true);
        }
      });
    }
    await uploadVideo();
  }

  void _nextPage() {
    refreshThisPage();
    _pageController.nextPage(duration: _animationDuration, curve: _animationCurve);
  }

  void _skipPage({int targetIndex}) {
    refreshThisPage();
    _pageController.jumpToPage(targetIndex != null ? targetIndex : _pageController.page.floor() + 2);
  }

  void _previousPage() {
    if (_isNewRegister) {
      switch (_pageController.page.floor()) {
        case _profileImagePage:
          _pageController.jumpToPage(_profileGreenHeadPage);
          break;
        case _profileReviewPage:
          _pageController.jumpToPage(_profileImagePage);
          break;
        default:
          _pageController.animateToPage(_pageController.page.floor() - 1, duration: _animationDuration, curve: _animationCurve);
      }
    } else if (_editProfile) {
      _pageController.animateToPage(_pageController.page.floor() - 1, duration: _animationDuration, curve: _animationCurve);
    } else {
      _pageController.animateToPage(_pageController.page.floor() - 1, duration: _animationDuration, curve: _animationCurve);
    }
  }

  void _onBackFromFirstPage() {
    getBinaryDialog(
      TextData.leave,
      TextData.leaveThisPage,
          () {
        if (!_editProfile && !_editPref) {
          /// Not editing, then back directly to main page
          utils.logoutClearData();
          getRoute.navigateToAndPopAll(
            StartPage()
          );
        } else {
          /// If editing, pop to go back
          getRoute.truePop();
        }
      },
    );
  }

  void _setVerificationInstruction() {
    if (_editProfile) {
      _verificationInstruction = myProfile.verificationInstruction;
    } else {
      final _random = new Random();
      if (instructionList.length > 0) {
        _verificationInstruction = instructionList[_random.nextInt(instructionList.length)];
      }
    }
  }

  void setImageFilesListToNull() {
    _imageFiles = List.generate(allowedProfileMedia, (index) => null);
  }

  String getPrefillProfile(String value) {
    return _editProfile ? value : null;
  }

  @override
  void initState() {
    _isNewRegister = widget.isNewRegister;
    _faceDetector = FirebaseVision.instance.faceDetector(
      FaceDetectorOptions(),
    );
    _picker = ImagePicker();
    _editPref = widget.editPref;
    _editProfile = widget.editProfile;
    if (_editProfile) {
      _tempProfile = UserProfile(
        images: List.from(myProfile.images),
        video: myProfile.video,
        videoThumnbnail: myProfile.videoThumnbnail,
      );
    }
    _pages = [];
    navigateToPref = widget.navigateToPref;
    _pageController = PageController(
      initialPage:  navigateToPref || _editPref ? _prefBeginPage : _editProfile ? _profileBeginPage : 0,
    );
    _alreadyHasPreference = false;
    _setVerificationInstruction();
    _preferredAgeValue = RangeValues(adultAge.toDouble(), maxAge.toDouble());
    _preferredAge = [_preferredAgeValue.start.round(), _preferredAgeValue.end.round()];
    _preferredLikeCountValue = RangeValues(0, 0);
    _preferredLikeCount = [_preferredLikeCountValue.start.round(), _preferredLikeCountValue.end.round()];
    _preferredDistanceValue = minDistance.toDouble();
    _preferredDistance = _preferredDistanceValue.floor();
    _preferredRelationship = _editProfile || _editPref ? myProfile.preferredRelationship : PreferredRelationshipStatus.noPreference;
    setImageFilesListToNull();
    currentColorCode = _editProfile ? myProfile.color : utils.getHexCodeByColor(appIconColor);
    _myBDay = _editProfile ? timeUtils.parseBdayString(myProfile.birthday) : _prefillBDay;
    prefillEmail = widget.prefillEmail;
    isExistingMember = widget.isExistingMember;
    _otpController = TextEditingController();
    _emailController = TextEditingController(text: prefillEmail);
    _lastNameController = TextEditingController(text: getPrefillProfile(myProfile?.lastName));
    _firstNameController = TextEditingController(text: getPrefillProfile(myProfile?.firstName));
    _cityController = TextEditingController(text: getPrefillProfile(myProfile?.city));
    _countryController = TextEditingController(text: getPrefillProfile(myProfile?.country));
    _schoolController = TextEditingController(text: getPrefillProfile(myProfile?.graduatedFrom));
    _professionController = TextEditingController(text: getPrefillProfile(myProfile?.profession));
    _hobbyController = TextEditingController(text: getPrefillProfile(myProfile?.hobby));
    _personalityController = TextEditingController(text: getPrefillProfile(myProfile?.personality.isNullOrEmpty ? remoteConfig.getString(RemoteConfigKey.newUserPersonality) : myProfile.personality));
    _prefHobbyController = TextEditingController();
    _prefHobbyFocusNode = FocusNode();
    _hobbyFocusNode = FocusNode();
    _personalityFocusNode = FocusNode();
    _cityFocusNode = FocusNode();
    _schoolFocusNode = FocusNode();
    _countryFocusNode = FocusNode();
    _professionFocusNode = FocusNode();
    _firstNameFocusNode = FocusNode();
    _lastNameFocusNode = FocusNode();
    _emailFocusNode = FocusNode();
    _otpFocusNode = FocusNode();
    _previousPageArrow = IconButton(
      icon: Icon(
        Icons.arrow_back,
        color: mainColor,
      ),
      onPressed: () => _previousPage(),
    );
    _nextPageArrow = IconButton(
      icon: Icon(
        Icons.arrow_forward,
        color: mainColor,
      ),
      onPressed: () => _nextPage(),
    );
    if (_editProfile) {
      _noLoveExperience = myProfile.noLoveExperience;
      _birthday = myProfile.birthday;
      _myAge = myProfile.age;
      if (_noLoveExperience) {
        _hasExperienceBefore = false;
        _noExperienceBefore = true;
      } else {
        _noExperienceBefore = false;
        _hasExperienceBefore = true;
      }
      _verificationInstruction = myProfile.verificationInstruction;
      _myGender = _editProfile ? myProfile.gender : null;
      _myEthnicity = myProfile.ethnicity.isNullOrEmpty ? null : myProfile.ethnicity;
      _myReligion = myProfile.religion.isNullOrEmpty ? null : myProfile.religion;
      _latitude = myProfile.latitude;
      _longitude = myProfile.longitude;
      _myZodiacSign = myProfile.zodiacSign;
      _myEducationLevel = myProfile.education.isNullOrEmpty ? null : myProfile.education;
      _setPrefRelationshipBool(_preferredRelationship, true);
      _prefillBDay = _myBDay;
      _setMyGender(_myGender, true);
    }
    if (_editPref) {
      _setOriginalPreferredGender();
      _preferredDistanceValue = myPreference.distance.toDouble();
      _preferredDistance = _preferredDistanceValue.floor();
      _preferredAgeValue = RangeValues(myPreference.age.first.toDouble(), myPreference.age.last.toDouble());
      _preferredAge = [_preferredAgeValue.start.round(), _preferredAgeValue.end.round()];
      _preferredLikeCountValue = RangeValues(myPreference.likeCounts.first.toDouble(), myPreference.likeCounts.last.toDouble());
      _preferredLikeCount = [_preferredLikeCountValue.start.round(), _preferredLikeCountValue.end.round()];
      _preferredEthnicity = _convertStringListToStringListWithQuote(myPreference.ethnicity);
      _preferredReligion = _convertStringListToStringListWithQuote(myPreference.religion);
      _preferredZodiacSign = _convertStringListToStringListWithQuote(myPreference.zodiacSign);
      _preferredEducation = _convertStringListToStringListWithQuote(myPreference.education);
      _preferredHobby = _convertStringListToStringListWithQuote(myPreference.hobby);
      if (myPreference.isGreenHead == null) {
        _preferredIsGreenHead = null;
      } else if (myPreference.isGreenHead) {
        _preferGreenHead = true;
        _preferredIsGreenHead = true;
      } else if (!myPreference.isGreenHead) {
        _preferNonGreenHead = true;
        _preferredIsGreenHead = false;
      }
    }
    super.initState();
  }

  List<String> _convertStringListToStringListWithQuote(List<String> targetList) {
    return targetList.map((x) => "'$x'").toList();
  }

  @override
  void dispose() {
    _prefHobbyController.dispose();
    _prefHobbyFocusNode.dispose();
    _otpController.dispose();
    _otpFocusNode.dispose();
    _emailController.dispose();
    _emailFocusNode.dispose();
    _pageController.dispose();
    _firstNameController.dispose();
    _lastNameController.dispose();
    _schoolController.dispose();
    _professionController.dispose();
    _professionFocusNode.dispose();
    _schoolFocusNode.dispose();
    _firstNameFocusNode.dispose();
    _lastNameFocusNode.dispose();
    _countryController.dispose();
    _cityFocusNode.dispose();
    _countryFocusNode.dispose();
    _hobbyController.dispose();
    _hobbyFocusNode.dispose();
    _personalityController.dispose();
    _personalityFocusNode.dispose();
    super.dispose();
  }

  Future<void> _uploadImages() async {
    for (int i = 0; i < allowedProfileMedia; i++) {
      var _file = _imageFiles[i];
      if (_file != null) {
        /// Add ' at start and end to conform to backend's need
        _imageLinks.add("'" + await uploader.uploadFileToFirebase(_file, false) + "'");
      } else {
        if (_editProfile) {
          int _tempIndex = _tempProfile.images.length - 1;
          if (_tempIndex >= i) {
            _imageLinks.add("'" + _tempProfile.images[i] + "'");
          } else {
            _imageLinks.add("'" + placeholderLink + "'");
          }
        } else {
          _imageLinks.add("'" + placeholderLink + "'");
        }
      }
      print("finish uploading: $i");
    }
  }

  Widget _buttonColumn({Function onNextPressed, String nextTitle = TextData.next, bool needBackButton = true}) {
    if (onNextPressed == null) onNextPressed = _nextPage;
    return Expanded(
        child: SafeArea(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Flexible(
                  child: _nextButton(onNextPressed, false, nextTitle),
                ),
                needBackButton ? Flexible(
                  child: _backButton(),
                ) : Container(),
              ],
            ),
          ),
        )
    );
  }

  Widget _cancelRegistrationButton() {
    return IconButton(
      icon: Icon(
        Icons.close,
        color: mainColor,
      ),
      onPressed: () {
        _onBackFromFirstPage();
      },
    );
  }

  Widget _backButton() {
    return LongElevatedButton(
      isReverseColor: true,
      padding: EdgeInsets.only(bottom: 24.0),
      title: TextData.back,
      onPressed: () {
        _previousPage();
      },
    );
  }

  Future<void> uploadVideo() async {
    String _thumbnailLink = "";
    /// If is editing profile and original video is empty or null and no new video is added
    /// then set the video and video thumbnail to empty
    if (_editProfile && myProfile.video.isNullOrEmpty && _videoFile == null) {
      _videoLink = "";
    } else {
      if (_videoFile != null) {
        _videoLink = await uploader.uploadFileToFirebase(_videoFile, true);
        Uint8List _thumbnail = await VideoThumbnail.thumbnailData(
          video: _videoLink,
          imageFormat: ImageFormat.JPEG,
          quality: 80,
        ).catchError((onThumbnailError) {
          debugPrint("onThumbnailError: $onThumbnailError");
        });
        File _tempFile = await utils.uintToFile(_thumbnail);
        _thumbnailLink = await uploader.uploadFileToFirebase(_tempFile, false).catchError((e) {
          debugPrint("Upload video thumbnail error: $e");
        });
      }
    }
    apiManager.updateVideoPath(_videoLink, _editProfile ? myProfile.email : _emailController.text, _thumbnailLink);
  }

  Widget _commonPreferenceChoiceWidget(String title, String label, List preferredList, String value, List sourceList) {
    return StatefulBuilder(
      builder: (ctx, customState) {
        return ScrollableFooter(
          children: [
            _titleDisplay(title),
            GenericStringDropDownField(
              text: label,
              onChanged: (newValue) {
                String _value = "'$newValue'";
                if (!preferredList.contains(_value)) {
                  customState(() {
                    preferredList.add(_value);
                  });
                }
              },
              list: sourceList,
              value: value,
            ),
            Flexible(
              child: Container(
                width: deviceWidth,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 12.0),
                  child: Wrap(
                    children: List.generate(preferredList.length, (index) {
                      return Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Tag(
                          key: Key(preferredList[index]),
                          text: preferredList[index].replaceAll("'", ""),
                          colorCode: currentColorCode,
                          icon: Icon(
                            Icons.clear,
                            color: mainColor,
                          ),
                          onPressed: () {
                            customState(() {
                              preferredList.removeAt(index);
                            });
                          },
                        ),
                      );
                    }),
                  ),
                ),
              ),
            ),
            _textDisplay(TextData.canChooseMoreThanOne),
            _buttonColumn(onNextPressed: () {
              apiManager.logAnalysis(Analysis.prefCommon + label.toUpperCase());
              _nextPage();
            }),
          ],
        );
      },
    );
  }

  Widget _titleDisplay(String value) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 24.0, top: 20.0),
      child: Text(
        value,
        style: bigTextStyle.merge(
            TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: bigFontSize + 3,
            )
        ),
      ),
    );
  }

  Widget _textDisplay(String value, {Color textColor}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
      child: Text(
        value,
        style: commonTextStyle.merge(TextStyle(
          fontWeight: FontWeight.w500,
          color: textColor,
        )),
      ),
    );
  }

  Widget _reviewInformation(String value, int page, {String lastValue, Widget extraWidget, bool needLastValue = true}) {
    return InkWell(
      onTap: () {
        _pageController.animateToPage(page, duration: _animationDuration, curve: _animationCurve);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 12.0, top: 20.0, bottom: 20.0),
              child: Text(
                value,
                maxLines: 4,
                style: commonTextStyle,
              ),
            ),
          ),
          extraWidget != null ? extraWidget : Container(),
          !needLastValue ? Container() : Flexible(
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(right: 5.0),
                child: Text(
                  lastValue.isNullOrEmpty ? TextData.empty : lastValue,
                  maxLines: 4,
                  style: commonTextStyle,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Icon(
                Icons.navigate_next,
                color: mainColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _setPrefRelationshipBool(int index, bool value) {
    switch (index) {
      case 0:
        _isPrefMarriage = value;
        _isPrefLongTerm = false;
        _isPrefCasual = false;
        _isPrefWhatever = false;
        break;
      case 1:
        _isPrefLongTerm = value;
        _isPrefMarriage = false;
        _isPrefCasual = false;
        _isPrefWhatever = false;
        break;
      case 2:
        _isPrefCasual = value;
        _isPrefMarriage = false;
        _isPrefWhatever = false;
        _isPrefLongTerm = false;
        break;
      case 3:
        _isPrefWhatever = value;
        _isPrefMarriage = false;
        _isPrefCasual = false;
        _isPrefLongTerm = false;
        break;
    }
  }

  Widget _preferredRelationshipBox(int index, customState) {

    bool _getValue() {
      switch (index) {
        case 0:
          return _isPrefMarriage;
        case 1:
          return _isPrefLongTerm;
        case 2:
          return _isPrefCasual;
        case 3:
          return _isPrefWhatever;
        default:
          return false;
      }
    }

    return CheckboxListTile(
      activeColor: appBgColor,
      checkColor: sharp_green,

      title: Text(
        PreferredRelationshipStatus.relationshipMap.values.toList()[index],
        style: commonTextStyle,
      ),
      onChanged: (bool value) {
        customState(() {
          _setPrefRelationshipBool(index, value);
        });
        if (!_isPrefWhatever && !_isPrefCasual && !_isPrefMarriage && !_isPrefLongTerm) {
          _preferredRelationship = PreferredRelationshipStatus.noPreference;
        } else {
          _preferredRelationship = index;
        }
      },
      value: _getValue(),
    );
  }

  void _setGenderValue(int index, bool value) {
    switch (index) {
      case GenderStatus.male:
        _isPrefMale = value;
        break;
      case GenderStatus.female:
        _isPrefFemale = value;
        break;
      case GenderStatus.transgender:
        _isPrefTransgender = value;
        break;
      case GenderStatus.genderQueer:
        _isPrefGenderqueer = value;
        break;
      case GenderStatus.agender:
        _isPrefAgender = value;
        break;
    }
  }

  void _setOriginalPreferredGender() {
    myPreference.gender.forEach((genderIndex) {
      _preferredGender.add(genderIndex);
      switch (genderIndex) {
        case GenderStatus.male:
          _isPrefMale = true;
          break;
        case GenderStatus.female:
          _isPrefFemale = true;
          break;
        case GenderStatus.transgender:
          _isPrefTransgender = true;
          break;
        case GenderStatus.genderQueer:
          _isPrefGenderqueer = true;
          break;
        case GenderStatus.agender:
          _isPrefAgender = true;
          break;
      }
    });
  }

  Widget _preferredGenderChoice(int index, customState) {

    bool _getValue() {
      switch (index) {
        case GenderStatus.male:
          return _isPrefMale;
        case GenderStatus.female:
          return _isPrefFemale;
        case GenderStatus.transgender:
          return _isPrefTransgender;
        case GenderStatus.genderQueer:
          return _isPrefGenderqueer;
        case GenderStatus.agender:
          return _isPrefAgender;
        default:
          return false;
      }
    }

    return CheckboxListTile(
      activeColor: appBgColor,
      checkColor: sharp_green,
      title: Text(
        GenderStatus.genderMap.values.toList()[index],
        style: commonTextStyle,
      ),
      onChanged: (bool value) {
        customState(() {
          _setGenderValue(index, value);
        });
        if (value) {
          _preferredGender.add(index);
        } else {
          _preferredGender.remove(index);
        }
      },
      value: _getValue(),
    );
  }

  String _getListStringAsString(List<String> values) {
    String _result = "";
    for (int i = 0; i < values.length; i++) {
      if (i != values.length - 1) {
        _result += "${values[i].replaceAll("'", "")}, ";
      } else {
        _result += "${values[i].replaceAll("'", "")}";
      }
    }
    _result = _result.trim();
    return _result.isNullOrEmpty ? TextData.noPreference : _result;
  }

  String _getPrefGenderString() {
    String _result = "";
    for (int i = 0; i < _preferredGender.length; i++) {
      if (i != _preferredGender.length - 1) {
        _result += "${GenderStatus.genderMap[_preferredGender[i]]}, ";
      } else {
        _result += "${GenderStatus.genderMap[_preferredGender[i]]}";
      }
    }
    _result = _result.trim();
    return _result.isNullOrEmpty ? TextData.noPreference : _result;
  }

  void _setMyGender(int index, bool value) {
    switch (index) {
      case GenderStatus.male:
        _isMale = value;
        _isFemale = false;
        _isTransgender = false;
        _isGenderqueer = false;
        _isAgender = false;
        /// Set preferred gender to opposite sex
        _preferredGender.clear();
        _preferredGender.add(GenderStatus.female);
        break;
      case GenderStatus.female:
        _isFemale = value;
        _isMale = false;
        _isTransgender = false;
        _isGenderqueer = false;
        _isAgender = false;
        /// Set preferred gender to opposite sex
        _preferredGender.clear();
        _preferredGender.add(GenderStatus.male);
        break;
      case GenderStatus.transgender:
        _isTransgender = value;
        _isMale = false;
        _isFemale = false;
        _isGenderqueer = false;
        _isAgender = false;
        break;
      case GenderStatus.genderQueer:
        _isGenderqueer = value;
        _isMale = false;
        _isFemale = false;
        _isTransgender = false;
        _isAgender = false;
        break;
      case GenderStatus.agender:
        _isAgender = value;
        _isMale = false;
        _isFemale = false;
        _isTransgender = false;
        _isGenderqueer = false;
        break;
    }
  }

  Widget _genderChoice(int index, customState) {

    bool _getValue() {
      switch (index) {
        case GenderStatus.male:
          return _isMale;
        case GenderStatus.female:
          return _isFemale;
        case GenderStatus.transgender:
          return _isTransgender;
        case GenderStatus.genderQueer:
          return _isGenderqueer;
        case GenderStatus.agender:
          return _isAgender;
        default:
          return false;
      }
    }

    return CheckboxListTile(
      activeColor: appBgColor,
      checkColor: sharp_green,

      title: Text(
        GenderStatus.genderMap.values.toList()[index],
        style: commonTextStyle,
      ),
      onChanged: (bool value) {
        customState(() {
          _setMyGender(index, value);
        });
        if (value) {
          _myGender = GenderStatus.genderMap.keys.toList()[index];
        } else {
          _myGender = null;
        }
      },
      value: _getValue(),
    );
  }

  void _setOriginalPrefExperience() {
    if (myPreference.isGreenHead) {
      _preferGreenHead = true;
    }
  }

  Widget _prefExperienceBox(int index, customState) {

    bool _getValue() {
      switch (index) {
        case 0:
          return _preferGreenHead;
        case 1:
          return _preferNonGreenHead;
        default:
          return false;
      }
    }

    return CheckboxListTile(
      activeColor: appBgColor,
      checkColor: sharp_green,
      title: Text(
        [TextData.prefExperienceBefore, TextData.prefNoExperienceBefore][index],
        style: commonTextStyle,
      ),
      onChanged: (bool value) {
        customState(() {
          switch (index) {
            case 0:
              _preferGreenHead = value;
              break;
            case 1:
              _preferNonGreenHead = value;
              break;
          }
        });
        /// When _preferredIsGreenHead = null, it means this user does not have any preference regarding partner's experience
        if ((_preferGreenHead && _preferNonGreenHead) || (!_preferGreenHead && !_preferNonGreenHead)) {
          _preferredIsGreenHead = null;
        } else {
          _preferredIsGreenHead = _preferGreenHead;
        }
      },
      value: _getValue(),
    );
  }

  Widget _experienceBox(int index, customState) {

    bool _getValue() {
      switch (index) {
        case 0:
          return _hasExperienceBefore;
        case 1:
          return _noExperienceBefore;
        default:
          return false;
      }
    }

    return CheckboxListTile(
      activeColor: appBgColor,
      checkColor: sharp_green,

      title: Text(
        [TextData.hasExperienceBefore, TextData.noExperienceBefore][index],
        style: commonTextStyle,
      ),
      onChanged: (bool value) {
        customState(() {
          switch (index) {
            case 0:
              _noExperienceBefore = false;
              _hasExperienceBefore = value;
              break;
            case 1:
              _noExperienceBefore = value;
              _hasExperienceBefore = false;
              break;
          }
        });
        if (!_noExperienceBefore && !_hasExperienceBefore) {
          _noLoveExperience = null;
        } else {
          _noLoveExperience = _hasExperienceBefore;
        }
      },
      value: _getValue(),
    );
  }

  Widget _nextButton(Function onPressed, bool isAlone, String title) {
    return SafeArea(
      child: LongElevatedButton(
        padding: EdgeInsets.only(bottom: isAlone ? 30 : 12),
        title: title,
        onPressed: onPressed,
      ),
    );
  }

  Widget _bottomNextButton(Function onPressed, {String title = TextData.next}) {
    return Expanded(
      child: Align(
        alignment: Alignment.bottomCenter,
        child: _nextButton(() {
          onPressed();
        }, true, title,
        ),
      ),
    );
  }

  void _pageTwoComplete() {
    if (_formTwoKey.currentState.validate()) {
      _otpFocusNode.unfocus();
      Dialogs.showLoadingDialog();
      apiManager.checkOtp(_otpController.text).then((jsonResponse) async {
        _otpController.clear();
        var jsonStatus = jsonResponse[DataKey.status];
        bool isOtpCorrect = jsonStatus == StatusCode.ok;
        if (isOtpCorrect) {
          if (isExistingMember) {
            await apiManager.getMyProfileAndPreference(email: _emailController.text).then((_) async {
              if (_alreadyHasPreference) {
                debugPrint("Exist profile and preference");
                /// Exist profile and preference
                await apiManager.signInToFirebase();
                getRoute.pop();
                getRoute.navigateToAndPopAll(MainPage());
              } else {
                getRoute.pop();
                debugPrint("Exist profile, no preference");
                /// Exist profile, but no preference
                _pageController.jumpToPage(_prefBeginPage);
                utils.toast(TextData.finishPrefFirst, isWarning: true);
              }
            });
          } else {
            getRoute.pop();
            debugPrint("no profile, no preference");
            /// No profile, no preference
            _nextPage();
          }
        } else {
          getRoute.pop();
          if (jsonStatus == StatusCode.expiredOtp) {
            utils.toast(TextData.expiredOtp, iconData: warningIcon, bgColor: warning);
          } else {
            utils.toast(TextData.wrongOtp, iconData: warningIcon, bgColor: warning);
          }
        }
      });
    }
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget _headerWidget(Widget child, {bool showPreviousButton = true, String title, List<Widget> actions, bool needActions = true}) {

    if (_editPref || _editProfile) {
      showPreviousButton = false;
      if (needActions) {
        if (actions == null) {
          actions = [];
        }
        actions.add(TextButton(
          onPressed: () {
            refreshThisPage();
            if (_editProfile) {
              _pageController.animateToPage(_profileReviewPage, duration: _animationDuration, curve: _animationCurve);
            } else if (_editPref) {
              _pageController.animateToPage(_prefEndPage, duration: _animationDuration, curve: _animationCurve);
            }
          },
          child: Text(
            TextData.skip,
            style: commonTextStyle,
          ),
        ));
      }
    }

    return Scaffold(
      appBar: MyAppBar(
        title: title ?? "",
        bgColor: appBgColor,
        toolbarHeight: kToolbarHeight * 1.5,
        elevation: 0,
        automaticallyImplyLeading: false,
        leading: showPreviousButton ? _previousPageArrow : _cancelRegistrationButton(),
        actions: actions,
      ),
      body: child,
    );
  }

  int _validImageCount() {
    int count = 0;
    _imageFiles.forEach((image) {
      if (image != null) count++;
    });
    return count;
  }

  @override
  Widget build(BuildContext context) {

    _pages = [
      /// Page 1
      _headerWidget(
          Form(
            key: _formOneKey,
            child: ScrollableFooter(
              children: [
                _titleDisplay(TextData.whatIsYourEmail),
                _textDisplay(isExistingMember ? TextData.retrieveAccountByEmail : TextData.whyNeedEmail),
                EmailTextField(
                  controller: _emailController,
                  focusNode: _emailFocusNode,
                ),
                _bottomNextButton(() async {

                  void _positiveAction() async {
                    await apiManager.getVerificationCode(_emailController.text).then((jsonResponse) {
                      getRoute.pop();
                      int jsonStatus = jsonResponse[DataKey.status];
                      double secDiff;
                      bool isSentSuccess = false;
                      switch (jsonStatus) {
                        case StatusCode.ok:
                          apiManager.logAnalysis(Analysis.getOtp);
                          isSentSuccess = true;
                          utils.toast(TextData.emailSent, isSuccess: true);
                          break;
                        case StatusCode.tooManyEmail:
                          secDiff = jsonResponse[DataKey.secDiff];
                          utils.toast(TextData.waitBeforeSend + utils.determineNeedS(secDiff.round(), "second"), isWarning: true);
                          break;
                        default:
                          utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
                      }
                      if (isSentSuccess) {
                        _nextPage();
                      }
                    });
                  }

                  if (_formOneKey.currentState.validate()) {
                    _emailFocusNode.unfocus();
                    Dialogs.showLoadingDialog(title: TextData.gettingCode);
                    await apiManager.isEmailAvailable(_emailController.text).then((List<bool> listOfBool) {
                      bool isEmailHasProfile = listOfBool.first;
                      _alreadyHasPreference = listOfBool.last;
                      if (isExistingMember) {
                        /// If the user is trying to login
                        if (!isEmailHasProfile) {
                          getRoute.pop();
                          /// If the provided email does not exist in backend,
                          /// prompt the user to register first
                          apiManager.logAnalysis(Analysis.fillRegisterEmail);
                          getBinaryDialog(TextData.accountNotFound, TextData.accountNotFoundMessage, () {
                            getRoute.navigateToAndPopAll(SetupPage(
                              isExistingMember: false,
                              prefillEmail: _emailController.text,
                              isNewRegister: true,
                            ));
                          }, negativeText: TextData.close,);
                        } else {
                          /// If the provided email is found,
                          /// send otp and go to next page
                          /// LOGGING IN MUST PROVIDE OTP
                          _positiveAction();
                          apiManager.logAnalysis(Analysis.fillLoginEmail);
                        }
                      } else {
                        /// If the user is trying to create an account
                        if (isEmailHasProfile) {
                          getRoute.pop();
                          /// If the email is found in our backend
                          /// prompt the user to log in
                          getBinaryDialog(TextData.accountFound, TextData.accountFoundMessage, () async {
                            Dialogs.showLoadingDialog(title: TextData.fetchingAccount);
                            isExistingMember = true;
                            apiManager.logAnalysis(Analysis.fillLoginEmail);
                            _positiveAction();
                          }, negativeText: TextData.close,);
                        } else {
                          /// If email is not found,
                          /// continue to create profile
//                          _positiveAction();
                          apiManager.logAnalysis(Analysis.fillRegisterEmail);
                          /// CREATE NEW PROFILE DOES NOT NEED OTP
                          /// 1. pop loading
                          getRoute.pop();
                          /// 2. skip otp page
                          /// 3. skip fill pref relationship page as well
                          _skipPage(
                            targetIndex: _profileNamePage,
                          );
                        }
                      }
                    });
                  }
                },),
              ],
            ),
          ),
          showPreviousButton: false
      ),
      /// Page 2
      _headerWidget(
          Form(
            key: _formTwoKey,
            child: ScrollableFooter(
              children: [
                _titleDisplay(TextData.otp),
                _textDisplay("${TextData.sentOtp}${_emailController.text}."),
                OtpTextField(
                  controller: _otpController,
                  focusNode: _otpFocusNode,
                  onCompleted: (String completedValue) {
                    _pageTwoComplete();
                  },
                ),
                Center(
                  child: LongElevatedButton(
                    key: _resendBtnKey,
                    isEnabled: true,
                    width: 140,
                    height: 40,
                    borderRadius: 8.0,
                    color: unfocusedColor,
                    onPressed: () async {
                      _resendBtnKey.currentState?.disableButton();
                      Dialogs.showLoadingDialog();
                      await apiManager.getVerificationCode(_emailController.text).then((jsonResponse) {
                        getRoute.pop();
                        int jsonStatus = jsonResponse[DataKey.status];
                        double secDiff;
                        switch (jsonStatus) {
                          case StatusCode.ok:
                            apiManager.logAnalysis(Analysis.resendOtp);
                            utils.toast(TextData.emailSent, isSuccess: true);
                            break;
                          case StatusCode.tooManyEmail:
                            secDiff = jsonResponse[DataKey.secDiff];
                            utils.toast(TextData.waitBeforeSend + utils.determineNeedS(secDiff.round(), "second"), isWarning: true);
                            break;
                          default:
                            utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
                        }
                      });
                      Future.delayed(Duration(seconds: 2), () {
                        _resendBtnKey.currentState?.enableButton();
                      });
                    },
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            TextData.resend,
                            style: longButtonTextStyle,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                _bottomNextButton(() {
                  _pageTwoComplete();
                }),
              ],
            ),
          ), showPreviousButton: false
      ),
//      /// Page 3
//      _headerWidget(ScrollableFooter(
//        children: [
//          _titleDisplay(TextData.startCreateProfile),
//          _bottomNextButton(() {
//            _nextPage();
//            apiManager.logAnalysis(Analysis.startCreateProfile);
//          }, title: TextData.start),
//        ],
//      ), showPreviousButton: false,
//      ),
      /// Page 3
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.yourPrefRelationship),
                _preferredRelationshipBox(PreferredRelationshipStatus.marriage, customState),
                _preferredRelationshipBox(PreferredRelationshipStatus.longTerm, customState),
                _preferredRelationshipBox(PreferredRelationshipStatus.causal, customState),
                _preferredRelationshipBox(PreferredRelationshipStatus.noPreference, customState),
                _textDisplay(TextData.prefRelationshipTip),
                _buttonColumn(onNextPressed: () {
                  _nextPage();
                  apiManager.logAnalysis(Analysis.fillPref);
                }, needBackButton: false),
              ],
            );
          },
        ),
        showPreviousButton: false,
      ),
      /// Page 4
      _headerWidget(
        Form(
          key: _formThreeKey,
          child: ScrollableFooter(
            children: [
              _titleDisplay(TextData.whatIsYourName),
              GenericTextField(
                controller: _firstNameController,
                focusNode: _firstNameFocusNode,
                nextFocusNode: _lastNameFocusNode,
                hint: TextData.firstName,
                textInputType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                inputFormatter: [
                  FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                ],
                autofillHints: [AutofillHints.givenName],
                validation: (String newValue) {
                  if (newValue.isNullOrEmpty) {
                    return "${TextData.name} must not be empty";
                  }
                  if (newValue.contains(" ")) {
                    return "${TextData.name} must not contain white space";
                  }
                  return null;
                },
              ),
              GenericTextField(
                controller: _lastNameController,
                focusNode: _lastNameFocusNode,
                nextFocusNode: _firstNameFocusNode,
                hint: TextData.lastName,
                textInputType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                inputFormatter: [
                  FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]+')),
                ],
                autofillHints: [AutofillHints.familyName],
                validation: (String newValue) {
                  if (newValue.isNullOrEmpty) {
                    return "${TextData.name} must not be empty";
                  }
                  if (newValue.contains(" ")) {
                    return "${TextData.name} must not contain white space";
                  }
                  return null;
                },
              ),
              _buttonColumn(onNextPressed: () {
                if (_formThreeKey.currentState.validate()) {
                  _firstNameFocusNode.unfocus();
                  _lastNameFocusNode.unfocus();
                  apiManager.logAnalysis(Analysis.fillName);
                  _nextPage();
                }
              }, needBackButton: !_isNewRegister),
            ],
          ),
        ), showPreviousButton: !_isNewRegister,
      ),
      /// Page 5
      _headerWidget(
        Form(
          key: _formFourKey,
          child: ScrollableFooter(
            children: [
              _titleDisplay(TextData.whereDoYouLive),
              GenericTypeAheadTextField(
                controller: _countryController,
                focusNode: _countryFocusNode,
                nextFocusNode: _cityFocusNode,
                hint: TextData.country,
                dataList: countryList,
              ),
              GenericTextField(
                controller: _cityController,
                focusNode: _cityFocusNode,
                nextFocusNode: _countryFocusNode,
                hint: TextData.city,
                autofillHints: [AutofillHints.addressCity],
              ),
              _buttonColumn(onNextPressed: () async {
                try {
                  if (!countryList.contains(_countryController.text)) {
                    utils.toast(TextData.noCountry, isWarning: true);
                  } else if (_formFourKey.currentState.validate()) {
                    if (_latitude == null || _longitude == null) {
                      List<Location> _confirmLocation = await locationFromAddress(_cityController.text + ", " + _countryController.text);
                      if (_latitude == null) {
                        _latitude = _confirmLocation.first.latitude;
                      }
                      if (_longitude == null) {
                        _longitude = _confirmLocation.first.longitude;
                      }
                    }
                    _countryFocusNode.unfocus();
                    _cityFocusNode.unfocus();
                    if (_latitude == null || _longitude == null) {
                      throw Exception("Location unavailable");
                    }
                    apiManager.logAnalysis(Analysis.getLocationBtn);
                    _nextPage();
                  }
                } on Exception catch (e) {
                  utils.toast(TextData.noSuchPlace, isWarning: true);
                }
              }),
            ],
          ),
        ),
      ),
      /// Page 6
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.whatIsYourGender),
                _genderChoice(0, customState),
                _genderChoice(1, customState),
                _genderChoice(2, customState),
                _genderChoice(3, customState),
                _genderChoice(4, customState),
                _buttonColumn(onNextPressed: () {
                  if (_myGender != null) {
                    apiManager.logAnalysis(Analysis.fillGender);
                    _nextPage();
                  } else {
                    utils.toast(TextData.selectGender, isWarning: true);
                  }
                }),
              ],
            );
          },
        ),
      ),
      /// Page 7
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.whatIsYourBirthday),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      _myBDay.year.toString() + "-",
                      style: bigTextStyle,
                    ),
                    Text(
                      _myBDay.month.toString() + "-",
                      style: bigTextStyle,
                    ),
                    Text(
                      _myBDay.day.toString(),
                      style: bigTextStyle,
                    ),
                    Container(width: 10,),
                    IconButton(
                      icon: Icon(
                        Icons.edit,
                        color: mainColor,
                      ),
                      onPressed: () {
                        customDate.showCustomDatePicker(
                          context: getRoute.getContext(),
                          initialDate: _myBDay,
                          firstDate: DateTime(1950, 1, 1),
                          lastDate: _prefillBDay,
                          cancelText: TextData.cancel,
                          confirmText: TextData.confirm,
                          initialEntryMode: customDate.DatePickerEntryMode.calendar,
                          initialDatePickerMode: customDate.DatePickerMode.year,
                        ).then((DateTime date) {
                          if (date != null) {
                            _myBDay = date;
                          }
                          customState(() {
                            _birthday = timeUtils.convertDateTimeDisplay(_myBDay);
                          });
                        });
                      },
                    ),
                  ],
                ),
                _buttonColumn(onNextPressed: () {
                  if (utils.isAdult(_myBDay)) {
                    _myZodiacSign = utils.getZodiacSign(_myBDay);
                    _myAge = utils.getAge(_myBDay);
                    _birthday = timeUtils.convertDateTimeDisplay(_myBDay);
                    apiManager.logAnalysis(Analysis.fillBDay);
                    _nextPage();
                  } else {
                    utils.toast(TextData.mustAdult, isWarning: true);
                  }
                }),
              ],
            );
          },
        ),
      ),
      /// Page 8
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.haveYouEverBeenInARelationship),
                _experienceBox(0, customState),
                _experienceBox(1, customState),
                _buttonColumn(onNextPressed: () {
                  if (_noLoveExperience != null) {
                    apiManager.logAnalysis(Analysis.fillExp);
                    if (_isNewRegister) {
                      _skipPage(
                        targetIndex: _profileImagePage,
                      );
                    } else {
                      _nextPage();
                    }
                  } else {
                    utils.toast(TextData.checkABox, isWarning: true);
                  }
                }),
              ],
            );
          },
        ),
      ),
      /// Page 9
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.whatIsYourEthnicityAndReligion),
                GenericStringDropDownField(
                  text: TextData.ethnicity,
                  onChanged: (newValue) {
                    customState(() {
                      _myEthnicity = newValue;
                    });
                  },
                  list: ethnicityList,
                  value: _myEthnicity,
                ),
                Container(height: 20,),
                GenericStringDropDownField(
                  text: TextData.religion,
                  onChanged: (newValue) {
                    customState(() {
                      _myReligion = newValue;
                    });
                  },
                  list: religionList,
                  value: _myReligion,
                ),
                _buttonColumn(onNextPressed: () {
                  if (_myEthnicity == null) {
                    utils.toast(TextData.selectEthnicity, isWarning: true);
                  } else if (_myReligion == null) {
                    utils.toast(TextData.selectReligion, isWarning: true);
                  } else {
                    apiManager.logAnalysis(Analysis.fillEthnAndReli);
                    _nextPage();
                  }
                }),
              ],
            );
          },
        ),
      ),
      /// Page 10
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return Form(
              key: _formFiveKey,
              child: ScrollableFooter(
                children: [
                  _titleDisplay(TextData.whatIsYourEducation),
                  GenericStringDropDownField(
                    text: TextData.educationLevel,
                    onChanged: (newValue) {
                      customState(() {
                        _myEducationLevel = newValue;
                      });
                    },
                    list: educationLevelList,
                    value: _myEducationLevel,
                  ),
                  Container(height: 20,),
                  GenericTypeAheadTextField(
                    controller: _schoolController,
                    focusNode: _schoolFocusNode,
                    hint: TextData.nameOfSchool,
                    dataList: schoolList,
                  ),
                  _buttonColumn(onNextPressed: () {
                    if (_formFiveKey.currentState.validate()) {
                      if (_myEducationLevel == null) {
                        utils.toast(TextData.selectEducationLevel, isWarning: true);
                      } else {
                        apiManager.logAnalysis(Analysis.fillEdu);
                        _schoolFocusNode.unfocus();
                        _nextPage();
                      }
                    }
                  }),
                ],
              ),
            );
          },
        ),
      ),
      /// Page 11
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return Form(
              key: _formSixKey,
              child: ScrollableFooter(
                children: [
                  _titleDisplay(TextData.whatIsYourProfession),
                  GenericTypeAheadTextField(
                    controller: _professionController,
                    focusNode: _professionFocusNode,
                    hint: TextData.profession,
                    dataList: professionList,
                  ),
                  _buttonColumn(onNextPressed: () {
                    if (_formSixKey.currentState.validate()) {
                      apiManager.logAnalysis(Analysis.fillProfession);
                      _professionFocusNode.unfocus();
                      _nextPage();
                    }
                  }),
                ],
              ),
            );
          },
        ),
      ),
      /// Page 12
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return Form(
              key: _formSevenKey,
              child: ScrollableFooter(
                children: [
                  _titleDisplay(TextData.howWouldYouDescribeYourself),
                  GenericTextField(
                    controller: _personalityController,
                    focusNode: _personalityFocusNode,
                    nextFocusNode: _hobbyFocusNode,
                    hint: TextData.personality,
                    textInputType: TextInputType.multiline,
                    maxLines: null,
                    maxLength: 250,
                  ),
                  GenericTypeAheadTextField(
                    controller: _hobbyController,
                    focusNode: _hobbyFocusNode,
                    nextFocusNode: _personalityFocusNode,
                    hint: TextData.hobby,
                    dataList: hobbyList,
                  ),
                  _buttonColumn(onNextPressed: () {
                    if (_formSevenKey.currentState.validate()) {
                      apiManager.logAnalysis(Analysis.fillHobby);
                      _personalityFocusNode.unfocus();
                      _hobbyFocusNode.unfocus();
                      _nextPage();
                    }
                  }),
                ],
              ),
            );
          },
        ),
      ),
      /// Page 13
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.pickColorLong),
                Center(
                  child: ColorPicker(
                    onPressed: onSelectColor,
                    customState: customState,
                    currentColorCode: currentColorCode,
                  ),
                ),
                _buttonColumn(onNextPressed: () {
                  if (currentColorCode != null) {
                    apiManager.logAnalysis(Analysis.fillColor);
                    _nextPage();
                  }
                }),
              ],
            );
          },
        ),
      ),
      /// Page 14
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(_editProfile ? TextData.skipEditImage : TextData.uploadImageTitle),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  child: Container(
                    width: deviceWidth,
                    height: deviceWidth,
                    child: GridView.builder(
                      shrinkWrap: true,
                      addAutomaticKeepAlives: false,
                      primary: false,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        childAspectRatio: 9 / 14,
                      ),
                      itemCount: allowedProfileMedia,
                      itemBuilder: (BuildContext ctx, int index) {

                        String _imageLink;

                        if (_editProfile) {
                          if (index > _tempProfile.images.length - 1) {
                            _imageLink = placeholderLink;
                          } else {
                            _imageLink = _tempProfile.images[index];
                          }
                        }

                        File _imageFile;
                        if (_imageFiles.length > index) {
                          _imageFile = _imageFiles[index];
                        }

                        Future<void> _handleNewImage(ImageSource source) async {
                          PickedFile _newImage = await _picker.getImage(source: source);
                          if (_newImage != null) {
                            _imageFile = File(_newImage.path);
                            _imageFiles.removeAt(index);
                            _imageFiles.insert(index, _imageFile);
                            customState(() {});
                          }
                        }

                        return Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: _editProfile ? utils.getColorByHex(myProfile.color) : utils.getColorByHex(currentColorCode) ?? appIconColor,
                            ),
                            borderRadius: BorderRadius.circular(imageRadius),
                          ),
                          child: GestureDetector(
                            onTap: () async {
                              getGenericBottomSheet([
                                Stack(
                                  children: [
                                    Container(
                                      height: deviceHeight,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 120,
                                          ),
                                          GestureDetector(
                                            onTap: () async {
                                              getRoute.pop();
                                              await _handleNewImage(ImageSource.camera);
                                            },
                                            child: Container(
                                              width: deviceWidth * 0.9,
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(commonRadius),
                                                child: Image.asset(
                                                  "assets/from_camera.jpg",
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          GestureDetector(
                                            onTap: () async {
                                              getRoute.pop();
                                              await _handleNewImage(ImageSource.gallery);
                                            },
                                            child: Container(
                                              width: deviceWidth * 0.9,
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(commonRadius),
                                                child: Image.asset(
                                                  "assets/from_gallery.jpg",
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          LongElevatedButton(
                                            title: TextData.removeImage,
                                            color: warning,
                                            onPressed: () {
                                              getRoute.pop();
                                              if (_editProfile) {
                                                if (_tempProfile.images.length - 1 >= index) {
                                                  _tempProfile.images[index] = placeholderLink;
                                                }
                                              }
                                              if (_imageFile != null) {
                                                _imageFiles.removeAt(index);
                                                _imageFile = null;
                                                _imageFiles.insert(index, null);
                                              }
                                              customState(() {});
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                    Positioned.fill(
                                      top: 40,
                                      child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            IconButton(
                                              onPressed: () => getRoute.pop(),
                                              icon: Icon(
                                                Icons.close,
                                                color: mainColor,
                                                size: 32,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: 12.0),
                                              child: Text(
                                                TextData.insertImage,
                                                style: bigTextStyle.merge(
                                                    TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 30
                                                    )
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: 12.0),
                                              child: Text(
                                                TextData.pickSource,
                                                style: commonTextStyle,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ], needTopIndicator: false, fullScreen: true);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(imageRadius),
                              child: _imageFile != null ? Image.file(
                                _imageFile,
                                fit: BoxFit.cover,
                              ) : _editProfile ? CachedNetworkImage(
                                imageUrl: _imageLink,
                                fit: BoxFit.cover,
                              ) : Image.asset(
                                placeholder,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Container(height: _editProfile ? 0 : 20,),
                _editProfile ? Container() : _textDisplay(TextData.uploadImageTip),
                Container(height: 10,),
                _buttonColumn(onNextPressed: () async {

                  int _faceCount = 0;
                  int _minPic = (allowedProfileMedia / 3).floor();
                  if (!_editProfile) {
                    _setVerificationInstruction();
                    if (_validImageCount() < _minPic) {
                      utils.toast("Please upload at least $_minPic images", isWarning: true);
                      return;
                    } else if (checkFace) {
                      Dialogs.showLoadingDialog(title: TextData.checking);
                      for (int i = 0; i < _imageFiles.length; i++) {
                        if (_imageFiles[i] != null) {
                          try {
                            _visionImage = FirebaseVisionImage.fromFile(_imageFiles[i]);
                            List<Face> _faces = await _faceDetector.processImage(_visionImage);
                            _faceCount += _faces.length;
                          } on Exception catch (e) {
                            _faceCount = 1;
                          }
                        }
                      }
                      getRoute.pop();
                      if (_faceCount == 0) {
                        utils.toast(TextData.noFace, isWarning: true);
                        return;
                      }
                    }
                  }
                  apiManager.logAnalysis(Analysis.uploadImage);
                  if (_isNewRegister) {
                    _skipPage(targetIndex: _profileReviewPage);
                  } else {
                    _nextPage();
                  }
                }, needBackButton: !_editProfile),
              ],
            );
          },
        ), title: TextData.uploadImages, actions: [
        IconButton(
          onPressed: () {
            getBinaryDialog(TextData.clearAllMedia, TextData.areYouSure, () {
              if (_editProfile) {
                _tempProfile.images = List.generate(allowedProfileMedia, (index) => placeholderLink);
              }
              setImageFilesListToNull();
              refreshThisPage();
            });
          },
          icon: Icon(
            Icons.delete,
            color: mainColor,
          ),
        ),
      ], showPreviousButton: !_editProfile,
      ),
      /// Page 15
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(_editProfile ? TextData.skipEditVideo : TextData.uploadVideoTitle),
                GestureDetector(
                  onTap: () async {
                    FilePickerResult _result = await FilePicker.platform.pickFiles(
                      type: FileType.video,
                    );
                    if (_result != null) {
                      _videoFile = File(_result.files.single.path);
                    }
                    customState(() {});
                  },
                  child: _videoFile != null ? VideoDownloadThumbnail(
                    videoFile: _videoFile,
                  ) : _editProfile && !myProfile.video.isNullOrEmpty ? VideoUrlThumbnail(
                    videoUrl: myProfile.video,
                    originalUrl: myProfile.video,
                    thumbnail: myProfile.videoThumnbnail,
                  ) : Image.asset(
                    videoPlaceholder,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(height: 10,),
                _buttonColumn(onNextPressed: () {
                  apiManager.logAnalysis(Analysis.uploadVideo);
                  _nextPage();
                }),
              ],
            );
          },
        ), title: TextData.uploadVideo, actions: [
        IconButton(
          onPressed: () {
            getBinaryDialog(TextData.removeVideo, TextData.areYouSure, () {
              if (_editProfile) {
                _tempProfile.video = null;
                _tempProfile.videoThumnbnail = null;
              }
              _videoFile = null;
              refreshThisPage();
            });
          },
          icon: Icon(
            Icons.delete,
            color: mainColor,
          ),
        ),
      ],
      ),
      /// Page 16
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {

            Future<void> _pickImage() async {
              if (_verificationImage != null) {
                getBinaryDialog(TextData.removeFile, TextData.areYouSure, () {
                  _verificationImage = null;
                  refreshThisPage();
                });
              } else {
                PickedFile _newImage = await _picker.getImage(
                  source: ImageSource.camera,
                );
                if (_newImage != null) {
                  _verificationImage = File(_newImage.path);
                  customState(() {});
                }
              }
            }

            return ScrollableFooter(
              children: [
                _titleDisplay(_editProfile ? TextData.skipVerImage : TextData.verificationImage),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: _textDisplay(TextData.requirement + ": " + _verificationInstruction)
                      ),
                      _editProfile ? Container() : IconButton(
                        onPressed: () {
                          _setVerificationInstruction();
                          _verificationImage = null;
                          refreshThisPage();
                        },
                        icon: Icon(
                          Icons.refresh,
                          color: mainColor,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(height: 10,),
                InkWell(
                  onTap: () async {
                    await _pickImage();
                  },
                  child: _verificationImage != null ? Image.file(
                    _verificationImage,
                    fit: BoxFit.cover,
                  ) : _editProfile ? CachedNetworkImage(
                    imageUrl: myProfile.verificationImage,
                    fit: BoxFit.cover,
                  ) : Image.asset(
                    placeholder,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(height: 20,),
                _buttonColumn(onNextPressed: () {
                  if (_verificationImage != null) {
                    apiManager.logAnalysis(Analysis.uploadVerificationImage);
                  }
                  _nextPage();
                }),
              ],
            );
          },
        ), actions: [
          IconButton(
              onPressed: () async {
                final picker = ImagePicker();
                if (_verificationImage != null) {
                  getBinaryDialog(TextData.removeFile, TextData.areYouSure, () {
                    _verificationImage = null;
                    refreshThisPage();
                  });
                } else {
                  PickedFile _pickedFile = await picker.getImage(
                    source: ImageSource.camera,
                  );
                  if (_pickedFile != null) {
                    _verificationImage = File(_pickedFile.path);
                  }
                  refreshThisPage();
                }
              },
              icon: Icon(
                Icons.camera,
                color: mainColor,
              )
          ),
          _editProfile ? Container() : IconButton(
            onPressed: () {
              _verificationImage = null;
              refreshThisPage();
            },
            icon: Icon(
              Icons.delete,
              color: mainColor,
            ),
          ),
        ],
      ),
      /// Page 17
      _headerWidget(
        ScrollableFooter(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            _isNewRegister ? Container() : ReviewTitle(title: TextData.preferredRelationship,),
            _isNewRegister ? Container() : ReviewCard(children: [
              _reviewInformation("${PreferredRelationshipStatus.relationshipMap[_preferredRelationship]}", _profileBeginPage, needLastValue: false,),
            ]),
            ReviewTitle(title: TextData.personalInformation,),
            ReviewCard(
              children: [
                _reviewInformation(TextData.firstName, _profileNamePage, lastValue: _firstNameController.text.capitalize),
                _reviewInformation(TextData.lastName, _profileNamePage, lastValue: _lastNameController.text.capitalize),
                _reviewInformation(TextData.gender, _profileGenderPage, lastValue: GenderStatus.genderMap[_myGender]),
                _reviewInformation(TextData.birthday, _profileBdayPage, lastValue: _birthday),
              ],
            ),
            ReviewTitle(title: TextData.location,),
            ReviewCard(
              children: [
                _reviewInformation(TextData.country, _profileLocationPage, lastValue: _countryController.text),
                _reviewInformation(TextData.city, _profileLocationPage, lastValue: _cityController.text),
              ],
            ),
            _isNewRegister ? Container() : ReviewTitle(title: TextData.educationAndCareer,),
            _isNewRegister ? Container() : ReviewCard(
              children: [
                _reviewInformation(TextData.educationLevel, _profileEducationPage, lastValue: _myEducationLevel),
                _reviewInformation(TextData.nameOfSchool, _profileEducationPage, lastValue: _schoolController.text),
                _reviewInformation(TextData.profession, _profileProfessionPage, lastValue: _professionController.text),
              ],
            ),
            ReviewTitle(title: TextData.aboutYourself,),
            ReviewCard(
              children: [
                _reviewInformation(_noLoveExperience ?? false ? TextData.notGreenHead : TextData.greenHead, _profileGreenHeadPage, needLastValue: false,),
                _isNewRegister ? Container() : _reviewInformation(TextData.ethnicity, _profileEthnicityAndReligionPage, lastValue: _myEthnicity),
                _isNewRegister ? Container() : _reviewInformation(TextData.religion, _profileEthnicityAndReligionPage, lastValue: _myReligion),
                _isNewRegister ? Container() : _reviewInformation(_personalityController.text, _profilePersonalPage, needLastValue: false,),
                _isNewRegister ? Container() : _reviewInformation(TextData.hobby, _profilePersonalPage, lastValue: _hobbyController.text),
              ],
            ),
            ReviewTitle(title: TextData.media,),
            ReviewCard(
              children: [
                _reviewInformation(_validImageCount() > 0 ? utils.determineNeedS(_validImageCount(), "Image") : TextData.noImage, _profileImagePage, needLastValue: false,),
                _isNewRegister ? Container() : _reviewInformation(_videoFile == null ? TextData.noVideo : TextData.hasVideo, _profileVideoPage, needLastValue: false,),
              ],
            ),
            _isNewRegister ? Container() : ReviewTitle(title: TextData.color,),
            _isNewRegister ? Container() : ReviewCard(
              children: [
                _reviewInformation(currentColorCode, _profileColorPage, needLastValue: false, extraWidget: Padding(
                  padding: EdgeInsets.only(right: 12.0),
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      color: utils.getColorByHex(currentColorCode),
                      borderRadius: BorderRadius.circular(32),
                    ),
                  ),
                )),
              ],
            ),
            Container(height: 15,),
            _buttonColumn(nextTitle: _editProfile ? TextData.doneEditing : TextData.done, onNextPressed: () async {
              if (_editProfile) {
                Dialogs.showLoadingDialog(title: TextData.editingProfile);
                await apiManager.updateProfile(
                  _firstNameController.text.trim().capitalize,
                  _lastNameController.text.trim().capitalize,
                  _emailController.text.trim().toLowerCase(),
                  _countryController.text.trim(),
                  _cityController.text.trim(),
                  _longitude,
                  _latitude,
                  _myGender,
                  currentColorCode,
                  _birthday,
                  _myAge,
                  _myEthnicity ?? "",
                  _myReligion ?? "",
                  _myZodiacSign,
                  _professionController.text?.trim() ?? "",
                  _schoolController.text?.trim() ?? "",
                  _myEducationLevel ?? "",
                  _personalityController.text?.trim() ?? "",
                  _preferredRelationship,
                  _noLoveExperience,
                  _hobbyController.text?.trim() ?? "",
                  _verificationImage == null ? myProfile.verificationInstruction : _verificationInstruction,
                ).then((jsonResponse) async {
                  await _updateMedia();
                  getRoute.truePop();
                  apiManager.logAnalysis(Analysis.editProfile);
                  widget.notifyParent();
                  getRoute.pop();
                  var jsonStatus = jsonResponse[DataKey.status];
                  if (jsonStatus == StatusCode.ok) {
                    utils.toast(TextData.editedProfile, isSuccess: true);
                  } else {
                    utils.toast(TextData.connectionFailed, isWarning: true);
                  }
                  apiManager.getMyProfileAndPreference();
                });
              } else {
                _nextPage();
              }
            }),
          ],
        ),
        needActions: false,
      ),
      /// Page 18
      _headerWidget(
        ScrollableFooter(
          children: [
            _titleDisplay(TextData.finishProfile),
            _bottomNextButton(() async {
              String password = utils.generatePassword();
              Dialogs.showLoadingDialog(title: TextData.creatingProfile);
              await apiManager.createAccountWithEmailAndPassword(
                _emailController.text.trim(),
                password,
              );
              apiManager.createProfile(
                _firstNameController.text.trim().capitalize,
                _lastNameController.text.trim().capitalize,
                _emailController.text.trim().toLowerCase(),
                password,
                _countryController.text.trim(),
                _cityController.text.trim(),
                _longitude,
                _latitude,
                _myGender,
                currentColorCode,
                _birthday,
                _myAge,
                _myEthnicity ?? "",
                _myReligion ?? "",
                _myZodiacSign,
                _professionController.text.trim(),
                _schoolController.text.trim(),
                _myEducationLevel ?? "",
                remoteConfig.getString(RemoteConfigKey.newUserPersonality) ?? "",
                _preferredRelationship,
                _noLoveExperience,
                _hobbyController.text.trim(),
                _verificationInstruction,
              ).then((jsonResponse) async {
                apiManager.logAnalysis(Analysis.createProfile);
                var jsonStatus = jsonResponse[DataKey.status];
                if (jsonStatus == StatusCode.ok) {
                  await apiManager.createUpdatePreference(
                    _editPref ? qp.email : _emailController.text,
                    _preferredGender,
                    _preferredDistance,
                    _preferredAge,
                    _preferredEthnicity,
                    _preferredReligion,
                    _preferredZodiacSign,
                    _preferredEducation,
                    _preferredHobby,
                    _preferredIsGreenHead,
                    _preferredRelationship,
                    _preferredLikeCount,
                  );
                  await apiManager.getMyProfileAndPreference();
                  await apiManager.signInToFirebase();
                  if (Platform.isAndroid && remoteConfig.getBool(RemoteConfigKey.androidBackgroundUpload)) {
                    _updateMediaInBackground();
                  } else {
                    await _updateMedia();
                  }
                  getRoute.pop();
                  /// Only new registration can reach this page
                  /// So directly go to main page
                  getRoute.navigateToAndPopAll(MainPage());
                } else if (jsonStatus == StatusCode.duplicateEmail) {
                  getRoute.pop();
                  _pageController.animateTo(_emailPage.toDouble(), duration: _animationDuration, curve: _animationCurve);
                  utils.toast(_emailController.text + TextData.duplicateEmail, isWarning: true);
                } else {
                  getRoute.pop();
                  utils.toast(TextData.connectionFailed, isWarning: true);
                }
              }).catchError((error) {
                debugPrint("Create profile error: $error");
                getRoute.pop();
              });
            }),
          ],
        ),
        needActions: false,
      ),
      /// Page 19
      _headerWidget(StatefulBuilder(
        builder: (ctx, customState) {
          return ScrollableFooter(
            children: [
              _titleDisplay(TextData.askPrefGender),
              _preferredGenderChoice(GenderStatus.male, customState),
              _preferredGenderChoice(GenderStatus.female, customState),
              _preferredGenderChoice(GenderStatus.transgender, customState),
              _preferredGenderChoice(GenderStatus.genderQueer, customState),
              _preferredGenderChoice(GenderStatus.agender, customState),
              _textDisplay(TextData.prefGenderTips),
              _bottomNextButton(() {
                apiManager.logAnalysis(Analysis.prefGender);
                _nextPage();
              }),
            ],
          );
        },
      ), showPreviousButton: false),
      /// Page 20
      _headerWidget(StatefulBuilder(
        builder: (ctx, customState) {
          return ScrollableFooter(
            children: [
              _titleDisplay(TextData.askPrefDistance),
              Slider.adaptive(
                value: _preferredDistanceValue,
                min: minDistance.toDouble(),
                max: maxDistance.toDouble(),
                divisions: maxDistance - minDistance,
                activeColor: cottonRed,
                inactiveColor: unfocusedColor,
                onChanged: (double value) {
                  customState(() {
                    _preferredDistanceValue = value;
                    _preferredDistance = _preferredDistanceValue.round();
                  });
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.remove,
                      color: mainColor,
                    ),
                    onPressed: () {
                      if (_preferredDistanceValue > minDistance) {
                        customState(() {
                          _preferredDistanceValue--;
                          _preferredDistance = _preferredDistanceValue.round();
                        });
                      }
                    },
                  ),
                  _textDisplay("${_preferredDistanceValue.round().toString()}$distanceSuffix"),
                  IconButton(
                    icon: Icon(
                      Icons.add,
                      color: mainColor,
                    ),
                    onPressed: () {
                      if (_preferredDistanceValue < maxDistance) {
                        customState(() {
                          _preferredDistanceValue++;
                          _preferredDistance = _preferredDistanceValue.round();
                        });
                      }
                    },
                  ),
                ],
              ),
              _buttonColumn(onNextPressed: () {
                apiManager.logAnalysis(Analysis.prefDistance);
                _nextPage();
              }),
            ],
          );
        },
      )),
      /// Page 21
      _headerWidget(StatefulBuilder(
        builder: (ctx, customState) {
          return ScrollableFooter(
            children: [
              _titleDisplay(TextData.askPrefAge),
              RangeSlider(
                values: _preferredAgeValue,
                min: adultAge.toDouble(),
                max: maxAge.toDouble(),
                divisions: maxAge - adultAge,
                activeColor: cottonRed,
                inactiveColor: unfocusedColor,
                onChanged: (RangeValues value) {
                  customState(() {
                    _preferredAgeValue = value;
                    _preferredAge = [_preferredAgeValue.start.round(), _preferredAgeValue.end.round()];
                  });
                },
              ),
              _textDisplay("${_preferredAgeValue.start.round()} - ${_preferredAgeValue.end.round()}"),
              _buttonColumn(onNextPressed: () {
                apiManager.logAnalysis(Analysis.prefAge);
                _nextPage();
              }),
            ],
          );
        },
      )),
      /// Page 22
      _headerWidget(_commonPreferenceChoiceWidget(
        TextData.askPrefEthnicity,
        TextData.ethnicity,
        _preferredEthnicity,
        _tempEthnicity,
        ethnicityList,
      )),
      /// Page 23
      _headerWidget(_commonPreferenceChoiceWidget(
        TextData.askPrefReligion,
        TextData.religion,
        _preferredReligion,
        _tempReligion,
        religionList,
      )),
      /// Page 24
      _headerWidget(_commonPreferenceChoiceWidget(
        TextData.askPrefZodiacSign,
        TextData.zodiacSign,
        _preferredZodiacSign,
        _tempZodiacSign,
        zodiacSignList,
      )),
      /// Page 25
      _headerWidget(_commonPreferenceChoiceWidget(
        TextData.askPrefEducation,
        TextData.educationLevel,
        _preferredEducation,
        _tempEducationLevel,
        educationLevelList,
      )),
      /// Page 26
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.askPrefHobby),
                GenericTypeAheadTextField(
                  dataList: hobbyList,
                  hint: TextData.hobby,
                  controller: _prefHobbyController,
                  trailing: IconButton(
                    onPressed: () {
                      if (!_prefHobbyController.text.isNullOrEmpty && hobbyList.contains(_prefHobbyController.text)) {
                        customState(() {
                          String _value = "'${_prefHobbyController.text}'";
                          if (!_preferredHobby.contains(_value)) {
                            _preferredHobby.add(_value);
                            _prefHobbyController.clear();
                          }
                        });
                      } else if (_prefHobbyController.text.isNullOrEmpty) {
                        utils.toast(TextData.hobbyIsEmpty, isWarning: true);
                      } else if (!hobbyList.contains(_prefHobbyController.text)) {
                        utils.toast(TextData.noSuchHobby, isWarning: true);
                      }
                    },
                    icon: Icon(
                      Icons.add,
                      color: mainColor,
                    ),
                  ),
                  focusNode: _prefHobbyFocusNode,
                  onSubmitted: (newValue) {
                    customState(() {
                      String _value = "'$newValue'";
                      if (!_preferredHobby.contains(_value)) {
                        customState(() {
                          _preferredHobby.add(_value);
                          _prefHobbyController.clear();
                        });
                      }
                    });
                  },
                  canSpace: true,
                  canEmpty: true,
                ),
                Flexible(
                  child: Container(
                    width: deviceWidth,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 12.0),
                      child: Wrap(
                        children: List.generate(_preferredHobby.length, (index) {
                          return Padding(
                            padding: EdgeInsets.all(4.0),
                            child: Tag(
                              key: Key(_preferredHobby[index]),
                              text: _preferredHobby[index].replaceAll("'", ""),
                              colorCode: currentColorCode,
                              icon: Icon(
                                Icons.clear,
                                color: mainColor,
                              ),
                              onPressed: () {
                                customState(() {
                                  _preferredHobby.removeAt(index);
                                });
                              },
                            ),
                          );
                        }),
                      ),
                    ),
                  ),
                ),
                _textDisplay(TextData.canChooseMoreThanOne),
                _buttonColumn(onNextPressed: () {
                  _prefHobbyFocusNode.unfocus();
                  apiManager.logAnalysis(Analysis.prefHobby);
                  _nextPage();
                }),
              ],
            );
          },
        ),
      ),
      /// Page 27
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.whatDoYouPrefer),
                _prefExperienceBox(0, customState),
                _prefExperienceBox(1, customState),
                _textDisplay(TextData.prefGreenHeadTip),
                _buttonColumn(onNextPressed: () {
                  apiManager.logAnalysis(Analysis.prefExperience);
                  _nextPage();
                }),
              ],
            );
          },
        ),
      ),
      /// Page 28
      _headerWidget(StatefulBuilder(
        builder: (ctx, customState) {
          return ScrollableFooter(
            children: [
              _titleDisplay(TextData.askPrefLikeCount),
              RangeSlider(
                values: _preferredLikeCountValue,
                min: 0,
                max: prefLikeCountMaximum,
                divisions: 100,
                activeColor: cottonRed,
                inactiveColor: unfocusedColor,
                onChanged: (RangeValues value) {
                  customState(() {
                    _preferredLikeCountValue = value;
                    _preferredLikeCount = [_preferredLikeCountValue.start.round(), _preferredLikeCountValue.end.round()];
                  });
                },
              ),
              _textDisplay(_preferredLikeCountValue.end == prefLikeCountMaximum ? "${_preferredLikeCountValue.start.round()} - ${TextData.unlimited}" : "${_preferredLikeCountValue.start.round()} - ${_preferredLikeCountValue.end.round()}"),
              _buttonColumn(onNextPressed: () {
                apiManager.logAnalysis(Analysis.prefLikeCount);
                _nextPage();
              }),
            ],
          );
        },
      )),
      /// Page 29
      _headerWidget(
        ScrollableFooter(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            ReviewTitle(title: TextData.gender,),
            ReviewCard(
              children: [
                _reviewInformation(_getPrefGenderString(), _prefBeginPage),
              ],
            ),
            ReviewTitle(title: TextData.distance,),
            ReviewCard(
              children: [
                _reviewInformation("$_preferredDistance$distanceSuffix", _prefDistancePage),
              ],
            ),
            ReviewTitle(title: TextData.ageRange,),
            ReviewCard(
              children: [
                _reviewInformation("${_preferredAge.first} - ${_preferredAge.last}", _prefAgePage),
              ]
            ),
            ReviewTitle(title: TextData.ethnicity,),
            ReviewCard(
              children: [
                _reviewInformation(_getListStringAsString(_preferredEthnicity), _prefEthnicityPage),
              ],
            ),
            ReviewTitle(title: TextData.religion,),
            ReviewCard(
              children: [
                _reviewInformation(_getListStringAsString(_preferredReligion), _prefReligionPage),
              ],
            ),
            ReviewTitle(title: TextData.zodiacSign,),
            ReviewCard(
              children: [
                _reviewInformation(_getListStringAsString(_preferredZodiacSign), _prefZodiacSignPage),
              ],
            ),
            ReviewTitle(title: TextData.educationLevel,),
            ReviewCard(
              children: [
                _reviewInformation(_getListStringAsString(_preferredEducation), _prefEducationPage),
              ]
            ),
            ReviewTitle(title: TextData.hobby,),
            ReviewCard(
              children: [
                _reviewInformation(_getListStringAsString(_preferredHobby), _prefHobbyPage),
              ],
            ),
            ReviewTitle(title: TextData.relationshipExperience,),
            ReviewCard(
              children: [
                _reviewInformation(_preferredIsGreenHead == null ? TextData.noPreference : _preferredIsGreenHead ? TextData.prefExperienceBefore : TextData.prefNoExperienceBefore, _prefGreenHeadPage),
              ],
            ),
            ReviewTitle(title: TextData.likeCount,),
            ReviewCard(
              children: [
                _reviewInformation(_preferredLikeCountValue.end == prefLikeCountMaximum ? "${_preferredLikeCountValue.start.round()} - ${TextData.unlimited}" : "${_preferredLikeCountValue.start.round()} - ${_preferredLikeCountValue.end.round()}", _prefLikeCountPage),
              ],
            ),
            Container(height: 15,),
            _buttonColumn(nextTitle: _editPref ? TextData.doneEditing : TextData.done, onNextPressed: () async {
              Dialogs.showLoadingDialog(title: _editPref ? TextData.editingPreference : TextData.creatingPreference);
              await apiManager.createUpdatePreference(
                _editPref ? qp.email : _emailController.text,
                _preferredGender,
                _preferredDistance,
                _preferredAge,
                _preferredEthnicity,
                _preferredReligion,
                _preferredZodiacSign,
                _preferredEducation,
                _preferredHobby,
                _preferredIsGreenHead,
                _preferredRelationship,
                _preferredLikeCount,
              );
              if (!_editPref) {
                await apiManager.getMyProfileAndPreference().then((_) async {
                  await apiManager.signInToFirebase();
                });
              }
              getRoute.pop();
              if (_editPref) {
                /// Pop setup page
                getRoute.truePop();
                widget.notifyParent();
                utils.toast(TextData.editedPreference, isSuccess: true);
              } else {
                apiManager.logAnalysis(Analysis.createPref);
                _nextPage();
              }
            }),
          ],
        ),
        needActions: false,
      ),
      /// Page 30
      _headerWidget(
        StatefulBuilder(
          builder: (ctx, customState) {
            return ScrollableFooter(
              children: [
                _titleDisplay(TextData.areYouReadyToStartNow),
                _bottomNextButton(() async {
                  getRoute.navigateToAndPopAll(MainPage());
                }, title: TextData.yes),
              ],
            );
          },
        ),
      ),
    ];

    return WillPopScope(
      onWillPop: () async {
        /// 17 is the index of the first page of create preference page
        if (
          (_editProfile && _pageController.page == _profileBeginPage) ||
          (_editPref && _pageController.page == _prefBeginPage) ||
          _pageController.page <= _otpPage ||
          _pageController.page == _prefBeginPage ||
          _pageController.page >= _pages.length - 1 ||
          _pageController.page == _profileNamePage && _isNewRegister
        ) {
          _onBackFromFirstPage();
        } else {
          _previousPage();
        }
        return false;
      },
      child: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: _pages,
        onPageChanged: (int newPage) {
          if (newPage == _emailPage || newPage == _profileReviewPage || newPage == _pages.length - 2) {
            refreshThisPage();
          }
        },
      ),
    );
  }
}
