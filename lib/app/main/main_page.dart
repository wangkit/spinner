import 'dart:async';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/app/main/instant_message/instant_message_page.dart';
import 'package:spinner/app/main/settings/settings_page.dart';
import 'package:spinner/app/profile/view_profile_page.dart';
import 'package:spinner/app/setup/setup_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/init_dev.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:flutter/material.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/remote_config_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/decoration.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/custom_dialog.dart';
import 'package:spinner/view/dialogs/filter_dialog.dart';
import 'package:spinner/view/dialogs/first_open_app_dialog.dart';
import 'package:spinner/view/dialogs/first_verify_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/widget/animation/faded_indexed_stack.dart';
import 'package:spinner/widget/animation/open_container_wrapper.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/badge/notification_badge.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/button/refresh_badge.dart';
import 'package:spinner/widget/button/toggle_button/custom_toggle_buttons.dart';
import 'package:spinner/widget/dialog/negative_button.dart';
import 'package:spinner/widget/dialog/positive_button.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/media/image/image_holder.dart';
import 'package:spinner/widget/media/image/thumbnail.dart';
import 'package:spinner/widget/no_data/no_data.dart';
import 'package:spinner/widget/profile/profile_list.dart';
import 'package:spinner/widget/profile/user_profile_holder.dart';
import 'package:spinner/widget/tag/tag.dart';
import 'discover/discover_page.dart';
import 'notification/notification_page.dart';

/// Global remaining discover count to update appbar
ValueNotifier<int> remainingDiscoverCount;

class MainPage extends StatefulWidget {

  MainPage({Key key,}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with AutomaticKeepAliveClientMixin<MainPage>, TickerProviderStateMixin {

  List<UserProfile> _userProfiles;
  List<UserPreference> _userPreferences;
  Future<void> _loading;
  bool _noProfiles;
  bool _showLoading;
  GlobalKey<ScaffoldState> _scaffoldKey;
  AnimationController _menuIconController;
  bool _isFiltered;
  List<UserProfile> _originalProfiles;
  PageController _pageController;
  int _currentIndex;
  int _bottomBarItemCount;
  GlobalKey<NotificationPageState> _notificationPageKey;
  GlobalKey<DiscoverPageState> _discoverPageKey;
  GlobalKey<InstantMessagePageState> _imPageKey;
  GlobalKey<SettingsPageState> _settingPageKey;
  GlobalKey<NotificationBadgeState> _notificationBadgeKey;
  static const _homeIndex = 0;
  static const _notificationIndex = 1;
  static const _discoverIndex = 2;
  static const _chatIndex = 3;
  static const _settingIndex = 4;

  @override
  void initState() {
    _pageController = PageController();
    _imPageKey = GlobalKey<InstantMessagePageState>();
    _notificationBadgeKey = GlobalKey<NotificationBadgeState>();
    _scaffoldKey = GlobalKey<ScaffoldState>();
    _notificationPageKey = GlobalKey<NotificationPageState>();
    _discoverPageKey = GlobalKey<DiscoverPageState>();
    _currentIndex = 0;
    _bottomBarItemCount = 5;
    _menuIconController = AnimationController(
      vsync: this,
      duration: kThemeAnimationDuration,
    );
    _userPreferences = [];
    _userProfiles = [];
    _originalProfiles = [];
    _isFiltered = false;
    _noProfiles = false;
    _showLoading = false;
    _loading = handleRefresh();
    remainingDiscoverCount = ValueNotifier<int>(myProfile.remainingDiscoverCount);
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!hasShownFirstInAppDialog) {
        hasShownFirstInAppDialog = true;
        utils.saveBoo(PrefKey.hasShownFirstInAppDialogKey, hasShownFirstInAppDialog);
        getFirstOpenAppDialog();
      }
    });
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    _pageController.dispose();
    _menuIconController.dispose();
    super.dispose();
  }

  void _onFilter(
      double _filterDistance,
      RangeValues _filterAge,
      bool _hasLoveExperience,
      List<bool> _genderSelectList,
      bool _isDistance,
      bool _isGender,
      bool _isAge,
      bool _isLoveExperiences
      ) {
    if (_originalProfiles.isNotEmpty) _userProfiles = List.from(_originalProfiles);
    _originalProfiles = List.from(_userProfiles);
    UserProfile _tempProfile;
    List<int> _removeIndexList = [];
    _showLoading = true;
    _isFiltered = true;
    refreshThisPage();
    int _loopTime = _userProfiles.length;
    for (int i = 0; i < _loopTime; i++) {
      _tempProfile = _userProfiles[i];
      if (
          /// Filter distance
          (utils.calculateDistance(myProfile.latitude, myProfile.longitude, _tempProfile.latitude, _tempProfile.longitude) > _filterDistance && _isDistance) ||
          /// Filter age
          (_tempProfile.age < _filterAge.start || _tempProfile.age > _filterAge.end && _isAge) ||
          /// Filter green head
          (_tempProfile.noLoveExperience == _hasLoveExperience && _isLoveExperiences) ||
          /// Filter gender
          (!_genderSelectList[_tempProfile.gender] && _isGender)
      ) {
        _removeIndexList.add(i);
      }
    }
    /// Reverse the remove list to avoid removing index closer to the beginning first
    _removeIndexList.reversed.forEach((i) {
      _userProfiles.removeAt(i);
    });
    _showLoading = false;
    refreshThisPage();
  }

  Future<void> handleRefresh({bool isRefresh = false}) async {
    final Completer<void> completer = Completer<void>();
    await apiManager.getPotentialMatches(isRefresh: isRefresh).then((jsonResponse) {
      _userProfiles.clear();
      _userPreferences.clear();
      int jsonStatus = jsonResponse[DataKey.status];
      switch (jsonStatus) {
        case StatusCode.ok:
          List<dynamic> listOfProfiles = jsonResponse[DataKey.profiles];
          List<dynamic> listOfPreferences = jsonResponse[DataKey.preferences];
          int remainingCount = jsonResponse[DataKey.remainingCount];
          _userPreferences.addAll(UserPreference.generateUserPreference(listOfPreferences));
          _userProfiles.addAll(UserProfile.generateUserProfile(listOfProfiles));
          _noProfiles = _userProfiles.isEmpty;
          if (isRefresh) {
            myProfile.remainingRefreshCount = remainingCount;
            if (remainingCount >= 0) {
              utils.toast(TextData.remainingCount + remainingCount.toString(), isSuccess: true);
            } else {
              utils.toast(TextData.tryAgainAfterTwentyFourHours, isWarning: true);
            }
          }
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("error: $onError");
    });
    _showLoading = false;
    refreshThisPage();
    completer.complete();
    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
      onWillPop: () async {
        getBinaryDialog(TextData.exit, TextData.areYouSure, () {
          utils.closeApp();
        });
        return false;
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: MyAppBar(
          automaticallyImplyLeading: false,
          /// This leading is needed to align the title widget to center
          leading: _currentIndex == _settingIndex ? null : OpenContainerWrapper(
            openedWidget: ViewProfilePage(
              userPreference: myPreference,
              userProfile: myProfile,
              isMyProfile: true,
            ),
            closedElevation: 0,
            closedColor: appBgColor,
            closedShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            closedWidget: Padding(
              padding: EdgeInsets.all(8.0),
              child: Thumbnail(
                radius: 24,
                disableOnTap: true,
                text: utils.getFullName(myProfile),
                color: utils.getColorByHex(myProfile.color),
                showBorder: true,
                imageUrl: utils.getProfileThumbnail(myProfile.images),
              ),
            ),
          ),
          titleWidget: _currentIndex == _discoverIndex && !myProfile.isVip ? ValueListenableBuilder(
            valueListenable: remainingDiscoverCount,
            builder: (ctx, int value, Widget child) {
              return Text(
                "${utils.determineNeedS(value, TextData.like.toLowerCase())} remaining",
                style: commonTextStyle
              );
            },
          ) : _currentIndex == _homeIndex && timeUtils.showCountDown() && showCountDown ? CountdownTimer(
            endTime: timeUtils.getCountDown().millisecondsSinceEpoch,
            textStyle: commonTextStyle,
            daysSymbol: Padding(
              padding: EdgeInsets.symmetric(horizontal: 3.0),
              child: Text(
                TextData.day,
                style: commonTextStyle,
              ),
            ),
            emptyWidget: GestureDetector(
              onTap: () async {
                _showLoading = true;
                refreshThisPage();
                Dialogs.showLoadingDialog(title: TextData.fetchingData);
                await Future.wait([
                  handleRefresh(),
                  apiManager.getMyProfileAndPreference(),
                ]);
                refreshThisPage();
                getRoute.pop();
              },
              child: FlipInX(
                child: Text(
                  TextData.newData,
                  style: commonTextStyle,
                ),
              ),
            ),
          ) : Image.asset(
            "assets/icon/spinner_without_bg.png",
            width: 50,
            height: 50,
            color: myProfile != null ? utils.getColorByHex(myProfile.color) : appIconColor,
          ),
          actions: [
            _isFiltered && _currentIndex == _homeIndex ? IconButton(
              onPressed: () {
                /// Filter must be set to false by this btn
                _isFiltered = false;
                _noProfiles = false;
                _userProfiles = List.from(_originalProfiles);
                _originalProfiles.clear();
                refreshThisPage();
              },
              icon: Icon(
                MdiIcons.filterRemove,
                color: mainColor,
              ),
            ) : Container(),
            _currentIndex == _homeIndex ? IconButton(
              onPressed: () {
                if (_userProfiles.every((profile) => profile.isPassedOrLiked)) {
                  utils.toast(TextData.pleaseWaitForNextBatch, isWarning: true);
                } else {
                  getFilterDialog(_onFilter);
                }
              },
              icon: Icon(
                MdiIcons.filter,
                color: mainColor,
              ),
            ) : Container(),
            !_isFiltered && [_homeIndex, _notificationIndex, _discoverIndex].contains(_currentIndex) ? RefreshBadge(
              needVip: _currentIndex == _homeIndex || _currentIndex == _discoverIndex,
              currentIndex: _currentIndex,
              onRefreshClicked: () {
                if (_currentIndex == _notificationIndex) {
                  _notificationPageKey.currentState?.handleRefresh(topRefreshBtnClicked: true);
                } else {

                  String _title = _currentIndex == _discoverIndex ? TextData.refreshDiscoverPage : TextData.refreshHomePage;

                  if (myProfile.remainingRefreshCount <= 0) {
                    utils.toast(TextData.tryAgainAfterTwentyFourHours, isWarning: true);
                  } else {
                    getBinaryDialog(_title, TextData.useRefreshCount, () {
                      switch (_currentIndex) {
                        case _discoverIndex:
                          _discoverPageKey.currentState?.handleRefresh(isRefresh: true);
                          break;
                        default:
                          _showLoading = true;
                          refreshThisPage();
                          handleRefresh(isRefresh: true);
                      }
                    });
                  }
                }
              },
            ) : Container(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          elevation: 0,
          currentIndex: _currentIndex,
          onTap: (index) {
            if (index != _currentIndex) {
              _currentIndex = index;
              if (index == _notificationIndex && myProfile.notificationCount > 0) {
                myProfile.notificationCount = 0;
                _notificationBadgeKey.currentState?.refreshThisPage();
                _notificationPageKey.currentState?.setAllRead();
              }
              refreshThisPage();
            }
          },
          selectedItemColor: mainColor,
          unselectedItemColor: mainColor,
          selectedFontSize: 12.0,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          items: List.generate(_bottomBarItemCount, (index) {
            return BottomNavigationBarItem(
              label: index == _homeIndex ? TextData.home
                  : index == _notificationIndex ? TextData.notifications
                  : index == _discoverIndex ? TextData.discover
                  : index == _chatIndex ? TextData.chat
                  : TextData.settings,
              icon: index == _notificationIndex ? NotificationBadge(
                key: _notificationBadgeKey,
                currentIndex: _currentIndex,
              ) : /// Chat Icon
              index == _chatIndex ? StreamBuilder<QuerySnapshot>(
                stream: firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: qp.id).where(ChatTable.hasUnread, isEqualTo: true).snapshots(),
                builder: (ctx, snapshot) {
                  bool hasUnread = false;
                  if (snapshot.hasData) {
                    hasUnread = snapshot.data.size > 0;
                  }
                  return Swing(
                    infinite: hasUnread,
                    child: Icon(
                      _currentIndex == index ? MdiIcons.chat : MdiIcons.chatOutline,
                      color: !hasUnread ? _currentIndex == index ? utils.getColorByHex(myProfile.color) : mainColor : warning,
                    ),
                  );
                },
              ) : Swing(
                child: Icon(
                  _currentIndex == index ?
                  index == _homeIndex ? Icons.home_filled
                      : index == _discoverIndex ? Icons.search
                      : index == _chatIndex ? Icons.chat_bubble
                      : Icons.settings :
                  index == _homeIndex ? Icons.home_outlined
                      : index == _discoverIndex ? Icons.search_outlined
                      : index == _chatIndex ? Icons.chat_bubble_outline_outlined
                      : Icons.settings_outlined,
                  color: _currentIndex == index ? utils.getColorByHex(myProfile.color) : mainColor,
                ),
              ),
            );
          })
        ),
/// Replaced by settings in bottom bar
//        drawerEdgeDragWidth: drawerEdge,
//        drawer: MainDrawer(
//          notifyParent: refreshThisPage,
//          scaffoldState: _scaffoldKey,
//          animateIcon: (bool isOpen) {
//            if (isOpen) {
//              _menuIconController.forward();
//            } else {
//              _menuIconController.reverse();
//            }
//          },
//        ),
        body: FadeIndexedStack(
          duration: Duration(milliseconds: 300),
          index: _currentIndex,
          children: [
            FutureBuilder(
              future: _loading,
              builder: (ctx, snapshot) {
                return snapshot.connectionState != ConnectionState.done || _showLoading ? LoadingWidget() :
                _noProfiles ? Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: NoData(
                        text: TextData.noMatchingProfiles,
                        asset: "assets/icon/fly_bird.png",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: LongElevatedButton(
                        title: TextData.editPreference,
                        color: utils.getColorByHex(myProfile.color),
                        onPressed: () {
                          getRoute.navigateTo(SetupPage(
                            editPref: true,
                            notifyParent: () async {
                              /// After edit preference
                              /// get latest profile and preference
                              Dialogs.showLoadingDialog(title: TextData.fetchingData);
                              await apiManager.getMyProfileAndPreference();
                              getRoute.pop();
                            },
                          ));
                        },
                      ),
                    ),
                  ],
                ) : ProfileList(
                  userPreferences: _userPreferences,
                  userProfiles: _userProfiles,
                  getIsProfileEmpty: (bool isEmpty) {
                    _noProfiles = isEmpty;
                    if (_noProfiles) {
                      refreshThisPage();
                    }
                  },
                );
              },
            ),
            NotificationPage(
              key: _notificationPageKey,
            ),
            DiscoverPage(
              key: _discoverPageKey,
            ),
            InstantMessagePage(
              key: _imPageKey,
            ),
            SettingsPage(
              key: _settingPageKey,
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
