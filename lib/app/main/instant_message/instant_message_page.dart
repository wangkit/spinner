import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/chat/chat_item_display.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/no_data/no_data.dart';

class InstantMessagePage extends StatefulWidget {
  InstantMessagePage({Key key}) : super(key: key);

  @override
  InstantMessagePageState createState() => InstantMessagePageState();
}

class InstantMessagePageState extends State<InstantMessagePage> with AutomaticKeepAliveClientMixin<InstantMessagePage> {

  @override
  bool get wantKeepAlive => true;

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: qp.id).orderBy(ChatTable.updatedAt, descending: true).snapshots(),
        builder: (ctx, snapshot) {
          if (!snapshot.hasData) {
            return LoadingWidget();
          }
          final chats = snapshot.data.docs;
          List<ChatItem> _chatItems = [];
          for (var chat in chats) {

            var data = chat.data();
            Timestamp updatedAt = data[ChatTable.updatedAt];
            Timestamp createdAt = data[ChatTable.createdAt];
            String updatedAtString = timeUtils.getDateTimeFromTimestamp(updatedAt);
            String createdAtString = timeUtils.getDateTimeFromTimestamp(createdAt);

            final chatItem = ChatItem(
              senderId: data[ChatTable.senderId],
              updatedAt: updatedAtString,
              targetEmail: data[ChatTable.targetEmail],
              lastMessage: data[ChatTable.lastMessage],
              targetColor: data[ChatTable.targetColor],
              targetId: data[ChatTable.targetId],
              targetFirstName: data[ChatTable.targetFirstName],
              targetLastName: data[ChatTable.targetLastName],
              isLastMessageMyself: data[ChatTable.isLastMessageMyself],
              targetImages: data[ChatTable.targetImages].cast<String>(),
              createdAt: createdAtString,
              hasUnread: data[ChatTable.hasUnread],
            );
            _chatItems.add(chatItem);
          }
          return _chatItems.isEmpty ? NoData(
            text: TextData.noChatItems,
            asset: "assets/icon/horn.png",
          ) : ListView.separated(
            separatorBuilder: (ctx, index) {
              return Divider(
                color: unfocusedColor,
                height: 1,
              );
            },
            itemCount: _chatItems.length,
            itemBuilder: (context, index) {
              return ChatItemDisplay(
                key: Key(_chatItems[index].hasUnread.toString() + _chatItems[index].lastMessage + _chatItems[index].updatedAt),
                chatItem: _chatItems[index],
              );
            },
          );
        },
      ),
    );
  }
}

