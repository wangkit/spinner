import 'dart:async';
import 'package:flutter/rendering.dart';
import 'package:spinner/constants/const.dart';
import 'package:flutter/material.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/no_data/no_data.dart';
import 'package:spinner/widget/profile/profile_list.dart';
import 'package:spinner/widget/profile/user_profile_holder.dart';

class DiscoverPage extends StatefulWidget {

  DiscoverPage({
    Key key,
  }) : super(key: key);

  @override
  DiscoverPageState createState() => DiscoverPageState();
}

class DiscoverPageState extends State<DiscoverPage> with AutomaticKeepAliveClientMixin<DiscoverPage> {

  List<UserProfile> _userProfiles;
  List<UserPreference> _userPreferences;
  Future<void> _loading;
  bool _noProfiles;
  bool _showLoading;

  @override
  void initState() {
    _userPreferences = [];
    _userProfiles = [];
    _noProfiles = false;
    _showLoading = false;
    _loading = handleRefresh();
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> handleRefresh({bool isRefresh = false}) async {
    if (isRefresh) _showLoading = true;
    refreshThisPage();
    final Completer<void> completer = Completer<void>();
    await apiManager.getDiscover(isRefresh: isRefresh).then((jsonResponse) {
      _userProfiles.clear();
      _userPreferences.clear();
      int jsonStatus = jsonResponse[DataKey.status];
      switch (jsonStatus) {
        case StatusCode.ok:
          List<dynamic> listOfProfiles = jsonResponse[DataKey.profiles];
          List<dynamic> listOfPreferences = jsonResponse[DataKey.preferences];
          _userPreferences.addAll(UserPreference.generateUserPreference(listOfPreferences));
          _userProfiles.addAll(UserProfile.generateUserProfile(listOfProfiles));
          int remainingCount = jsonResponse[DataKey.remainingCount];
          _noProfiles = _userProfiles.isEmpty;
          if (isRefresh) {
            myProfile.remainingRefreshCount = remainingCount;
            if (remainingCount >= 0) {
              utils.toast(TextData.remainingCount + remainingCount.toString(), isSuccess: true);
            } else {
              utils.toast(TextData.tryAgainAfterTwentyFourHours, isWarning: true);
            }
          }
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("error: $onError");
    });
    _showLoading = false;
    refreshThisPage();
    completer.complete();
    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
      future: _loading,
      builder: (ctx, snapshot) {
        return snapshot.connectionState != ConnectionState.done || _showLoading ? LoadingWidget() :
        _noProfiles ? NoData(
          text: TextData.noDiscoveries,
          asset: "assets/icon/bow.png",
        ) : ProfileList(
          isDiscoverPage: true,
          userPreferences: _userPreferences,
          userProfiles: _userProfiles,
          getIsProfileEmpty: (bool isEmpty) {
            _noProfiles = isEmpty;
            if (_noProfiles) {
              refreshThisPage();
            }
          },
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
