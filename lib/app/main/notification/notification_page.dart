import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:spinner/app/profile/view_profile_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/activity.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:spinner/utils/bottom_sheet/sale_bottom_sheet.dart';
import 'package:spinner/view/dialogs/block_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/widget/button/bottom_sheet_button.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/no_data/no_data.dart';
import 'package:spinner/widget/no_data/prompt_subscribe.dart';

class NotificationPage extends StatefulWidget {

  NotificationPage({Key key}) : super(key: key);

  @override
  NotificationPageState createState() => NotificationPageState();
}

class NotificationPageState extends State<NotificationPage> with AutomaticKeepAliveClientMixin<NotificationPage> {

  RefreshController _refreshController;
  List<ActivityNotification> _notifications;
  bool onFirstLoad;
  Future<void> _loading;
  bool noNotifications = false;
  bool showLoading = false;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    _refreshController = RefreshController(initialRefresh: false);
    _notifications = [];
    onFirstLoad = true;
    _loading = handleRefresh();
    super.initState();
  }

  void setAllRead() {
    /// Set all notification items in list to true
    _notifications.forEach((notification) {
      if (!notification.isRead) {
        notification.isRead = true;
      }
    });
    /// Call api to set all notification itesm to true
    apiManager.readAllNotifications();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> handleRefresh({bool topRefreshBtnClicked = false, bool onPullDown = false, bool onScrollDown = false}) async {
    if (onFirstLoad || onPullDown || onScrollDown || topRefreshBtnClicked) {
      noNotifications = false;
      final Completer<void> completer = Completer<void>();
      if (topRefreshBtnClicked) {
        setState(() {
          showLoading = true;
          _notifications.clear();
        });
      }
      var startFrom;
      if (onScrollDown) {
        startFrom = _notifications.length;
      } else {
        startFrom = 0;
      }
      await getNotifications(startFrom, onScrollDown).then((_) {
        onFirstLoad = false;
      });
      if (onScrollDown && _notifications.length == startFrom) {
      } else {
        refreshThisPage();
        showLoading = false;
      }
      completer.complete();
      return completer.future.then<void>((_) {
        if (onPullDown) {
          _refreshController.refreshCompleted();
        }
        if (onScrollDown) {
          _refreshController.loadComplete();
        }
      });
    }
  }

  Future<void> getNotifications(int startFrom, bool addData) async {
    await apiManager.fetchActivity(startFrom).then((jsonResponse) async {
      int jsonStatus = jsonResponse[DataKey.status];
      switch (jsonStatus) {
        case StatusCode.ok:
          List<dynamic> listOfActivities = jsonResponse[DataKey.activity];
          var _newNotifications = ActivityNotification.generateNotification(listOfActivities);
          if (!addData) {
            _notifications.clear();
          }
          _notifications..addAll(List<ActivityNotification>.from(ActivityNotification.filterNotification(_notifications, _newNotifications)));
          noNotifications = _notifications.isEmpty;
          break;
        default:
          utils.toast(TextData.connectionFailed, isWarning: true);
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
      builder: (context, projectSnap) {
        switch (projectSnap.connectionState) {
          default:
            return projectSnap.connectionState != ConnectionState.done || showLoading ? LoadingWidget() :
            noNotifications ? NoData(
              text: TextData.noNotifications,
              asset: "assets/icon/elephant.png",
            ) : SmartRefresher(
              controller: _refreshController,
              enablePullUp: true,
              onRefresh: () => handleRefresh(onPullDown: true),
              onLoading: () => handleRefresh(onScrollDown: true),
              child: ListView.builder(
                addAutomaticKeepAlives: true,
                scrollDirection: Axis.vertical,
                itemCount: _notifications.length,
                itemBuilder: (context, index) {

                  ActivityNotification _thisNotification = _notifications[index];

                  return Container(
                    width: deviceWidth,
                    padding: EdgeInsets.symmetric(vertical: 2.0),
                    child: Card(
                      elevation: commonElevation,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(commonRadius)
                      ),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(commonRadius),
                        onTap: () async {
                          if (myProfile.isVip) {
                            Dialogs.showLoadingDialog(title: TextData.fetchingData);
                            await apiManager.getPpById(_thisNotification.creatorId).then((jsonResponse) {
                              getRoute.pop();
                              UserProfile _thisProfile;
                              UserPreference _thisPreference;
                              var jsonStatus = jsonResponse[DataKey.status];
                              if (jsonStatus == StatusCode.ok) {
                                List<dynamic> profile = jsonResponse[DataKey.profile];
                                List<dynamic> preference = jsonResponse[DataKey.myPreference];
                                _thisProfile = UserProfile.generateUserProfile(profile).first;
                                _thisPreference = preference != null ? UserPreference.generateUserPreference(preference).first : null;
                                getRoute.navigateTo(ViewProfilePage(userProfile: _thisProfile, userPreference: _thisPreference));
                              }
                            });
                          } else {
                            getSaleBottomSheet();
                          }
                        },
                        onLongPress: () {
                          getGenericBottomSheet([
                            BottomSheetButton(
                              label: TextData.blockUser,
                              iconData: Icons.block,
                              onTap: () => getBlockDialog(_thisNotification.creatorId, utils.getFullNameByNames(_thisNotification.lastName, _thisNotification.firstName), utils.getChatId([_thisNotification.creatorId, myProfile.id]), null),
                            ),
                          ]);
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(height: 10,),
                            Flexible(
                              child: ListTile(
                                /// Leading widget will consume entire width without a sized widget
                                leading: SizedBox(
                                  width: 60,
                                  height: 60,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(commonImageRadius),
                                    child: CachedNetworkImage(
                                      imageUrl: _thisNotification.creatorImage,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                trailing: Text(
                                  timeUtils.getTimeDiffString(_thisNotification.createdAt),
                                  style: commonTextStyle,
                                ),
                                title: RichText(
                                  text: TextSpan(
                                    text: _thisNotification.firstName + " " + _thisNotification.lastName,
                                    style: commonTextStyle.merge(
                                        TextStyle(
                                          color: utils.getColorByHex(_thisNotification.creatorColor),
                                        )
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: _thisNotification.activity == Activity.greeting ? Activity.activityMap[_thisNotification.activity] + _thisNotification.message
                                            : Activity.activityMap[_thisNotification.activity],
                                        style: commonTextStyle,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 10,),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            );
        }
      },
      future: _loading,
    );
  }
}


