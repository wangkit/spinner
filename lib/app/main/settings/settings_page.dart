import 'dart:async';
import 'dart:math';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soy_common/pages/soy_about_page.dart';
import 'package:spinner/app/preference/view_preference_page.dart';
import 'package:spinner/app/profile/view_profile_page.dart';
import 'package:spinner/app/settings/user_experience_settings.dart';
import 'package:spinner/app/start/start_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/media/image/thumbnail.dart';

class SettingsPage extends StatefulWidget {

  SettingsPage({Key key,}) : super(key: key);

  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {

  double _iconSize;

  Map<String, Widget> _detailMap = {
    TextData.myProfile: ViewProfilePage(
      userPreference: myPreference,
      userProfile: myProfile,
      isMyProfile: true,
    ),
    TextData.myPreference: ViewPreferencePage(
      userPreference: myPreference,
      userProfile: myProfile,
      isMyPreference: true,
    ),
    TextData.aboutUs: SoyAboutPage(
      mainColor: mainColor,
      shareMsg: shareMsg,
      elevation: 0,
      title: TextData.about,
      titleBarColor: !useBlackTheme ? Colors.white70 : Colors.grey[900],
      backgroundColor: appBgColor,
      bigTextStyle: bigTextStyle,
      smallTextStyle: smallTextStyle,
      commonTextStyle: commonTextStyle,
      appName: appName,
      version: versionName + "(" + versionCode.toString() + ")",
    ),
    TextData.UserSettings: UserExperienceSettings(),
  };

  Map<String, String> _subtitleMap = {
    TextData.myProfile: "View / Edit Your Profile",
    TextData.myPreference: "View / Edit Your Preference",
    TextData.aboutUs: "About Spinner of Yarns",
    TextData.UserSettings: "Fine Tune Your Experience",
  };

  Map<String, Icon> _iconMap;

  @override
  void initState() {
    _iconSize = 52;
    _iconMap = {
      TextData.myProfile: Icon(
        Icons.account_circle,
        size: _iconSize,
        color: utils.getColorByHex(myProfile.color),
      ),
      TextData.myPreference: Icon(
        Icons.room_preferences,
        size: _iconSize,
        color: Colors.red
      ),
      TextData.aboutUs: Icon(
        MdiIcons.information,
        size: _iconSize,
        color: Colors.blue
      ),
      TextData.UserSettings: Icon(
        Icons.settings,
        size: _iconSize,
        color: Colors.yellow,
      ),
    };
    super.initState();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 20,
            ),
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Thumbnail(
                    radius: 100,
                    width: 100,
                    height: 100,
                    disableOnTap: true,
                    text: utils.getFullName(myProfile),
                    color: utils.getColorByHex(myProfile.color),
                    showBorder: false,
                    imageUrl: utils.getProfileThumbnail(myProfile.images),
                  ),
                  Container(
                    height: 10,
                  ),
                  Text(
                    "${utils.getProfileValue(myProfile.quickDisplayOne, myProfile, 0)}, ${utils.getProfileValue(myProfile.quickDisplayTwo, myProfile, 1)}",
                    key: UniqueKey(),
                    textAlign: TextAlign.center,
                    style: bigTextStyle,
                  ),
                  Container(
                    height: 10,
                  ),
                  Text(
                    myProfile.personality,
                    textAlign: TextAlign.center,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    key: UniqueKey(),
                    style: commonTextStyle,
                  ),
                  Container(
                    height: 10,
                  ),
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: GridView.builder(
                        itemCount: 4,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 1,
                        ),
                        itemBuilder: (ctx, index) {
                          return Card(
                            elevation: 4.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(commonRadius / 2),
                            ),
                            child: InkWell(
                              onTap: () {
                                getRoute.navigateTo(_detailMap.values.toList()[index]);
                              },
                              borderRadius: BorderRadius.circular(commonRadius / 2),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(12.0),
                                    child: _iconMap[_iconMap.keys.toList()[index]],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 12.0),
                                    child: Text(
                                      _subtitleMap.keys.toList()[index],
                                      style: commonTextStyle,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 18.0),
                                    child: Text(
                                      _subtitleMap.values.toList()[index],
                                      style: smallTextStyle.merge(
                                        TextStyle(
                                          color: Colors.grey,
                                        )
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
