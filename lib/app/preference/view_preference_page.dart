import 'dart:async';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:spinner/app/setup/setup_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/bottom_sheet/generic_bottom_sheet.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/view/dialogs/loading_dialog.dart';
import 'package:spinner/widget/animation/open_container_wrapper.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/button/bottom_sheet_button.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/button/refresh_badge.dart';
import 'package:spinner/widget/divider/separation_dot.dart';
import 'package:spinner/widget/loading/loading_widget.dart';
import 'package:spinner/widget/media/image/image_holder.dart';
import 'package:spinner/widget/media/image/image_viewer.dart';
import 'package:spinner/widget/preference/user_preference_holder.dart';
import 'package:spinner/widget/profile/user_profile_holder.dart';

class ViewPreferencePage extends StatefulWidget {
  final UserPreference userPreference;
  final UserProfile userProfile;
  final bool isMyPreference;

  ViewPreferencePage({
    Key key,
    @required this.userPreference,
    @required this.userProfile,
    this.isMyPreference = false,
  }) : super(key: key);

  @override
  ViewPreferencePageState createState() => ViewPreferencePageState();
}

class ViewPreferencePageState extends State<ViewPreferencePage> with AutomaticKeepAliveClientMixin<ViewPreferencePage> {

  UserPreference _userPreference;
  UserProfile _userProfile;
  bool _showLoading;
  Future<void> _loading;
  bool _isMyPreference;
  bool _onFirstLoad;
  ScrollController _scrollController;
  bool _isExtended;
  Widget _fab;
  StrictPreference _userStrictPreference;

  @override
  void initState() {
    _isExtended = true;
    setFab();
    _scrollController = ScrollController();
    _userProfile = widget.userProfile;
    _userPreference = widget.userPreference;
    _onFirstLoad = true;
    _isMyPreference = widget.isMyPreference;
    _showLoading = false;
    _loading =  _fetchPreference();
    if (_isMyPreference) {
      _userStrictPreference = StrictPreference(
        gender: myStrictPreference.gender,
        distance: myStrictPreference.distance,
        religion: myStrictPreference.religion,
        ethnicity: myStrictPreference.ethnicity,
        education: myStrictPreference.education,
        hobby: myStrictPreference.hobby,
        isGreenHead: myStrictPreference.isGreenHead,
        preferredRelationship: myStrictPreference.preferredRelationship,
        age: myStrictPreference.age,
        likeCounts: myStrictPreference.likeCounts,
        zodiacSign: myStrictPreference.zodiacSign,
      );
    }
    super.initState();
  }

  void setFab() {
    if (_isExtended) {
      _fab = OpenContainerWrapper(
        openedWidget: SetupPage(
          editPref: true,
          notifyParent: _fetchPreference,
        ),
        closedElevation: 6.0,
        closedShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(26),
          ),
        ),
        closedWidget: FloatingActionButton.extended(
          key: UniqueKey(),
          heroTag: UniqueKey(),
          onPressed: null,
          icon: Icon(
            Icons.edit,
            color: mainColor,
          ),
          label: Text(
            TextData.edit,
            style: commonTextStyle,
          ),
        ),
      );
    } else {
      _fab = FloatingActionButton.extended(
        onPressed: () async {
          Dialogs.showLoadingDialog(title: TextData.editingStrictPreference);
          myStrictPreference = StrictPreference(
            gender: _userStrictPreference.gender,
            age: _userStrictPreference.age,
            distance: _userStrictPreference.distance,
            preferredRelationship: _userStrictPreference.preferredRelationship,
            hobby: _userStrictPreference.hobby,
            isGreenHead: _userStrictPreference.isGreenHead,
            education: _userStrictPreference.education,
            ethnicity: _userStrictPreference.ethnicity,
            likeCounts: _userStrictPreference.likeCounts,
            religion: _userStrictPreference.religion,
            zodiacSign: _userStrictPreference.zodiacSign,
          );
          await apiManager.editStrictPreference().then((status) {
            getRoute.pop();
            switch (status) {
              case StatusCode.ok:
                _isExtended = true;
                setFab();
                refreshThisPage();
                utils.toast(TextData.editedPreference, isSuccess: true);
                break;
              default:
                utils.toast(TextData.connectionFailed, isWarning: true);
            }
          });
        },
        icon: Icon(
          Icons.done,
          color: mainColor,
        ),
        label: Text(
          TextData.done,
          style: commonTextStyle,
        ),
      );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<void> _fetchPreference() async {
    _showLoading = true;
    refreshThisPage();
    if (_isMyPreference && !_onFirstLoad) {
      await apiManager.getMyProfileAndPreference();
      _userPreference = myPreference;
    }
    _onFirstLoad = false;
    _showLoading = false;
    _isExtended = true;
    refreshThisPage();
  }

  void refreshThisPage() {
    if (this.mounted) {
      setState(() {});
    }
  }

  void setExtended(bool shouldExtend) {
    _isExtended = shouldExtend;
    setFab();
    refreshThisPage();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      floatingActionButton: _isMyPreference ? AnimatedSwitcher(
        transitionBuilder: (Widget child, Animation<double> animation) {
          return ScaleTransition(
            scale: animation,
            child: child,
            alignment: Alignment.bottomRight,
          );
        },
        duration: kThemeAnimationDuration,
        child: _fab,
      ) : null,
      appBar: MyAppBar(
        title: _isMyPreference ? TextData.myPreference : "${utils.getFullName(widget.userProfile)}'s ${TextData.preferences}",
        actions: _isMyPreference ? [
          IconButton(
            onPressed: () {
              getBinaryDialog(
                TextData.strictPreference,
                TextData.strictPreferenceExample,
                () {},
                needNegativeButton: false,
                positiveText: TextData.ok,
              );
            },
            icon: Icon(
              Icons.info,
              color: mainColor,
            ),
          ),
          RefreshBadge(
            onRefreshClicked: _fetchPreference,
            needVip: false,
          ),
        ] : [],
      ),
      body: FutureBuilder(
        future: _loading,
        builder: (ctx, snapshot) {
          return snapshot.connectionState != ConnectionState.done || _showLoading ? LoadingWidget() :
          UserPreferenceHolder(
            key: UserPreference().getPreferenceWidgetKey(_userPreference),
            userProfile: _userProfile,
            userPreference: _userPreference,
            isMyPreference: _isMyPreference,
            scrollController: _scrollController,
            setExtended: setExtended,
            userStrictPreference: _userStrictPreference,
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
