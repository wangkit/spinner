import 'package:animate_do/animate_do.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:spinner/app/setup/setup_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/analysis.dart';
import 'package:spinner/resources/values/remote_config_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/decoration.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/widget/appbar/appbar.dart';
import 'package:spinner/widget/button/custom_bouncing_widget.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';

class StartPage extends StatefulWidget {

  StartPage({
    Key key,
  }) : super(key: key);

  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> with TickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async {
        getBinaryDialog(
          TextData.exit,
          TextData.areYouSureYouWantToCloseTheApp,
          utils.closeApp,
          positiveColor: errorColor,
          negativeColor: success,
        );
        return false;
      },
      child: Container(
        decoration: BoxDecoration(
          color: remoteConfig.getBool(RemoteConfigKey.startBackgroundUseGradient) ? null : utils.getColorByHex(remoteConfig.getString(RemoteConfigKey.startBackgroundTopColor)),
          gradient: remoteConfig.getBool(RemoteConfigKey.startBackgroundUseGradient) ? LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [utils.getColorByHex(remoteConfig.getString(RemoteConfigKey.startBackgroundTopColor)),  utils.getColorByHex(remoteConfig.getString(RemoteConfigKey.startBackgroundBottomColor))],
          ) : null,
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 14.0, bottom: 24),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100.0),
                            child: Hero(
                              tag: "spinner_without_bg",
                              child: Image.asset(
                                "assets/icon/spinner_without_bg.png",
                                fit: BoxFit.cover,
                                color: defaultWhiteColor,
                                width: 70,
                                height: 70,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      child: BounceInDown(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40.0,),
                          child: Text(
                            remoteConfig.getString(RemoteConfigKey.startPageQuote) ?? TextData.johnQuote,
                            style: bigTextStyle.merge(
                                TextStyle(
                                  fontSize: 60,
                                  color: defaultWhiteColor,
                                )
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 8.0, top: 4.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        FadeInLeft(
                          child: LongElevatedButton(
                            padding: EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0,),
                            title: TextData.newMember,
                            color: utils.getColorByHex(remoteConfig.getString(RemoteConfigKey.startBackgroundNewMemberButtonColor)),
                            textStyle: longButtonTextStyle.merge(
                              TextStyle(
                                color: defaultWhiteColor,
                              )
                            ),
                            onPressed: () {
                              apiManager.logAnalysis(Analysis.newMemberButtonClick);
                              getRoute.navigateToAndPopAll(
                                SetupPage(
                                  isExistingMember: false,
                                  isNewRegister: true,
                                )
                              );
                            },
                          ),
                        ),
                        FadeInRight(
                          child: LongElevatedButton(
                            padding: EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0,),
                            isReverseColor: true,
                            color: transparent,
                            title: TextData.existingMember,
                            textStyle: longButtonTextStyle.merge(
                              TextStyle(
                                color: defaultWhiteColor,
                              )
                            ),
                            onPressed: () async {
                              apiManager.logAnalysis(Analysis.existingMemberButtonClick);
                              getRoute.navigateToAndPopAll(
                                  SetupPage(isExistingMember: true,)
                              );
                            },
                          ),
                        ),
                        Expanded(
                          child: SafeArea(
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 12.0),
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                        text: TextData.byContinuing,
                                        style: smallTextStyle.merge(
                                          TextStyle(
                                            color: defaultWhiteColor,
                                          )
                                        ),
                                      ),
                                      TextSpan(
                                        recognizer: TapGestureRecognizer()..onTap = () {
                                          utils.launchURL(privacyPolicyUrl);
                                        },
                                        text: TextData.privacyPolicy,
                                        style: TextStyle(
                                          color: defaultWhiteColor,
                                          decoration: TextDecoration.underline,
                                          fontSize: smallFontSize
                                        ),
                                      ),
                                      TextSpan(
                                        text: " " + TextData.and + " ",
                                        style: smallTextStyle.merge(
                                          TextStyle(
                                            color: defaultWhiteColor,
                                          )
                                        ),
                                      ),
                                      TextSpan(
                                        recognizer: TapGestureRecognizer()..onTap = () {
                                          utils.launchURL(eulaLink);
                                        },
                                        text: TextData.endUserLicenseAgreement,
                                        style: TextStyle(
                                          color: defaultWhiteColor,
                                          decoration: TextDecoration.underline,
                                          fontSize: smallFontSize,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
