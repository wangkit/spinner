import 'dart:async';
import 'package:animate_do/animate_do.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:soy_common/pages/soy_splash_page.dart';
import 'package:spinner/app/main/main_page.dart';
import 'package:spinner/app/setup/setup_page.dart';
import 'package:spinner/app/start/start_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/network/api_services.dart';
import 'package:spinner/network/iap_manager.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/background/background_task.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/remote_config_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/force_update_dialog.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'dart:io';
import 'package:spinner/widget/emoji/custom_emoji_lists.dart' as emojiList;
import 'package:spinner/constants/const.dart';
import 'package:spinner/network/downloader.dart';
import 'package:spinner/network/socket.dart';
import 'package:spinner/network/uploader.dart';
import 'package:spinner/page_route/get_route.dart';
import 'package:spinner/resources/models/env.dart';
import 'package:spinner/utils/time_utils.dart';
import 'package:spinner/utils/utils.dart';
import 'package:workmanager/workmanager.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  static const platform = const MethodChannel("emoji_picker");
  double iconSize = 60;
  var splashPref;
  String iconColorCode;
  String _likeAnimationName = "Like heart";

  Future<void> initCalls() async {
    if (!qp.email.isNullOrEmpty) {
      debugPrint("Has account");
      /// Can enter directly
      await Future.wait([
        apiManager.initClientOrRetrieveData(),
        apiManager.getMyProfileAndPreference(),
      ]);
    } else {
      debugPrint("No account");
      /// New comer or need login
      await apiManager.initClientOrRetrieveData();
    }
  }

  Future<bool> _isEmojiAvailable(String emoji) async {
    if (Platform.isAndroid) {
      bool isAvailable;
      try {
        isAvailable = await platform.invokeMethod("isAvailable", {"emoji": emoji});
      } on PlatformException catch (_) {
        isAvailable = false;
      }
      return isAvailable;
    } else {
      return true;
    }
  }

  Future<Map<String, String>> getAvailableEmojis(Map<String, String> map) async {
    Map<String, String> newMap = Map<String, String>();

    for (int index = 0; index < map.length; index++) {
      String key = map.keys.toList()[index];
      /// map[key] get the emoji value, and only emoji with length equals to two can be normally displayed, thus the checking
      if (await _isEmojiAvailable(map[key]) && map[key].length == 2) {
        newMap[key] = map[key];
      }
    }
    return newMap;
  }

  Future<void> initSharedPreference() async {
    smileyMap = await getAvailableEmojis(emojiList.smileys);
    animalMap = await getAvailableEmojis(emojiList.animals);
    foodMap = await getAvailableEmojis(emojiList.foods);
    travelMap = await getAvailableEmojis(emojiList.travel);
    activityEmojiMap = await getAvailableEmojis(emojiList.activities);
    objectMap = await getAvailableEmojis(emojiList.objects);
    symbolMap = await getAvailableEmojis(emojiList.symbols);
    flagMap = await getAvailableEmojis(emojiList.flags);
    await utils.loadPref();
  }

  void _initCalls() {
    /// Step one: Init instances
    utils = Utils();
    iapManager = IapManager();
    getRoute = GetRoute();
    timeUtils = TimeUtils();
    downloader = Downloader();
    uploader = Uploader();
    apiManager = ApiManager();
    socketManager = SocketManager();
    analytics = FirebaseAnalytics();
    Workmanager.initialize(
      callbackDispatcher,
      isInDebugMode: env.flavor == BuildFlavor.development,
    );
    /// Step two: Init variables with this environment
    apiBaseUrl = env.baseUrl;
    apiManager.getAllLists();
    bool needForceUpdate;
    WidgetsFlutterBinding.ensureInitialized();
    /// Step three: Init shared preference
    initSharedPreference().then((_) {
      Future.wait([
        initCalls(),
        PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
          /// Using packageInfo.appName here will get a null value for iOS.
          /// Since we will not change the app name in the future, simply hardcoding the app name here solves the problem.
          appName = myAppName;
          versionCode = int.parse(packageInfo.buildNumber);
          versionName = packageInfo.version;
          googleStoreUrl = remoteConfig.getString(RemoteConfigKey.googleStoreUrl);
          appleStoreUrl = remoteConfig.getString(RemoteConfigKey.appleStoreUrl);
          checkFace = remoteConfig.getBool(RemoteConfigKey.checkFace);
          int installedVersionNumber = int.parse(versionName.toString().trim().replaceAll(".", ""));
          int requiredVersionNumber = int.parse(remoteConfig.getString(RemoteConfigKey.minimumVersion).toString().trim().replaceAll(".", ""));
          needForceUpdate = installedVersionNumber < requiredVersionNumber && remoteConfig.getBool(RemoteConfigKey.enforceMinimumVersion);
        }),
      ]).then((_) {
        if (!needForceUpdate) {
          /// Step nine: Navigate to forum app.main page if myProfile is got and this app does not need to update
          SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) async {
            /// Init firebase messaging instance
            firebaseMessaging = FirebaseMessaging();
            /// Register firebase messaging
            firebaseMessaging.requestNotificationPermissions(
                const IosNotificationSettings(sound: true, badge: true, alert: true));
            firebaseMessaging.onIosSettingsRegistered
                .listen((IosNotificationSettings settings) {
            });
            Future.delayed(Duration(seconds: 2), () {
              firebaseMessaging.configure(
                onMessage: (Map<String, dynamic> message) async {
                  /// Handle push notification when app is on foreground
                  myBackgroundMessageHandler(message);
                },
                /// For unknown reason, adding below line will cause an exception
                onBackgroundMessage: onBackgroundMessage,
                onLaunch: (Map<String, dynamic> message) async {
                  /// Handle push notification when app is terminated
                  myBackgroundMessageHandler(message);
                },
                onResume: (Map<String, dynamic> message) async {
                  /// Handle push notification when app is in background
                  myBackgroundMessageHandler(message);
                },
              );
            });
            /// Not necessarily qp.email, use any object inside qp can achieve the same result
            if (qp.email.isNullOrEmpty || qp.password.isNullOrEmpty) {
              /// No account
              getRoute.navigateToAndPopAll(
                  StartPage()
              );
            } else {
              /// Has account
              /// Sign in to firebase with email and password
              await apiManager.signInToFirebase();
              if (myPreference == null) {
                /// If myPreference is null, this means this user hasn't created a preference yet
                /// If so, bring this user to setup page 19 (pref begin page)
                getRoute.navigateToAndPopAll(
                    SetupPage(
                      isExistingMember: true,
                      navigateToPref: true,
                    )
                );
                utils.toast(TextData.finishPrefFirst, isWarning: true);
              } else {
                getRoute.navigateToAndPopAll(
                    MainPage()
                );
              }
            }
          });
        } else if (needForceUpdate) {
          showForceUpdateDialog();
        } else {
          utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return SoySplashPage(
      buildFunctions: () {
        deviceWidth = MediaQuery.of(context).size.width;
        deviceHeight = MediaQuery.of(context).size.height;
      },
      backgroundColorCode: remoteConfig.getString(RemoteConfigKey.splashBackgroundColor),
      initFunctions: () => _initCalls(),
      assetPath: "assets/icon/spinner_without_bg.png",
    );
  }
}

Future<dynamic> onBackgroundMessage(Map<String, dynamic> message) async {
  debugPrint("Background message: $message");
  return Future<void>.value();
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  debugPrint("Background handler message: $message");
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    String title;
    String body;
    title = notification['title'];
    body = notification['body'];
    utils.toast(body, isSuccess: true);
  }

  // Or do other work.
}