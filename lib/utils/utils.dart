import 'dart:collection';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:animate_do/animate_do.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/color/hex.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/gender_status.dart';
import 'package:spinner/resources/status/preferred_relationship_status.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/widget/restart/restart_widget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';
import 'package:encrypt/encrypt.dart' as en;

class Utils {
  /// Quit focus on text field and close keyboard
  void quitFocus({bool createNewFocusToRemoveOld = false}) {
    FocusScopeNode currentFocus = FocusScope.of(getRoute.getContext());
    if (!currentFocus.hasPrimaryFocus) {
      if (createNewFocusToRemoveOld) {
        currentFocus.requestFocus(FocusNode());
      } else {
        currentFocus.unfocus();
      }
    }
    closeKeyboard();
  }

  void logoutClearData() {
    prefs.remove(PrefKey.id);
    prefs.remove(PrefKey.email);
    prefs.remove(PrefKey.password);
    prefs.remove(PrefKey.accountColor);
    qp.password = null;
    qp.id = null;
    hasCalledFCMRegistration = false;
    hasCalledCatKey = false;
    qp.email = null;
    myProfile = null;
    firebaseUser = null;
    myPreference = null;
  }

  String getProfileThumbnail(List<String> imageList) {
    if (imageList.isEmpty) return placeholderLink;
    return imageList.first;
  }

  double getProfileMediaHeight() {
    return deviceHeight - kToolbarHeight - kToolbarHeight - (deviceHeight * 0.15);
  }

  fieldFocusChange(FocusNode currentFocus,FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(getRoute.getContext()).requestFocus(nextFocus);
  }

  String getHexCodeByColor(Color color) {
    return color.value.toRadixString(16);
  }

  void closeApp() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  Color getVisibleColor(String colorCode) {
    return getColorByHex(colorCode).withRed(getColorByHex(colorCode).red ~/ 2).withBlue(getColorByHex(colorCode).blue ~/ 2).withGreen(getColorByHex(colorCode).green ~/ 2);
  }

  /// Close the keyboard.
  void closeKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  /// Open the keyboard.
  void openKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.show');
  }

  bool isDarkColor(String color) {
    return ThemeData.estimateBrightnessForColor(utils.getColorByHex(color)) == Brightness.dark;
  }

  Color getCorrectContrastColor(String color) {
    return getDefaultWhiteOrBlackColor(isDarkColor(color));
  }

  Color getDefaultWhiteOrBlackColor(bool isWhite) {
    return isWhite ? defaultWhiteColor : defaultBlackColor;
  }

  String getFullName(UserProfile profile) {
    return profile.firstName + " " + profile.lastName;
  }

  String getFullNameByChat(ChatItem chatItem) {
    return chatItem.targetFirstName + " " + chatItem.targetLastName;
  }

  String getFullNameByNames(String lastName, String firstName) {
    return firstName + " " + lastName;
  }

  Color getColorByHex(String colorCode, {Color nullColor}) {
    if (colorCode.isNullOrEmpty) {
      return nullColor == null ? unfocusedColor : nullColor;
    } else {
      return HexColor(colorCode) ?? nullColor ?? unfocusedColor;
    }
  }

  String encodeString(String target) {
    return encrypter.encrypt(target, iv: iv).base64;
  }

  String decodeString(String encodedString) {
    return encrypter.decrypt(en.Encrypted.from64(encodedString), iv: iv);
  }

  Future<void> launchURL(String url, {String fallbackUrl}) async {
    if (fallbackUrl == null) fallbackUrl = url;
    if (!url.contains("http") && !url.contains("mailto")) {
      url = "https://" + url;
    }
    try {
      await canLaunch(url) ? launch(url, forceSafariVC: false) : launch(fallbackUrl, forceSafariVC: false);
    } on Exception catch(e) {
      toast(TextData.cantOpenUrl, bgColor: warning);
    }
  }

  String generatePassword({bool isWithLetters = true, bool isWithUppercase = true, bool isWithNumbers = true, bool isWithSpecial = false, double numberCharPassword = 8}) {

    //Define the allowed chars to use in the password
    String _lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
    String _upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String _numbers = "0123456789";
    String _special = "@#=+!£\$%&?[](){}";

    //Create the empty string that will contain the allowed chars
    String _allowedChars = "";

    //Put chars on the allowed ones based on the input values
    _allowedChars += (isWithLetters ? _lowerCaseLetters : '');
    _allowedChars += (isWithUppercase ? _upperCaseLetters : '');
    _allowedChars += (isWithNumbers ? _numbers : '');
    _allowedChars += (isWithSpecial ? _special : '');

    int i = 0;
    String _result = "";

    //Create password
    while (i < numberCharPassword.round()) {
      //Get random int
      int randomInt = Random.secure().nextInt(_allowedChars.length);
      //Get random char and append it to the password
      _result += _allowedChars[randomInt];
      i++;
    }

    return _result;
  }

  void addEmoji(TextEditingController controller, String emoji) {
    int offsetStart;
    int originalOffsetStart;
    String currentText;
    Runes sRunes;
    int emojiCount;
    var beforeInsertionText;
    var beforeInsertionRunes;
    int emojiLength = emoji.length;
    try {
      /// Sometimes, some weird emoji that can't be understood is passed into this function as an argument
      /// To filter these unwanted emojis, make sure the length of the emoji string is 2
      if (emojiLength == 2) {
        if (controller.selection.start < 0) {
          controller.text = controller.text + emoji;
        } else {
          /// Need to turn string into runes since emoji will crash app
          /// Calculate number of emoji in text before insertion point first
          /// then, since each emoji's length is 2 instead of 1 of normal character
          /// Deduct the offset by the number of emoji to get the correct offset of runes
          /// Emoji utf16 code are all larger than 125000, but i am not sure if there are any normal characters beyond 125000, so this might be a problem
          emojiCount = 0;
          offsetStart = controller.selection.start;
          originalOffsetStart = offsetStart;
          currentText = controller.text;
          sRunes = currentText.runes;
          beforeInsertionText = currentText.substring(0, offsetStart);
          beforeInsertionRunes = beforeInsertionText.runes;
          beforeInsertionRunes.forEach((int rune) {
            if (rune > 125000 && rune < 130000) {
              emojiCount++;
            }
          });
          offsetStart = offsetStart - emojiCount;
          controller.text = String.fromCharCodes(sRunes, 0, offsetStart)
              + emoji + String.fromCharCodes(sRunes, offsetStart, sRunes.length);
          if (Platform.isAndroid) {
            controller.value = controller.value.copyWith(
              text: controller.text,
              selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
              composing: TextRange.empty,
            );
          } else {
            /// This delay is needed for ios to allow jumping of cursor in text field
            /// Any milliseconds smaller than 30 will cause unstable performance
            Future.delayed(Duration(milliseconds: 30), () {
              controller.value = controller.value.copyWith(
                text: controller.text,
                selection: TextSelection(baseOffset: originalOffsetStart + 2, extentOffset: originalOffsetStart + 2),
                composing: TextRange.empty,
              );
            });
          }
        }
      }
    } on Exception catch (e) {
      print("Add emoji error: $e");
    }
  }

  String getChatId(List<String> listOfIds) {
    /// Chat id must always remain the same when:
    /// sender is A, receiver is B
    /// and
    /// receiver is A, sender is B
    /// Using integer parsed from the uuid as id is a way to make sure the id will always remain the same
    /// whenever the conversation involves A and B

    assert(listOfIds.length > 1, "List of ids must have at least two ids in order to form a chat and get the chat id");
    var uuid = Uuid();
    String chatId = "";
    SplayTreeMap<int, String> mapOfIdAndSum = SplayTreeMap();
    listOfIds.forEach((id) {
      int sum = 0;
      uuid.parse(id).forEach((value) {
        if (value.isEven) {
          sum += value;
        } else {
          sum -= value;
        }
      });
      mapOfIdAndSum[sum] = id;
    });
    var newMap = Map.fromEntries(mapOfIdAndSum.entries.toList()..sort((e1, e2) =>
        e1.key.compareTo(e2.key)));
    newMap.values.toList().forEach((id) {
      chatId += id;
    });
    return chatId;
  }

  void emailToSpinnerOfYarns({String subject = "Contacting Spinner of Yarns", String body}) async {
    if (Platform.isAndroid) {
      final emailUrl = body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body";
      await launchURL(emailUrl);
    } else if (Platform.isIOS) {
      final emailUrl = Uri.encodeFull(body == null ? "mailto:$companyEmail?subject=$subject" : "mailto:$companyEmail?subject=$subject&body=$body");
      await launchURL(emailUrl);
    } else {
      toast(TextData.notSupportedOs, bgColor: warning, iconData: warningIcon);
    }
  }

  Future<void> openFacebook(String fbId) async {
    var fallbackUrl = "https://www.facebook.com/$fbId";
    var fbProtocolUrl = Platform.isAndroid ? "fb.me/$fbId" : Platform.isIOS ? fallbackUrl : fallbackUrl;
    await launchURL(fbProtocolUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> openTwitter(String twitterId) async {
    var fallbackUrl = "https://twitter.com/$twitterId";
    await launchURL(fallbackUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> openYoutube() async {
    var fallbackUrl = "https://www.youtube.com/channel/UCdGAjAYzhEtB6wuawH0dvaA";
    await launchURL(fallbackUrl, fallbackUrl: fallbackUrl);
  }

  Future<void> launchCaller(BuildContext context, String phoneNumber) async {
    String url = "tel:$phoneNumber";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      toast(TextData.phoneDialFail, bgColor: warning, iconData: warningIcon);
    }
  }

  void switchTheme() {
    if (useBlackTheme) {
      appTheme = CustomBlack;
      appBgColor = expandableBlackColor;
      mainColor = defaultWhiteColor;
    } else {
      appTheme = CustomWhite;
      appBgColor = littleGrey;
      mainColor = defaultBlackColor;
    }
  }

  Future<void> loadPref() async {
    useBlackTheme = prefs.getBool(PrefKey.useBlackThemeKey) ?? false;
    showCountDown = prefs.getBool(PrefKey.showCountDownKey) ?? true;
    canPushNotifications = prefs.getBool(PrefKey.canPushKey) ?? true;
    showProfileDetailsDirectly = prefs.getBool(PrefKey.showProfileDetailsDirectlyKey) ?? false;
    hasShownFirstInAppDialog = prefs.getBool(PrefKey.hasShownFirstInAppDialogKey) ?? false;
    switchTheme();
    qp.uniqueId = prefs.getString(PrefKey.uniqueId) ?? "";
    qp.id = prefs.getString(PrefKey.id) ?? "";
    qp.secret = prefs.getString(PrefKey.secret) ?? "";
    qp.email = prefs.getString(PrefKey.email) ?? "";
    hasCalledFCMRegistration = prefs.getBool(PrefKey.hasRegisteredFCMKey) ?? false;
    hasShownVerifyDialog = prefs.getBool(PrefKey.hasShownVerifyDialogKey) ?? false;
    hasShownLocationDialog = prefs.getBool(PrefKey.hasShownLocationDialogKey) ?? false;
    profileDetailsOrderingList = prefs.getStringList(PrefKey.profileOrderValueKey) ?? [
      DataKey.zodiacSign,
      DataKey.distance,
      DataKey.religion,
      DataKey.gender,
      DataKey.ethnicity,
      DataKey.education,
      DataKey.graduatedFrom,
      DataKey.hobby,
      DataKey.isGreenHead,
      DataKey.preferredRelationship,
      DataKey.profession,
      DataKey.country,
      DataKey.city,
      DataKey.age,
      DataKey.likeCount,
    ];
    preferenceDetailsOrderingList = prefs.getStringList(PrefKey.preferenceOrderValueKey) ?? [
      DataKey.gender,
      DataKey.distance,
      DataKey.age,
      DataKey.ethnicity,
      DataKey.religion,
      DataKey.zodiacSign,
      DataKey.education,
      DataKey.hobby,
      DataKey.isGreenHead,
      DataKey.preferredRelationship,
      DataKey.likeCount,
    ];
    /// If any one of the above four is empty, remove all four items and set them all to null to restart the process
    if (qp.uniqueId.isNullOrEmpty || qp.id.isNullOrEmpty || qp.secret.isNullOrEmpty || qp.email.isNullOrEmpty) {
      prefs.remove(PrefKey.uniqueId);
      prefs.remove(PrefKey.id);
      prefs.remove(PrefKey.secret);
      prefs.remove(PrefKey.email);
      qp = QuickProfile(
        uniqueId: null,
        id: null,
        email: null,
        secret: null,
      );
    }
    recentEmojis = prefs.getStringList(PrefKey.recentEmoji) ?? [];
  }

  Flushbar getFlushbar(String msg, BuildContext context, {Color bgColor, IconData iconData, int seconds = 3, String title, Function onPressed, FlushbarPosition flushBarPosition = FlushbarPosition.BOTTOM}) {
    if (bgColor == null) {
      bgColor = Theme.of(context).snackBarTheme.backgroundColor;
    }
    Flushbar flushbar = Flushbar(
      mainButton: onPressed == null ? null : FlatButton(
        onPressed: onPressed,
        child: Dance(
          child: Icon(
            MdiIcons.crown,
            color: Colors.yellow,
          ),
        ),
      ),
      titleText: title != null ? Text(
        title,
        style: TextStyle(
          color: Colors.white,
          fontSize: 19.0,
          fontWeight: FontWeight.bold,
        ),
      ) : null,
      messageText: Text(
        msg,
        style: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
        ),
      ),
      icon: iconData != null ? Icon(
        iconData,
        color: Colors.white,
      ) : Container(),
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      flushbarPosition: flushBarPosition,
      flushbarStyle: FlushbarStyle.GROUNDED,
      borderRadius: 0,
      leftBarIndicatorColor: bgColor,
      backgroundColor: expandableBlackColor,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      duration: Duration(seconds: seconds),
    );
    return flushbar;
  }

  String shortStringForLongInt(int value) {
    if (value == null) {
      return "0";
    }
    if (value < 0) {
      return "0";
    }
    if (value > 999) {
      var _formattedNumber = NumberFormat.compactCurrency(
        symbol: "",
        decimalDigits: 1,
      ).format(value);
      return _formattedNumber.toLowerCase();
    } else {
      return value.toString();
    }
  }

  IconData getZodiacIcon(String zodiacSign) {
    switch (zodiacSign) {
      case TextData.aquarius:
        return MdiIcons.zodiacAquarius;
      case TextData.capricorn:
        return MdiIcons.zodiacCapricorn;
      case TextData.pisces:
        return MdiIcons.zodiacPisces;
      case TextData.aries:
        return MdiIcons.zodiacAries;
      case TextData.taurus:
        return MdiIcons.zodiacTaurus;
      case TextData.gemini:
        return MdiIcons.zodiacGemini;
      case TextData.cancer:
        return MdiIcons.zodiacCancer;
      case TextData.leo:
        return MdiIcons.zodiacLeo;
      case TextData.virgo:
        return MdiIcons.zodiacVirgo;
      case TextData.libra:
        return MdiIcons.zodiacLibra;
      case TextData.scorpio:
        return MdiIcons.zodiacScorpio;
      case TextData.sagittarius:
        return MdiIcons.zodiacSagittarius;
      default:
        return MdiIcons.spaceStation;
    }
  }

  int calculateDistance(lat1, lon1, lat2, lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return (12742 * asin(sqrt(a))).round();
  }

  String getSubscriptionDetails() {
    if (Platform.isIOS) {
      return "Recurring billing, cancel anytime. Subscriptions will be applied to your iTunes account on confirmation and will automatically renew unless canceled before the end of the current period. For more information, see our license agreement at https://www.$eulaLink and privacy policy at https://www.$privacyPolicyUrl.";
    } else {
      return "Recurring billing, cancel anytime. Subscriptions will be applied to your Google account on confirmation and will automatically renew unless canceled before the end of the current period. For more information, see our license agreement at https://www.$eulaLink and privacy policy at https://www.$privacyPolicyUrl.";
    }
  }

  String getZodiacSign(DateTime date)  {
    var days = date.day;
    var months = date.month;
    if (months == 1) {
      if (days >= 21) {
        return TextData.aquarius;
      } else {
        return TextData.capricorn;
      }
    } else if (months == 2) {
      if (days >= 20) {
        return TextData.pisces;
      }else {
        return TextData.aquarius;
      }
    } else if (months == 3) {
      if (days >= 21) {
        return TextData.aries;
      } else {
        return TextData.pisces;
      }
    } else if (months == 4) {
      if (days >= 21) {
        return TextData.taurus;
      } else {
        return TextData.aries;
      }
    } else if (months == 5) {
      if (days >= 22) {
        return TextData.gemini;
      } else {
        return TextData.taurus;
      }
    } else if (months == 6) {
      if (days >= 22) {
        return TextData.cancer;
      } else {
        return TextData.gemini;
      }
    } else if (months == 7) {
      if (days >= 23) {
        return TextData.leo;
      } else {
        return TextData.cancer;
      }
    } else if (months == 8) {
      if (days >= 23) {
        return TextData.virgo;
      } else {
        return TextData.leo;
      }
    } else if (months == 9) {
      if (days >= 24) {
        return TextData.libra;
      } else {
        return TextData.virgo;
      }
    } else if (months == 10) {
      if (days >= 24) {
        return TextData.scorpio;
      } else {
        return TextData.libra;
      }
    } else if (months == 11) {
      if (days >= 23) {
        return TextData.sagittarius;
      } else {
        return TextData.scorpio;
      }
    } else if (months == 12) {
      if (days >= 22) {
        return TextData.capricorn;
      } else {
        return TextData.sagittarius;
      }
    }
    return "";
  }

  int getAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  bool isAdult(DateTime bDay) {
    DateTime today = DateTime.now();
    DateTime adultDate = DateTime(
      bDay.year + adultAge,
      bDay.month,
      bDay.day,
    );
    return adultDate.isBefore(today);
  }

  String determineNeedS(int number, String text, {bool showNumber = true}) {
    if (number > 1) {
      if (showNumber) {
        if (text == "reply") {
          return shortStringForLongInt(number) + " " + text.replaceAll("y", "") + "ies";
        } else {
          return shortStringForLongInt(number) + " " + text + "s";
        }
      } else {
        return text + "s";
      }
    } else {
      if (showNumber) {
        return shortStringForLongInt(number) + " " + text;
      } else {
        return text;
      }
    }
  }

  void deleteChat(bool isOnlyMe, String targetId) {
    firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: qp.id).where(ChatTable.targetId, isEqualTo: targetId).get().then((result) {
      if (result.docs.isNotEmpty) {
        firestore.collection(chatDbName).doc(result.docs.first.id).delete();
      }
    });
    if (!isOnlyMe) {
      /// Delete chat for other party as well
      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: targetId).where(ChatTable.targetId, isEqualTo: qp.id).get().then((result) {
        if (result.docs.isNotEmpty) {
          firestore.collection(chatDbName).doc(result.docs.first.id).delete();
        }
      });
    }
  }

  void clearSingleMessage(String messageId) {
    firestore.collection(messageDbName).where(MessageItem.messageId, isEqualTo: messageId).get().then((result) {
      if (result.docs.isNotEmpty) {
        result.docs.forEach((messageToBeDeleted) {
          firestore.collection(messageDbName).doc(messageToBeDeleted.id).delete();
        });
      }
    });
  }

  void clearMessageInChat(String chatId, String targetId) {
    firestore.collection(messageDbName).where(MessageItem.chatId, isEqualTo: chatId).get().then((result) {
      if (result.docs.isNotEmpty) {
        result.docs.forEach((messageToBeDeleted) {
          firestore.collection(messageDbName).doc(messageToBeDeleted.id).delete();
        });
      }
      /// Update my chat item
      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: qp.id).where(ChatTable.targetId, isEqualTo: targetId).get().then((result) {
        if (result.docs.isNotEmpty) {
          firestore.collection(chatDbName).doc(result.docs.first.id).update({
            ChatTable.hasUnread: false,
            ChatTable.lastMessage: "",
            ChatTable.isLastMessageMyself: false,
            ChatTable.updatedAt: result.docs.first.data()[ChatTable.createdAt],
          });
        }
      });
      /// Update target chat item
      firestore.collection(chatDbName).where(ChatTable.senderId, isEqualTo: targetId).where(ChatTable.targetId, isEqualTo: qp.id).get().then((result) {
        if (result.docs.isNotEmpty) {
          firestore.collection(chatDbName).doc(result.docs.first.id).update({
            ChatTable.hasUnread: false,
            ChatTable.lastMessage: "",
            ChatTable.isLastMessageMyself: false,
            ChatTable.updatedAt: result.docs.first.data()[ChatTable.createdAt],
          });
        }
      });
    });
  }

  Future<File> uintToFile(Uint8List data) async {
    final tempDir = await getTemporaryDirectory();
    final _tempFile = await File('${tempDir.path}/${Uuid().v4()}.jpg').create();
    _tempFile.writeAsBytesSync(data);
    return _tempFile;
  }

  toast(String msg, {bool isSuccess = false, bool isWarning = false, Color bgColor, IconData iconData, int seconds = 5, String title, Function onPressed, FlushbarPosition flushBarPosition = FlushbarPosition.BOTTOM}) {
    if (isSuccess) {
      iconData = successIcon;
      bgColor = success;
    }
    if (isWarning) {
      iconData = warningIcon;
      bgColor = warning;
    }
    Flushbar flushbar = getFlushbar(msg, getRoute.getContext(), bgColor: bgColor, iconData: iconData, title: title, seconds: seconds, onPressed: onPressed, flushBarPosition: flushBarPosition);
    bool shouldToast = true;
    flushbar
      ..onStatusChanged = (FlushbarStatus status) {
        switch (status) {
          case FlushbarStatus.SHOWING:
            shouldToast = false;
            break;
          case FlushbarStatus.IS_APPEARING:
            shouldToast = false;
            break;
          case FlushbarStatus.IS_HIDING:
            shouldToast = false;
            break;
          case FlushbarStatus.DISMISSED:
            shouldToast = true;
            break;
        }
      };
    if (shouldToast) {
      try {
        if (getRoute.getContext() != null) {
          flushbar..show(getRoute.getContext());
        }
      } on Exception catch (e) {
        print("Toast exception: $e");
      }
    }
  }

  saveStr(String key, String value) {
    prefs.setString(key, value);
  }

  saveStrList(String key, List<String> value) {
    prefs.setStringList(key, value);
  }

  saveInt(String key, int value) {
    prefs.setInt(key, value);
  }

  saveBoo(String key, bool value) {
    prefs.setBool(key, value);
  }

  saveDouble(String key, double value) {
    prefs.setDouble(key, value);
  }

  TextStyle getNameTextStyle(Color textColor, bool bold) {
    if (bold) {
      return TextStyle(
        color: textColor,
        fontWeight: FontWeight.w500,
        fontSize: bigFontSize,
      );
    } else {
      return TextStyle(
        color: textColor,
        fontSize: commonFontSize,
      );
    }
  }

  restartApp() {
    RestartMainWidget.restartApp();
  }

  String getUserTemplate(String nickname) {
    return userTag + nickname;
  }

  String getTextAfterUserTag(String value) {
    return value.split(userTag).last;
  }

  void addToUserDataMap(String nickname, String colorCode, String id) {
    if (!nicknameVsColorMap.containsKey(nickname)) {
      nicknameVsColorMap[nickname] = colorCode;
    }
    if (!nicknameVsIdMap.containsKey(nickname)) {
      nicknameVsIdMap[nickname] = id;
    }
  }

  bool isMatch(String detailName, UserPreference pref, UserProfile profile, {bool isUsingMyPreference = true, UserProfile targetProfile}) {
    switch (detailName) {
      case DataKey.zodiacSign:
        return pref.zodiacSign.contains(profile.zodiacSign);
      case DataKey.distance:
        if (isUsingMyPreference) {
          return pref.distance >= utils.calculateDistance(myProfile.latitude, myProfile.longitude, profile.latitude, profile.longitude);
        }
        return pref.distance >= utils.calculateDistance(targetProfile.latitude, targetProfile.longitude, profile.latitude, profile.longitude);
      case DataKey.religion:
        return pref.religion.contains(profile.religion);
      case DataKey.ethnicity:
        return pref.ethnicity.contains(profile.ethnicity);
      case DataKey.education:
        return pref.education.contains(profile.education);
      case DataKey.hobby:
        return pref.hobby.contains(profile.hobby);
      case DataKey.isGreenHead:
        return pref.isGreenHead == profile.noLoveExperience;
      case DataKey.preferredRelationship:
        return pref.preferredRelationship == profile.preferredRelationship;
      case DataKey.age:
        return pref.age.first >= profile.age && pref.age.last <= profile.age;
      case DataKey.gender:
        return pref.gender.contains(profile.gender);
      case DataKey.likeCount:
        return pref.likeCounts.first >= profile.likedCount && pref.likeCounts.last <= profile.likedCount;
      default:
        return false;
    }
  }

  String getProfileValue(String detailName, UserProfile _thisProfile, int index) {

    if (index == 0 && detailName.isNullOrEmpty) {
      detailName = DataKey.age;
    }

    if (index == 1 && detailName.isNullOrEmpty) {
      detailName = DataKey.city;
    }

    switch (detailName) {
      case DataKey.name:
        return utils.getFullName(_thisProfile);
      case DataKey.zodiacSign:
        return _thisProfile.zodiacSign;
      case DataKey.distance:
        return utils.calculateDistance(myProfile.latitude, myProfile.longitude, _thisProfile.latitude, _thisProfile.longitude).toString() + distanceSuffix + " from you";
      case DataKey.religion:
        return _thisProfile.religion;
      case DataKey.ethnicity:
        return _thisProfile.ethnicity;
      case DataKey.education:
        return _thisProfile.education;
      case DataKey.graduatedFrom:
        return _thisProfile.graduatedFrom;
      case DataKey.hobby:
        return _thisProfile.hobby;
      case DataKey.isGreenHead:
        return _thisProfile.noLoveExperience ? TextData.noExperience : TextData.hasExperience;
      case DataKey.preferredRelationship:
        return PreferredRelationshipStatus.relationshipMap[_thisProfile.preferredRelationship];
      case DataKey.profession:
        return _thisProfile.profession;
      case DataKey.country:
        return _thisProfile.country;
      case DataKey.city:
        return _thisProfile.city;
      case DataKey.age:
        return _thisProfile.age.toString();
      case DataKey.personality:
        return _thisProfile.personality;
      case DataKey.likeCount:
        return utils.shortStringForLongInt(_thisProfile.likedCount);
      case DataKey.gender:
        return GenderStatus.genderMap[_thisProfile.gender];
      default:
        return "";
    }
  }
}
