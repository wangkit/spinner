import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:confetti/confetti.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:lottie/lottie.dart';
import 'package:purchases_flutter/package_wrapper.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/bottom_sheet/confirm_purchase_bottom_sheet.dart';
import 'package:spinner/view/view.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/button/toggle_button/custom_toggle_buttons.dart';
import 'package:spinner/widget/button/toggle_button/custom_toggle_switch.dart';
import 'package:spinner/widget/smart_text/smart_text.dart';
import 'package:spinner/widget/tag/tag.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:toggle_switch/toggle_switch.dart';

void getSaleBottomSheet({String optionalMessage}) {

  List<String> _marketingIcons = [
    "profile.json",
    "notification.json",
    "chat.json",
    "refresh.json",
    "preference.json",
    "time.json",
  ];
  List<String> _marketingTitles = [
    "Get up to 20 extra profiles",
    "Who find you attractive?",
    "Start chatting right now",
    "No more waiting",
    "Are you someone's dream partner?",
    "Know more about them",
  ];
  List<String> _marketingMessages = [
    "Increase number of profiles retrieved in discover page and home page",
    "Full access to profiles of people who have interacted with your profile",
    "Why spend time liking if you can start chatting right now?",
    "Get new profiles without waiting for up to 3 times a day",
    "Peep into other's preference list to see if you are a good match for them",
    "View activity report of your potential matches",
  ];
  String targetKid = VipSubscription.halfYearKid;

  showModalBottomSheet(
    backgroundColor: appBgColor,
    isScrollControlled: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(commonRadius),
    ),
    context: getRoute.getContext(),
    builder: (BuildContext ctx) {
      return SafeArea(
        child: FlipInY(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 20,),
                Container(
                  width: deviceWidth,
                  height: deviceWidth * 0.7,
                  child: Swiper(
                    itemCount: _marketingIcons.length,
                    autoplay: true,
                    autoplayDisableOnInteraction: true,
                    autoplayDelay: 3000,
                    pagination: SwiperPagination(
                      builder: DotSwiperPaginationBuilder(
                        activeColor: utils.getVisibleColor(myProfile.color),
                        color: unfocusedColor,
                      )
                    ),
                    itemWidth: deviceWidth,
                    itemBuilder: (ctx, index) {
                      return Column(
                        children: [
                          Container(
                            width: deviceWidth * 0.35,
                            height: deviceWidth * 0.35,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(commonRadius),
                                child: LottieBuilder.asset(
                                  "assets/lottie/" + _marketingIcons[index],
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 18.0, right: 18.0, top: 4.0),
                            child: Text(
                              _marketingTitles[index],
                              textAlign: TextAlign.center,
                              style: commonTextStyle.merge(
                                TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10,),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 18.0,),
                            child: Text(
                              _marketingMessages[index],
                              textAlign: TextAlign.center,
                              style: smallTextStyle,
                            ),
                          ),
                          SizedBox(height: 20,),
                        ],
                      );
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: FutureBuilder(
                    future: iapManager.getVipProducts(),
                    builder: (ctx, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Container();
                      } else {

                        List<dynamic> _packageList = snapshot.data;

                        if (_packageList == null || _packageList.isEmpty) {
                          return ListTile(
                            leading: Icon(
                              Icons.warning,
                              color: warning,
                            ),
                            title: Text(
                              TextData.noBilling,
                              style: commonTextStyle,
                            ),
                          );
                        }

                        int length = _packageList.length - 1;
                        Package _monthPackage = _packageList.first;
                        Package _halfYearPackage = _packageList[1];
                        Package _annualPackage = _packageList[2];
                        double _height = deviceWidth / 2;
                        int _currentIndex = 1;
                        double _width = deviceWidth / 3;

                        return Column(
                          children: [
                            StatefulBuilder(
                              builder: (ctx, customState) {
                                return CustomToggleSwitch(
                                  initialLabelIndex: _currentIndex,
                                  cornerRadius: commonRadius,
                                  inactiveBgColor: Colors.grey[300],
                                  activeBgColor: appBgColor,
                                  minWidth: _width,
                                  minHeight: _height,
                                  length: length,
                                  children: List.generate(length, (index) {

                                    Package _targetPackage;
                                    int _subscriptionLength;
                                    String _price;
                                    String _currencyCode;
                                    String _period;

                                    switch (index) {
                                      case 0:
                                        _targetPackage = _monthPackage;
                                        _subscriptionLength = 1;
                                        _period = "month";
                                        break;
                                      case 1:
                                        _targetPackage = _halfYearPackage;
                                        _subscriptionLength = 6;
                                        _period = "months";
                                        break;
                                      case 2:
                                        _targetPackage = _annualPackage;
                                        _subscriptionLength = 12;
                                        _period = "months";
                                        break;
                                    }

                                    _currencyCode = _targetPackage.product.currencyCode;
                                    _price = "\$" + (_targetPackage.product.price / _subscriptionLength).toStringAsFixed(2);

                                    return Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          height: _height / 20,
                                        ),
                                        Text(
                                          _subscriptionLength.toString(),
                                          style: bigTextStyle.merge(
                                              TextStyle(
                                                fontWeight: FontWeight.w700,
                                                fontSize: 30,
                                                color: defaultBlackColor,
                                              )
                                          ),
                                        ),
                                        Text(
                                          _period,
                                          style: smallTextStyle.merge(
                                            TextStyle(
                                              color: defaultBlackColor,
                                            )
                                          ),
                                        ),
                                        SizedBox(
                                          height: _height / 20,
                                        ),
                                        Text(
                                          _currencyCode,
                                          style: smallTextStyle.merge(
                                              TextStyle(
                                                color: defaultBlackColor,
                                              )
                                          ),
                                        ),
                                        Text(
                                          _price,
                                          style: commonTextStyle.merge(
                                            TextStyle(
                                              color: defaultBlackColor,
                                            )
                                          ),
                                        ),
                                        Text(
                                          "per month",
                                          style: smallTextStyle.merge(
                                              TextStyle(
                                                color: defaultBlackColor,
                                              )
                                          ),
                                        ),
                                        SizedBox(
                                          height: _height / 20,
                                        ),
                                        _currentIndex == index && (index == 1 || index == 2) ? JelloIn(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(30),
                                              border: Border.all(
                                                color: Colors.amber,
                                              ),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.0),
                                              child: Text(
                                                index == 1 ? TextData.popular : TextData.bestValue,
                                                style: smallTextStyle.merge(
                                                  TextStyle(
                                                    color: defaultBlackColor,
                                                    fontWeight: FontWeight.w600,
                                                  )
                                                ),
                                              ),
                                            ),
                                          ),
                                        ) : Container(),
                                      ],
                                    );
                                  }),
                                  onToggle: (index) {
                                    switch (index) {
                                      case 0:
                                        targetKid = _monthPackage.product.identifier;
                                        break;
                                      case 1:
                                        targetKid = _halfYearPackage.product.identifier;
                                        break;
                                      case 2:
                                        targetKid = _annualPackage.product.identifier;
                                        break;
                                    }
                                    _currentIndex = index;
                                    customState(() {});
                                  },
                                );
                              },
                            ),
                            SizedBox(height: 10,),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: LongElevatedButton(
                                height: 45,
                                width: deviceWidth * 0.55,
                                title: TextData.continueText,
                                color: utils.getColorByHex(myProfile.color),
                                textColor: utils.isDarkColor(myProfile.color) ? defaultWhiteColor : defaultBlackColor,
                                onPressed: () async {
                                  Package _targetPackage;
                                  switch (targetKid) {
                                    case VipSubscription.monthKid:
                                      getRoute.pop();
                                      _targetPackage = _monthPackage;
                                      break;
                                    case VipSubscription.halfYearKid:
                                      getRoute.pop();
                                      _targetPackage = _halfYearPackage;
                                      break;
                                    case VipSubscription.yearKid:
                                      getRoute.pop();
                                      _targetPackage = _annualPackage;
                                      break;
                                    default:
                                      utils.toast(TextData.selectAnItemFirst, isWarning: true);
                                  }
                                  if (_targetPackage != null) {
                                    getPurchaseBottomSheet(_targetPackage);
                                  }
                                },
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                getRoute.pop();
                              },
                              child: Text(
                                TextData.later,
                                style: longButtonTextStyle,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: SmartText(
                                text: utils.getSubscriptionDetails(),
                                style: smallTextStyle.merge(
                                  TextStyle(
                                    fontSize: extraSmallFontSize
                                  )
                                ),
                                onUrlClick: (String url) {
                                  /// Yarner web doesn't need prefix
                                  utils.launchURL(url.replaceFirst("https://www.", ""));
                                },
                              ),
                            ),
                          ],
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}