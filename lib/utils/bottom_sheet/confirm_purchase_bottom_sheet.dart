import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:confetti/confetti.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:purchases_flutter/package_wrapper.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/view.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';
import 'package:spinner/widget/button/toggle_button/custom_toggle_buttons.dart';
import 'package:spinner/widget/button/toggle_button/custom_toggle_switch.dart';
import 'package:spinner/widget/smart_text/smart_text.dart';
import 'package:spinner/widget/tag/tag.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:toggle_switch/toggle_switch.dart';

void getPurchaseBottomSheet(Package targetPackage) {

  int _length;
  String _price;
  String _period;

  switch (targetPackage.product.identifier) {
    case VipSubscription.monthKid:
      _length = 1;
      _period = "month";
      break;
    case VipSubscription.halfYearKid:
      _length = 6;
      _period = "months";
      break;
    case VipSubscription.yearKid:
      _length = 12;
      _period = "months";
      break;
  }

  _price = targetPackage.product.currencyCode + "\$" + (targetPackage.product.price / _length).toStringAsFixed(2);

  Future<void> purchase() async {
    await iapManager.buyVipProduct(targetPackage.product.identifier, (bool shouldPlay) {
      if (shouldPlay) {
        getRoute.pop();
      }
    });
  }

  showModalBottomSheet(
    backgroundColor: appBgColor,
    isScrollControlled: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(commonRadius),
    ),
    context: getRoute.getContext(),
    builder: (BuildContext ctx) {
      return SafeArea(
        child: FlipInX(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 20,),
                Pulse(
                  infinite: true,
                  child: Icon(
                    MdiIcons.crown,
                    color: success,
                    size: 76,
                  ),
                ),
                SizedBox(height: 40,),
                Text(
                  TextData.spinnerVipSubscription,
                  style: commonTextStyle,
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 12.0),
                  child: Container(
                    padding: EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(commonRadius),
                      border: Border.all(
                        color: unfocusedColor
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "$_length $_period",
                              style: commonTextStyle,
                            ),
                            Text(
                              _price,
                              style: commonTextStyle,
                            ),
                          ],
                        ),
                        Divider(
                          color: unfocusedColor,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Total",
                              style: commonTextStyle,
                            ),
                            Text(
                              targetPackage.product.currencyCode + "\$" + targetPackage.product.price.toStringAsFixed(2),
                              style: commonTextStyle,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                LongElevatedButton(
                  title: TextData.buyNow,
                  textColor: utils.isDarkColor(myProfile.color) ? defaultWhiteColor : defaultBlackColor,
                  color: utils.getColorByHex(myProfile.color),
                  onPressed: () async {
                    await purchase();
                  },
                ),
                SizedBox(height: 10,),
                TextButton(
                  onPressed: () {
                    getRoute.pop();
                  },
                  child: Text(
                    TextData.notNow,
                    style: longButtonTextStyle,
                  ),
                ),
                SizedBox(height: 20,),
              ],
            ),
          ),
        ),
      );
    },
  );
}