import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/view/dialogs/custom_dialog.dart';
import 'package:spinner/widget/dialog/negative_button.dart';
import 'package:spinner/widget/dialog/positive_button.dart';

getFirstVerifyDialog() {

  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: false,
    ),
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          TextData.firstVerify,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Icon(
              MdiIcons.robot,
              color: errorColor,
              size: 36,
            ),
            Icon(
              Icons.verified_user,
              color: success,
              size: 36,
            ),
          ],
        ),
        actions: <Widget>[
          PositiveButton(
            useFlexible: false,
            onPressed: () {
              hasShownVerifyDialog = true;
              utils.saveBoo(PrefKey.hasShownVerifyDialogKey, hasShownVerifyDialog);
            },
            color: success,
            text: TextData.ok,
            isHalfWidth: false,
          ),
        ],
      );
    },
  );
}