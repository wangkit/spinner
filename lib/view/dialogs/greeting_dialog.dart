import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/utils/bottom_sheet/sale_bottom_sheet.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/view/dialogs/custom_dialog.dart';
import 'package:spinner/widget/dialog/negative_button.dart';
import 'package:spinner/widget/dialog/positive_button.dart';

getGreetingDialog(String targetId, String name, Function onGreet, bool blockedMe) {

  TextEditingController _controller = TextEditingController();
  GlobalKey<FormState> _fieldKey = GlobalKey<FormState>();
  int _maximum = 50;

  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: true,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          "Send a greeting message to $name to increase your chance of getting a like.",
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Container(
          width: deviceWidth,
          child: Form(
            key: _fieldKey,
            child: TextFormField(
              controller: _controller,
              maxLines: null,
              maxLength: _maximum,
              style: commonTextStyle,
              validator: (value) {
                if (value.isNullOrEmpty) {
                  return TextData.noGreetingMessage;
                }
                if (value.length > _maximum) {
                  return TextData.textTooLong;
                }
                return null;
              },
              decoration: InputDecoration(
                hintStyle: commonTextStyle.merge(
                  TextStyle(
                    color: unfocusedColor,
                  )
                ),
                hintText: TextData.greetingMessage,
              ),
            ),
          ),
        ),
        actions: <Widget>[
          NegativeButton(text: TextData.cancel, color: warning,),
          PositiveButton(onPressed: () async {
            if (blockedMe) {
              utils.toast(TextData.cantSendMessageToThis, isWarning: true);
            } else if (_fieldKey.currentState.validate()) {
              getRoute.pop();
              apiManager.sendGreeting(targetId, _controller.text).then((status) {
                onGreet();
                if (status == StatusCode.ok) {
                  utils.toast(TextData.messageSent, isSuccess: true);
                }
                if (status == StatusCode.alreadyGreeted) {
                  utils.toast(TextData.alreadyGreeted, isWarning: true, onPressed: () {
                    getSaleBottomSheet();
                  });
                }
              });
            }
          }, color: success, text: TextData.send, isHalfWidth: true, autoPop: false,),
        ],
      );
    },
  );
}