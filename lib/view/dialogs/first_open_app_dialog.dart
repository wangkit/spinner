import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:spinner/app/setup/setup_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/values/remote_config_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/widget/button/long_elevated_button.dart';

void getFirstOpenAppDialog() {
  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: true,
    ),
    builder: (BuildContext context) {
      return AlertDialog(
        backgroundColor: appBgColor,
        shape: DialogConfig.dialogShape(),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () => getRoute.pop(),
                  icon: Icon(
                    Icons.clear,
                    color: mainColor,
                  ),
                ),
              ],
            ),
            LottieBuilder.asset(
              "assets/lottie/edit.json",
            ),
            Center(
              child: Text(
                remoteConfig.getString(RemoteConfigKey.firstOpenAppText),
                textAlign: TextAlign.center,
                style: commonTextStyle,
              ),
            ),
          ],
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: LongElevatedButton(
              title: TextData.editPreference,
              color: utils.getColorByHex(myProfile.color),
              onPressed: () {
                getRoute.pop();
                getRoute.navigateTo(SetupPage(
                  editPref: true,
                ));
              },
            ),
          ),
        ],
      );
    },
  );
}