import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/view/dialogs/custom_dialog.dart';
import 'package:spinner/widget/dialog/negative_button.dart';
import 'package:spinner/widget/dialog/positive_button.dart';

getBinaryDialog(String title, String content, Function onPressed,
    {Color negativeColor = warning, Color positiveColor = success, String positiveText = TextData.yes, String negativeText = TextData.no,
      Function negativeOnPressed, bool barrierDismissible = true, Function onDismiss, bool needNegativeButton = true}) {
  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: barrierDismissible,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        shape: DialogConfig.dialogShape(),
        content: Container(
          width: deviceWidth,
          child: Text(
            content,
            style: DialogConfig.dialogMessageStyle,
          ),
        ),
        actions: <Widget>[
          needNegativeButton ? NegativeButton(text: negativeText, color: negativeColor, onPressed: negativeOnPressed,) : Container(),
          PositiveButton(onPressed: onPressed, color: positiveColor, text: positiveText, isHalfWidth: needNegativeButton,),
        ],
      );
    },
  ).then((_) {
    if (onDismiss != null) {
      onDismiss();
    }
  });
}