import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/loading/loading_widget.dart';

class Dialogs {
  static Future<void> showLoadingDialog({String title}) async {
    return showModal<void>(
        context: getRoute.getContext(),
        configuration: FadeScaleTransitionConfiguration(
          barrierDismissible: false,
        ),
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(DialogConfig.dialogCornerRadius),
                  ),
                  backgroundColor: appBgColor,
                  children: <Widget> [
                    Center(
                      child: Column(
                          children: [
                            LoadingWidget(),
                            Container(height: 20,),
                            Text(
                              title ?? TextData.pleaseWait,
                              style: commonTextStyle,
                            ),
                          ]
                      ),
                    )
                  ]
              ),
          );
        }
      );
  }
}