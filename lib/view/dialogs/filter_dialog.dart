import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/status/gender_status.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/widget/button/toggle_button/custom_toggle_buttons.dart';
import 'package:spinner/widget/dialog/negative_button.dart';
import 'package:spinner/widget/dialog/positive_button.dart';
import 'package:spinner/widget/tag/tag.dart';

import 'custom_dialog.dart';

/// Put variables outside of the dialog to retain the values
double _filterDistance = myPreference.distance.toDouble();
RangeValues _filterAge = RangeValues(myPreference.age.first.toDouble(), myPreference.age.last.toDouble());
bool _hasLoveExperience = false;
List<bool> _genderSelectList = List.generate(GenderStatus.genderMap.length, (index) => index == 0 ? true : false);
bool _isDistance = false;
bool _isGender = false;
bool _isAge = false;
bool _isLoveExperience = false;

getFilterDialog(Function onConfirm, {bool barrierDismissible = true}) {

  Widget _titleWidget(String title) {
    return Padding(
      padding: EdgeInsets.all(0.0),
      child: Text(
        title,
        style: commonTextStyle,
      ),
    );
  }

  showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(
      isBarrierDismissible: barrierDismissible,
    ),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        shape: DialogConfig.dialogShape(),
        content: SingleChildScrollView(
          child: Column(
            children: [
              StatefulBuilder(
                builder: (ctx, customState) {
                  return Column(
                    children: [
                      _titleWidget(TextData.distance + ": ${_filterDistance.round().toString()}$distanceSuffix"),
                      Slider.adaptive(
                        value: _filterDistance,
                        min: minDistance.toDouble(),
                        max: maxDistance.toDouble(),
                        divisions: maxDistance - minDistance,
                        activeColor: cottonRed,
                        inactiveColor: unfocusedColor,
                        onChanged: (double value) {
                          customState(() {
                            _filterDistance = value;
                          });
                        },
                      ),
                      SizedBox(height: 20,),
                      CustomToggleButtons(
                        borderRadius: BorderRadius.circular(12.0),
                        isSelected: _genderSelectList,
                        renderBorder: false,
                        children: List.generate(GenderStatus.genderMap.length, (index) => Tag(
                          key: Key(_genderSelectList[index].toString()),
                          onPressed: () {
                            for (int i = 0; i < _genderSelectList.length; i++) {
                              _genderSelectList[i] = false;
                            }
                            customState(() {
                              _genderSelectList[index] = true;
                            });
                          },
                          colorCode: _genderSelectList[index] ? myProfile.color : utils.getHexCodeByColor(unfocusedColor),
                          text: GenderStatus.genderMap[index],
                          textColor: !_genderSelectList[index] ? mainColor: utils.getDefaultWhiteOrBlackColor(myProfile.isDarkColor),
                        ),),
                      ),
                      SizedBox(height: 20,),
                      _titleWidget(TextData.ageRange + ": ${_filterAge.start.round()} - ${_filterAge.end.round()}"),
                      RangeSlider(
                        values: _filterAge,
                        min: adultAge.toDouble(),
                        max: maxAge.toDouble(),
                        divisions: maxAge - adultAge,
                        activeColor: cottonRed,
                        inactiveColor: unfocusedColor,
                        onChanged: (RangeValues value) {
                          customState(() {
                            _filterAge = value;
                          });
                        },
                      ),
                      CheckboxListTile(
                        activeColor: appBgColor,
                        checkColor: sharp_green,
                        title: Text(
                          TextData.haveLoveExperiences,
                          style: commonTextStyle,
                        ),
                        onChanged: (bool value) {
                          customState(() {
                            _hasLoveExperience = value;
                          });
                        },
                        value: _hasLoveExperience,
                      ),
                      SizedBox(height: 10,),
                      Wrap(
                        runSpacing: 15.0,
                        children: [
                          Tag(
                            key: Key(_isDistance.toString() + TextData.filterDistance),
                            text: TextData.filterDistance,
                            textColor: _isDistance ? utils.getDefaultWhiteOrBlackColor(myProfile.isDarkColor) : mainColor,
                            colorCode: _isDistance ? myProfile.color : utils.getHexCodeByColor(unfocusedColor),
                            onPressed: () {
                              _isDistance = !_isDistance;
                              customState(() {});
                            },
                          ),
                          Tag(
                            key: Key(_isGender.toString() + TextData.filterGender),
                            text: TextData.filterGender,
                            textColor: _isGender ? utils.getDefaultWhiteOrBlackColor(myProfile.isDarkColor) : mainColor,
                            colorCode: _isGender ? myProfile.color : utils.getHexCodeByColor(unfocusedColor),
                            onPressed: () {
                              _isGender = !_isGender;
                              customState(() {});
                            },
                          ),
                          Tag(
                            key: Key(_isAge.toString() + TextData.filterAge),
                            text: TextData.filterAge,
                            textColor: _isAge ? utils.getDefaultWhiteOrBlackColor(myProfile.isDarkColor) : mainColor,
                            colorCode: _isAge ? myProfile.color : utils.getHexCodeByColor(unfocusedColor),
                            onPressed: () {
                              _isAge = !_isAge;
                              customState(() {});
                            },
                          ),
                          Tag(
                            key: Key(_isLoveExperience.toString() + TextData.filterLoveExperience),
                            text: TextData.filterLoveExperience,
                            textColor: _isLoveExperience ? utils.getDefaultWhiteOrBlackColor(myProfile.isDarkColor) : mainColor,
                            colorCode: _isLoveExperience ? myProfile.color : utils.getHexCodeByColor(unfocusedColor),
                            onPressed: () {
                              _isLoveExperience = !_isLoveExperience;
                              customState(() {});
                            },
                          ),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
        actions: <Widget>[
          NegativeButton(text: TextData.cancel, color: warning, isHalfWidth: true,),
          PositiveButton(onPressed: () => onConfirm(_filterDistance, _filterAge, _hasLoveExperience, _genderSelectList, _isDistance, _isGender, _isAge, _isLoveExperience), color: success, text: TextData.filter, isHalfWidth: true,),
        ],
      );
    },
  );
}