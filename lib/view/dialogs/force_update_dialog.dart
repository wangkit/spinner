import 'dart:io';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/config/dialog_config.dart';
import 'package:spinner/view/dialogs/custom_dialog.dart';
import 'package:spinner/widget/dialog/negative_button.dart';
import 'package:spinner/widget/dialog/positive_button.dart';

showForceUpdateDialog() {

  void onPositivePressed() async {
    final url = Platform.isAndroid ? googleStoreUrl : appleStoreUrl;
    await utils.launchURL(url);
  }

  void onNegativePressed() {
    utils.closeApp();
  }

  return showModal(
    context: getRoute.getContext(),
    configuration: DialogConfig.getTransition(isBarrierDismissible: false),
    builder: (BuildContext context) {
      return CustomAlertDialog(
        shape: DialogConfig.dialogShape(),
        title: Text(
          TextData.newVersion,
          textAlign: TextAlign.center,
          style: DialogConfig.dialogTitleStyle,
        ),
        content: Container(
          width: deviceWidth,
          child: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  TextData.newVersionMessage,
                  style: TextStyle(
                    color: mainColor,
                  ),
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          NegativeButton(text: TextData.close, onPressed: onNegativePressed),
          PositiveButton(text: TextData.update, onPressed: onPositivePressed),
        ],
      );
    },
  );
}