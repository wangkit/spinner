import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/icon_set/detail_icons.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/gender_status.dart';
import 'package:spinner/resources/status/preferred_relationship_status.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/widget/tag/tag.dart';

Widget bottomSheetPullIndicator() {
  return Padding(
    padding: EdgeInsets.all(8.0),
    child: Divider(color: unfocusedColor, thickness: 4, indent: deviceWidth * 0.43, endIndent: deviceWidth * 0.43,),
  );
}