import 'package:flutter/cupertino.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';

BoxDecoration getBackgroundBox({Color bgColor}) {
  return BoxDecoration(
    color: bgColor ?? appBgColor,
  );
}