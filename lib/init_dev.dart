import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:spinner/spinner_root.dart';
import 'package:spinner/widget/restart/restart_widget.dart';
import 'config/app_config.dart';
import 'constants/const.dart';
import 'resources/models/env.dart';

void main() async {
  BuildEnvironment.init(
    flavor: BuildFlavor.development,
    baseUrl: 'https://spinner-dev.herokuapp.com/spinner/dating/V1',
    bucketUrl: "https://cloud-cube.s3.amazonaws.com/khrmzgmdbfxq",
  );
  WidgetsFlutterBinding.ensureInitialized();
  assert(env != null);
  await Firebase.initializeApp();
  remoteConfig = await RemoteConfig.instance;
  await remoteConfig.fetch(
    expiration: Duration(seconds: 0),
  );
  await remoteConfig.activateFetched();
  prefs = await SharedPreferences.getInstance();
  useBlackTheme = prefs.getBool(PrefKey.useBlackThemeKey) ?? false;
  runApp(RestartMainWidget(
    child: SpinnerRoot(),
  ));
}