import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

/// Profile ordering list
List<String> profileDetailsOrderingList;

/// Preference ordering list
List<String> preferenceDetailsOrderingList;

/// Drawer edge
var drawerEdge = deviceWidth * 0.15;

/// Theme color
Color appTheme;

/// Main color
Color mainColor;

/// Bg color
Color appBgColor;

final numberOfMessagesPerPage = 100;

double commonRadius = 16.0;

double imageRadius = 8.0;

double commonImageRadius = 8.0;

double commonElevation = 8.0;

double nsfwScoreThreshold = 0.5;

/// warning icon
IconData warningIcon = MdiIcons.alertBox;

/// success icon
IconData successIcon = Icons.thumb_up;

/// big font size
double bigFontSize = 23.0;

/// font size
double commonFontSize = 20.0;

/// small font size
double smallFontSize = 17.0;

/// extra small font size
double extraSmallFontSize = 14.0;

/// extra big font size
double extraBigFontSize = 26.0;

/// Adult age
const adultAge = 18;

/// Poles of distance value
const minDistance = 10;
const maxDistance = 600;

/// Max age
const maxAge = 100;

/// small text style across the app
TextStyle smallTextStyle = TextStyle(
  color: mainColor,
  fontSize: smallFontSize,
);

/// big text style across the app
TextStyle bigTextStyle = TextStyle(
  color: mainColor,
  fontSize: bigFontSize,
);

/// common text style across the app
TextStyle commonTextStyle = TextStyle(
  color: mainColor,
  fontSize: commonFontSize,
);

TextStyle longButtonTextStyle = TextStyle(
  fontWeight: FontWeight.w500,
  fontSize: 19.0,
  color: mainColor,
);

Duration textAnimationSpeedDuration = Duration(milliseconds: 140);

Duration textAnimationPauseDuration = Duration(seconds: 1);

Duration textFieldOpacityAnimationDuration = Duration(milliseconds: 500);

bool hasCalledFCMRegistration = false;

const allowedProfileMedia = 6;

const double prefLikeCountMaximum = 10000;

var numberOfDetailsInCard = 2;

const distanceSuffix = "km";

var hasCalledCatKey = false;

/// use black theme
bool useBlackTheme;

/// show count down
bool showCountDown;

/// can push
bool canPushNotifications;

/// determine if need show verify image
bool hasShownVerifyDialog = false;

/// determine if show profile details directly
bool showProfileDetailsDirectly = false;

/// determine if need show location dialog
bool hasShownLocationDialog = false;