import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/network/api_services.dart';
import 'package:spinner/network/uploader.dart';
import 'package:workmanager/workmanager.dart';
import 'package:http/http.dart' as http;
import '../resources/values/data_key.dart';

class BackgroundTask {
  static const uploadFileId = "1";
  static const testingId = "2";
  static const String uploadFile = "uploadFile";
  static const String testing = "testing";
  static const String listOfImagePathKey = "listOfImagePath";
  static const String tempProfileImagesKey = "tempProfileImages";
  static const String isEditProfileKey = "isEditProfile";
  static const String emailKey = "email";
  static const String uploadPathKey = "uploadPathKey";
  static const String baseUrlKey = "baseUrlKey";

  static void registerUploadMedia(List<File> imageFiles, List<String> tempProfileImages, bool isEditProfile, String email, String uploadPath, String baseUrl, String secret, String uniqueId) {

    List<String> listOfImagePath = List.generate(imageFiles.length, (index) {
      if (imageFiles[index] != null) {
        return imageFiles[index].path;
      } else {
        return placeholderLink;
      }
    });

    Workmanager.registerOneOffTask(
        BackgroundTask.uploadFileId,
        BackgroundTask.uploadFile,
        inputData: {
          listOfImagePathKey: listOfImagePath,
          tempProfileImagesKey: tempProfileImages,
          isEditProfileKey: isEditProfile,
          emailKey: email,
          uploadPathKey: uploadPath,
          baseUrlKey: baseUrl,
          DataKey.secret: secret,
          DataKey.uniqueId: uniqueId,
        },
        constraints: Constraints(
          networkType: NetworkType.connected,
          requiresBatteryNotLow: false,
          requiresCharging: false,
          requiresDeviceIdle: false,
          requiresStorageNotLow: false,
        )
    );
  }
}

void callbackDispatcher() {
  Workmanager.executeTask((String task, Map inputData) async {

    switch (task) {
      case BackgroundTask.uploadFile:

        Uploader _uploader = Uploader();
        String _baseUrl = inputData[BackgroundTask.baseUrlKey];
        String _secret = inputData[DataKey.secret];
        String _uniqueId = inputData[DataKey.uniqueId];
        String _uploadPath = inputData[BackgroundTask.uploadPathKey];
        String _email = inputData[BackgroundTask.emailKey];
        List<String> _listOfImagePath = inputData[BackgroundTask.listOfImagePathKey].cast<String>();
        List<File> _imageFiles = List.generate(_listOfImagePath.length, (index) {
          if (_listOfImagePath[index] != placeholderLink) {
            return File(_listOfImagePath[index]);
          } else {
            return null;
          }
        });
        List<String> _imageLinks = [];
        bool _editProfile = inputData[BackgroundTask.isEditProfileKey];

        Future<void> _uploadImages() async {
          for (int i = 0; i < allowedProfileMedia; i++) {
            var _file = _imageFiles[i];
            if (_file != null) {
              /// Add ' at start and end to conform to backend's need
              _imageLinks.add("'" + await _uploader.uploadFileToFirebase(_file, false, uploadPath: _uploadPath) + "'");
            } else {
              if (_editProfile) {

                List<String> _tempProfileImages = inputData[BackgroundTask.tempProfileImagesKey].cast<String>();

                int _tempIndex = _tempProfileImages.length - 1;
                if (_tempIndex >= i) {
                  _imageLinks.add("'" + _tempProfileImages[i] + "'");
                } else {
                  _imageLinks.add("'" + placeholderLink + "'");
                }
              } else {
                _imageLinks.add("'" + placeholderLink + "'");
              }
            }
          }
        }
        await _uploadImages().then((_) {
          final url = _baseUrl + "/updateImagePath";
          Map<String, String> param = {
            DataKey.uniqueId: _uniqueId,
            DataKey.secret: _secret,
            DataKey.image: _imageLinks.toString(),
            DataKey.email: _email,
            DataKey.isVerification: false.toString(),
          };
          return http.post(url, body: param).catchError((onError) {
            debugPrint("Upload images error: $onError");
          });
        }).catchError((e) {
          debugPrint("Upload error: $e");
        });
        break;
      case BackgroundTask.testing:
        debugPrint("testing: ${BackgroundTask.testing}");
        break;
      default:
        debugPrint("nothing");
    }
    return Future.value(true);
  });
}