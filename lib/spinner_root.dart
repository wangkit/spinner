import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:spinner/app/splash/splash_page.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'config/app_config.dart';

class SpinnerRoot extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () {
        utils.quitFocus();
      },
      child: RefreshConfiguration(
        headerBuilder: () => WaterDropMaterialHeader(
          color: mainColor,
          backgroundColor: appBgColor,
        ),
        footerBuilder: () => CustomFooter(
          builder: (BuildContext context, LoadStatus status) {
            switch (status) {
              default:
                return Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: CupertinoActivityIndicator(),
                  ),
                );
            }
          },
        ),
        footerTriggerDistance: 80.0,
        headerTriggerDistance: 40.0,
        springDescription: SpringDescription(stiffness: 170, damping: 16, mass: 1.9),
        maxOverScrollExtent : 100,
        maxUnderScrollExtent: 100,
        enableScrollWhenRefreshCompleted: true,
        enableLoadingWhenFailed : true,
        hideFooterWhenNotFull: false,
        enableBallisticLoad: true,
        child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: myAppName,
          theme: ThemeData(
            appBarTheme: AppBarTheme(
              color: useBlackTheme ? splashBackgroundColor : littleGrey,
            ),
            scaffoldBackgroundColor: useBlackTheme ? expandableBlackColor : littleGrey,
            backgroundColor: useBlackTheme ? expandableBlackColor : littleGrey,
            bottomNavigationBarTheme: BottomNavigationBarThemeData(
              backgroundColor: useBlackTheme ? expandableBlackColor : littleGrey,
            ),
            textSelectionColor: myProfile == null ? unfocusedColor : utils.getColorByHex(myProfile?.color),
            textSelectionHandleColor: myProfile == null ? unfocusedColor : utils.getColorByHex(myProfile?.color),
            cursorColor: myProfile == null ? unfocusedColor : utils.getColorByHex(myProfile?.color),
            primarySwatch: useBlackTheme ? CustomBlack : CustomWhite,
            floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: cottonLightBlue,
            ),
            toggleableActiveColor: cottonLightBlue,
            fontFamily: appFontFamily,
          ),
          home: SplashPage(),
        ),
      ),
    );
  }
}

