import 'package:flutter/material.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

class UserProfile {
  String id;
  String firstName;
  String lastName;
  String email;
  String country;
  String city;
  double longitude;
  double latitude;
  int gender;
  String color;
  String birthday;
  int age;
  String ethnicity;
  String religion;
  String zodiacSign;
  String profession;
  String birthplace;
  String education;
  String graduatedFrom;
  String personality;
  int preferredRelationship;
  bool noLoveExperience;
  String hobby;
  List<String> images;
  String video;
  String verificationImage;
  String verificationInstruction;
  int status;
  String quickDisplayOne;
  String quickDisplayTwo;
  String lastActiveAt;
  String updatedAt;
  String createdAt;
  bool isLiked;
  int likedCount;
  String videoThumnbnail;
  bool isVerified;
  int notificationCount;
  String password;
  bool isRobot;
  bool isDarkColor;
  String nextFetchTime;
  bool isVip;
  bool isPush;
  int remainingRefreshCount;
  int remainingDiscoverCount;
  bool likedMe;
  bool blockedMe;
  bool isPassedOrLiked;
  bool isGreeted;
  int matchCount;
  int greetCount;
  String lastLikeTime;

  UserProfile({
    this.id,
    this.remainingRefreshCount,
    this.firstName,
    this.blockedMe,
    this.lastName,
    this.remainingDiscoverCount,
    this.password,
    this.email,
    this.country,
    this.city,
    this.longitude,
    this.latitude,
    this.videoThumnbnail,
    this.gender,
    this.color,
    this.birthday,
    this.video,
    this.verificationImage,
    this.verificationInstruction,
    this.age,
    this.ethnicity,
    this.religion,
    this.zodiacSign,
    this.profession,
    this.birthplace,
    this.quickDisplayOne,
    this.quickDisplayTwo,
    this.notificationCount,
    this.education,
    this.graduatedFrom,
    this.personality,
    this.preferredRelationship,
    this.isPassedOrLiked,
    this.noLoveExperience,
    this.hobby,
    this.images,
    this.status,
    this.likedMe,
    this.lastActiveAt,
    this.isLiked,
    this.updatedAt,
    this.createdAt,
    this.likedCount,
    this.isVerified,
    this.isRobot,
    this.isDarkColor,
    this.nextFetchTime,
    this.isVip,
    this.isPush,
    this.isGreeted,
    this.matchCount,
    this.lastLikeTime,
    this.greetCount,
  });

  Key getProfileWidgetKey(UserProfile profile) {
    /// Don't use quick display here
    return Key(profile.updatedAt.toString() + profile.lastActiveAt.toString() + profile.images.toString() + profile.color.toString() + profile.video.toString() + profile.city.toString() + profile.country.toString() + profile.firstName.toString() + profile.lastName.toString());
  }

  static List<UserProfile> generateUserProfile(List targetList) {
    return List<UserProfile>.generate(targetList.length, (index) {

      var _thisProfile = targetList[index];

      List<String> _filterImage() {
        try {
          if (_thisProfile[23] != null) {
            List<String> _tempList = _thisProfile[23].cast<String>();
            while (_tempList.contains(placeholderLink)) {
              _tempList.remove(placeholderLink);
            }
            return _tempList;
          }
          /// If we failed to upload images before preference user goes to main page, use placeholder first
          return [placeholderLink];
        } on Exception catch (e) {
          debugPrint("Exception: $e");
          return [placeholderLink];
        }
      }

      return UserProfile(
        id: _thisProfile[0],
        firstName: _thisProfile[1],
        lastName: _thisProfile[2],
        email: _thisProfile[3],
        password: utils.decodeString(_thisProfile[4]),
        country: _thisProfile[5],
        city: _thisProfile[6],
        longitude: _thisProfile[7],
        latitude: _thisProfile[8],
        gender: _thisProfile[9],
        color: _thisProfile[10],
        birthday: _thisProfile[11],
        age: _thisProfile[12],
        ethnicity: _thisProfile[13],
        religion: _thisProfile[14],
        zodiacSign: _thisProfile[15],
        profession: _thisProfile[16],
        education: _thisProfile[17],
        graduatedFrom: _thisProfile[18],
        personality: _thisProfile[19],
        preferredRelationship: _thisProfile[20],
        noLoveExperience: _thisProfile[21],
        hobby: _thisProfile[22],
        images: _filterImage(),
        verificationImage: _thisProfile[24],
        verificationInstruction: _thisProfile[25],
        video: _thisProfile[26],
        status: _thisProfile[27],
        lastActiveAt: _thisProfile[28],
        updatedAt: _thisProfile[29],
        createdAt: _thisProfile[30],
        isLiked: _thisProfile[31],
        likedCount: _thisProfile[32],
        isVerified: _thisProfile[33],
        isRobot: _thisProfile[34],
        isDarkColor: utils.isDarkColor(_thisProfile[10]),
        /// If no fetch time, it means now is the fetch time since a fetch must be done when entering app
        nextFetchTime: _thisProfile[35] ?? timeUtils.getCurrentUTCDateTime(),
        notificationCount: _thisProfile[37],
        isVip: _thisProfile[38],
        isPush: _thisProfile[39],
        remainingRefreshCount: _thisProfile[40],
        videoThumnbnail: _thisProfile[41],
        likedMe: _thisProfile[42],
        quickDisplayOne: _thisProfile[43],
        quickDisplayTwo: _thisProfile[44],
        isGreeted: _thisProfile[45] ?? false,
        remainingDiscoverCount: _thisProfile[46],
        blockedMe: _thisProfile[47],
        matchCount: _thisProfile[48],
        greetCount: _thisProfile[49],
        lastLikeTime: _thisProfile[50],
        isPassedOrLiked: _thisProfile[51],
      );
    });
  }
}

class QuickProfile {
  String uniqueId;
  String id;
  String secret;
  String password;
  String email;

  QuickProfile({
    this.uniqueId,
    this.password,
    this.id,
    this.secret,
    this.email,
  });
}

class MessageItem {
  static String messageId = 'messageId';
  static String senderEmail = 'senderEmail';
  static String receiverEmail = 'receiverEmail';
  static String message = 'message';
  static String createdAt = 'createdAt';
  static String senderColor = 'senderColor';
  static String senderName = 'senderName';
  static String receiverName = 'receiverName';
  static String senderId = 'senderId';
  static String chatId = 'chatId';
  static String receiverId = 'receiverId';
}

class ChatTable {
  static String tableName = 'chat';
  static String senderId = 'senderId';
  static String updatedAt = 'updatedAt';
  static String targetEmail = 'targetEmail';
  static String lastMessage = 'lastMessage';
  static String targetColor = 'targetColor';
  static String targetId = 'targetId';
  static String targetLastName = 'targetLastName';
  static String targetFirstName = 'targetFirstName';
  static String createdAt = 'createdAt';
  static String isLastMessageMyself = 'isLastMessageMyself';
  static String hasUnread = 'hasUnread';
  static String targetImages = 'targetImages';
}

class ChatItem {
  String senderId;
  String targetEmail;
  String lastMessage;
  String targetColor;
  String updatedAt;
  String targetId;
  String targetLastName;
  String targetFirstName;
  String createdAt;
  bool hasUnread;
  bool isLastMessageMyself;
  List<String> targetImages;

  ChatItem({
    this.senderId,
    this.targetId,
    this.updatedAt,
    this.targetImages,
    this.targetEmail,
    this.isLastMessageMyself,
    this.targetLastName,
    this.targetFirstName,
    this.targetColor,
    this.createdAt,
    this.hasUnread,
    this.lastMessage,
  });
}

class ActivityNotification {
  String notificationId;
  String creatorId;
  String creatorColor;
  String lastName;
  String firstName;
  String creatorImage;
  String targetId;
  int activity;
  String message;
  bool isRead;
  String createdAt;

  ActivityNotification({
    this.notificationId,
    this.creatorId,
    this.lastName,
    this.firstName,
    this.creatorColor,
    this.creatorImage,
    this.targetId,
    this.message,
    this.activity,
    this.isRead,
    this.createdAt,
  });

  static List<ActivityNotification> filterNotification(List<ActivityNotification> currentNotifications, List<ActivityNotification> newNotifications) {
    List<String> _currentNotificationIdList = [];
    List<ActivityNotification> _newNotificationList = [];
    currentNotifications.forEach((notification) {
      _currentNotificationIdList.add(notification.notificationId);
    });
    newNotifications.forEach((notification) {
      if (!_currentNotificationIdList.contains(notification.notificationId)) {
        _newNotificationList.add(notification);
      }
    });
    return _newNotificationList;
  }

  static List<ActivityNotification> generateNotification(List targetList) {
    return List<ActivityNotification>.generate(targetList.length, (index) {

      List _thisNotification = targetList[index];
      List _creatorDetails = _thisNotification[2];

      return ActivityNotification(
        notificationId: _thisNotification[0],
        creatorId: _thisNotification[1],
        creatorImage: _creatorDetails[0],
        creatorColor: _creatorDetails[1],
        lastName: _creatorDetails[2],
        firstName: _creatorDetails[3],
        targetId: _thisNotification[3],
        activity: _thisNotification[4],
        isRead: _thisNotification[5],
        createdAt: _thisNotification[6],
        message: _thisNotification[7],
      );
    });
  }
}

class StrictPreference {
  String id;
  bool gender;
  bool distance;
  bool age;
  bool ethnicity;
  bool religion;
  bool zodiacSign;
  bool education;
  bool hobby;
  bool isGreenHead;
  bool preferredRelationship;
  String updatedAt;
  bool likeCounts;

  StrictPreference({
    this.id,
    this.gender,
    this.distance,
    this.age,
    this.ethnicity,
    this.religion,
    this.zodiacSign,
    this.education,
    this.hobby,
    this.isGreenHead,
    this.preferredRelationship,
    this.updatedAt,
    this.likeCounts,
  });

  Key getPreferenceWidgetKey(StrictPreference pref) {
    return Key(pref.updatedAt.toString() + pref.gender.toString());
  }

  static List<StrictPreference> generateStrictPreference(List targetList) {
    return List<StrictPreference>.generate(targetList.length, (index) {

      var _thisPreference = targetList[index];

      return StrictPreference(
        id: _thisPreference[0],
        gender: _thisPreference[1],
        distance: _thisPreference[2],
        age: _thisPreference[3],
        ethnicity: _thisPreference[4],
        religion: _thisPreference[5],
        zodiacSign: _thisPreference[6],
        education: _thisPreference[7],
        hobby: _thisPreference[8],
        isGreenHead: _thisPreference[9],
        preferredRelationship: _thisPreference[10],
        likeCounts: _thisPreference[11],
        updatedAt: _thisPreference[12],
      );
    });
  }
}

class UserPreference {
  String id;
  List<int> gender;
  int distance;
  List<int> age;
  List<String> ethnicity;
  List<String> religion;
  List<String> zodiacSign;
  List<String> education;
  List<String> hobby;
  bool isGreenHead;
  int preferredRelationship;
  String updatedAt;
  List<int> likeCounts;

  UserPreference({
    this.id,
    this.gender,
    this.distance,
    this.age,
    this.ethnicity,
    this.religion,
    this.zodiacSign,
    this.education,
    this.hobby,
    this.isGreenHead,
    this.preferredRelationship,
    this.updatedAt,
    this.likeCounts,
  });

  Key getPreferenceWidgetKey(UserPreference pref) {
    return Key(pref.updatedAt.toString() + pref.gender.toString());
  }

  static List<UserPreference> generateUserPreference(List targetList) {
    return List<UserPreference>.generate(targetList.length, (index) {

      var _thisPreference = targetList[index];

      return UserPreference(
        gender: _thisPreference[0].cast<int>(),
        distance: _thisPreference[1],
        age: _thisPreference[2].cast<int>(),
        ethnicity: _thisPreference[3].cast<String>(),
        religion: _thisPreference[4].cast<String>(),
        zodiacSign: _thisPreference[5].cast<String>(),
        education: _thisPreference[6].cast<String>(),
        hobby: _thisPreference[7].cast<String>(),
        isGreenHead: _thisPreference[8],
        preferredRelationship: _thisPreference[9],
        updatedAt: _thisPreference[10],
        likeCounts: _thisPreference[11].cast<int>(),
        id: _thisPreference[12],
      );
    });
  }
}

class BlockReason {
  static const int verbalAbuse = 0;
  static const int sexualHarassment = 1;
  static const int annoying = 2;
  static const int unreasonable = 3;
  static const int noReason = 4;

  static const Map<int, String> blockMap = {
    verbalAbuse: "Verbal Abuse",
    sexualHarassment: "Sexual Harassment",
    annoying: "Annoying",
    unreasonable: "Unreasonable",
    noReason: "No Reason",
  };
}