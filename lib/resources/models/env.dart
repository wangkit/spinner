import 'package:meta/meta.dart';

enum BuildFlavor { production, development, staging }

BuildEnvironment get env => _env;
BuildEnvironment _env;

class BuildEnvironment {
  /// Backend url
  final String baseUrl;

  /// Environment as stated in enum in line 3
  final BuildFlavor flavor;

  /// Url to fetch data from bucket
  final String bucketUrl;

  BuildEnvironment._init({this.bucketUrl, this.flavor, this.baseUrl,});

  /// Init only once
  static void init({@required flavor, @required baseUrl, @required bucketUrl,}) =>
      _env ??= BuildEnvironment._init(flavor: flavor, baseUrl: baseUrl, bucketUrl: bucketUrl);
}