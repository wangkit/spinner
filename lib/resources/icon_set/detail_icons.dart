import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class DetailIcons {
  static const IconData name = MdiIcons.identifier;
  static const IconData distance = Icons.map;
  static const IconData zodiacSign = MdiIcons.star;
  static const IconData religion = MdiIcons.church;
  static const IconData ethnicity = Icons.people;
  static const IconData education = MdiIcons.doctor;
  static const IconData graduatedFrom = MdiIcons.school;
  static const IconData hobby = MdiIcons.football;
  static const IconData isGreenHead = MdiIcons.heartPulse;
  static const IconData preferredRelationship = MdiIcons.heartBox;
  static const IconData profession = MdiIcons.bagPersonal;
  static const IconData country = MdiIcons.flag;
  static const IconData city = MdiIcons.city;
  static const IconData age = MdiIcons.numeric0Box;
  static const IconData likeCount = MdiIcons.heart;
  static const IconData personality = Icons.person;
  static const IconData gender = MdiIcons.genderMaleFemale;
}