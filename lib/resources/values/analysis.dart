import 'package:spinner/app/main/settings/settings_page.dart';

class Analysis {
  static const String newMemberButtonClick = "NEW_MEMBER_BUTTON_CLICK";
  static const String existingMemberButtonClick = "EXISTING_MEMBER_BUTTON_CLICK";
  static const String setupNextPageButtonClick = "SET_UP_NEXT_PAGE_BUTTON_CLICK";
  static const String getOtp = "GET_OTP";
  static const String resendOtp = "RESEND_OTP";
  static const String startCreateProfile = "START_CREATE_PROFILE";
  static const String fillPref = "FILL_PREF_RELATIONSHIP";
  static const String fillName = "FILL_NAME";
  static const String getLocationBtn = "GET_LOCATION_BUTTON";
  static const String fillLocation = "FILL_LOCATION";
  static const String fillGender = "FILL_GENDER";
  static const String fillBDay = "FILL_BIRTHDAY";
  static const String fillExp = "FILL_EXPERIENCE";
  static const String fillRegisterEmail = "FILL_REGISTER_EMAIL";
  static const String fillLoginEmail = "FILL_LOGIN_EMAIL";
  static const String fillEthnAndReli = "FILL_ETHNICITY_AND_RELIGION";
  static const String fillEdu = "FILL_EDUCATION";
  static const String fillProfession = "FILL_PROFESSION";
  static const String fillHobby = "FILL_HOBBY";
  static const String fillColor = "FILL_COLOR";
  static const String uploadImage = "UPLOAD_IMAGES";
  static const String uploadVideo = "UPLOAD_VIDEO";
  static const String uploadVerificationImage = "UPLOAD_VERIFICATION_IMAGE";
  static const String createProfile = "CREATE_PROFILE";
  static const String editProfile = "EDIT_PROFILE";
  static const String prefGender = "PREF_GENDER";
  static const String prefDistance = "PREF_DISTANCE";
  static const String prefAge = "PREF_AGE";
  static const String prefCommon = "PREF_COMMON_";
  static const String prefHobby = "PREF_HOBBY";
  static const String prefExperience = "PREF_EXPERIENCE";
  static const String prefLikeCount = "PREF_LIKE_COUNT";
  static const String createPref = "CREATE_PREFERENCE";
}
