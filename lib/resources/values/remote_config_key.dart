class RemoteConfigKey {
  static const splashBackgroundColor = 'splashBackgroundColor';
  static const startBackgroundUseGradient = 'startBackgroundUseGradient';
  static const startBackgroundTopColor = 'startBackgroundTopColor';
  static const startBackgroundBottomColor = 'startBackgroundBottomColor';
  static const startPageQuote = "startPageQuote";
  static const startBackgroundNewMemberButtonColor = 'startBackgroundNewMemberButtonColor';
  static const minimumVersion = "minimumVersion";
  static const firstOpenAppText = "firstOpenAppText";
  static const newUserPersonality = "newUserPersonality";
  static const checkFace = "checkFace";
  static const androidBackgroundUpload = "androidBackgroundUpload";
  static const enforceMinimumVersion = "enforceMinimumVersion";
  static const appleStoreUrl = "appleStoreUrl";
  static const googleStoreUrl = "googleStoreUrl";
}