class PrefKey {
  static String secret = "secretKey";
  static String password = "passwordKey";
  static String id = "idKey";
  static String accountColor = "accountColorKey";
  static String recentEmoji = "emojiRecentKey";
  static String uniqueId = "uniqueIdKey";
  static String email = "emailKey";
  static String useBlackThemeKey = "UseBlackThemeKey";
  static String hasShownFirstInAppDialogKey = "hasShownFirstInAppDialogKey";
  static String hasRegisteredFCMKey = "hasRegisteredFCMKey";
  static String profileOrderValueKey = "profileOrderMapValueKey";
  static String preferenceOrderValueKey = "preferenceDetailsOrderingListValueKey";
  static String canPushKey = "canPushKey";
  static String showProfileDetailsDirectlyKey = "showProfileDetailsDirectlyKey";
  static String hasShownVerifyDialogKey = "hasShownVerifyDialogKey";
  static String hasShownLocationDialogKey = "hasShownLocationDialogKey";
  static String showCountDownKey = "showCountDownKey";
}