class Action {
  static const int getVipProductDetails = 0;
  static const int pendingPurchaseVip = 1;
  static const int finishPurchaseVip = 2;
  static const int cancelPurchase = 3;
}