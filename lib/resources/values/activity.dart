class Activity {
  static const likeProfile = 0;
  static const unlikeProfile = 1;
  static const verifyAsRobot = 2;
  static const verifyAsUser = 3;
  static const viewProfile = 4;
  static const matched = 5;
  static const greeting = 6;
  static const discoverLike = 7;

  static const Map<int, String> activityMap = {
    likeProfile: " liked you.",
    unlikeProfile: " unliked you.",
    verifyAsRobot: " verified you as a robot.",
    verifyAsUser: " verified you as a human.",
    viewProfile: " viewed your profile.",
    matched: " and you matched!💞",
    greeting: " said: ",
    discoverLike: " discovered and liked your profile.",
  };
}