import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:spinner/resources/models/env.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';

class Uploader {

  String getUploadPath() {
    String referenceName = "";
    switch (env.flavor) {
      case BuildFlavor.production:
        referenceName = "prod/";
        break;
      case BuildFlavor.development:
        referenceName = "dev/";
        break;
      case BuildFlavor.staging:
        referenceName = "staging/";
        break;
    }
    return referenceName;
  }

  Future<String> uploadFileToFirebase(File file, bool isVideo, {String uploadPath}) async {
    try {
      String fileName = path.basename(file.path);
      String referenceName;
      String folderName;
      if (uploadPath.isNullOrEmpty) {
        referenceName = getUploadPath();
      } else {
        referenceName = uploadPath;
      }
      if (isVideo) {
        folderName = "video";
      } else {
        folderName = "image";
      }
      final StorageReference firebaseStorageRef = FirebaseStorage.instance
          .ref()
          .child("$referenceName/profile/$folderName/$fileName");
      StorageUploadTask uploadTask = firebaseStorageRef.putFile(file);
      StorageTaskSnapshot storageSnapshot = await uploadTask.onComplete;
      var downloadUrl = await storageSnapshot.ref.getDownloadURL();
      if (uploadTask.isComplete) {
        var url = downloadUrl.toString();
        return url;
      }
    } on Exception catch (e) {
      debugPrint("Firebase error: $e");
    }
    return null;
  }
}