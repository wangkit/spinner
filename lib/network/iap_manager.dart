import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/values/action.dart' as act;
import 'package:spinner/resources/values/text.dart';

class IapManager {

  Future<bool> isVipActive() async {
    PurchaserInfo purchaserInfo = await Purchases.getPurchaserInfo();
    if (purchaserInfo.activeSubscriptions.isNotEmpty) {
      for (int i = 0; i < purchaserInfo.activeSubscriptions.length; i++) {
        if (purchaserInfo.activeSubscriptions[i] == VipSubscription.monthKid ||
            purchaserInfo.activeSubscriptions[i] == VipSubscription.yearKid ||
            purchaserInfo.activeSubscriptions[i] == VipSubscription.halfYearKid) {
          debugPrint("-------------------------------------This is VIP-------------------------------------");
          return true;
        }
      }
    }
    debugPrint("-------------------------------------Not VIP-------------------------------------");
    return false;
  }

  Future<List<dynamic>> getVipProducts() async {
    Offerings offerings = await Purchases.getOfferings();
    PurchaserInfo purchaserInfo = await Purchases.getPurchaserInfo();
    apiManager.actionLog(act.Action.getVipProductDetails);
    if (offerings.current != null && offerings.current.availablePackages.isNotEmpty) {
      return [offerings.current.monthly, offerings.current.sixMonth, offerings.current.annual, purchaserInfo.allPurchasedProductIdentifiers.isNotEmpty];
    } else {
      utils.toast(TextData.noProductAvailable, isWarning: true);
      return null;
    }
  }

  Future<void> buyVipProduct(String targetKid, Function hasBought) async {
    Offerings offerings = await Purchases.getOfferings();
    if (offerings.current != null && offerings.current.availablePackages.isNotEmpty) {
      try {
        Package _vipPackage;
        offerings.current.availablePackages.forEach((package) {
          if (package.product.identifier == targetKid) {
            _vipPackage = package;
          }
        });
        apiManager.actionLog(act.Action.pendingPurchaseVip);
        PurchaserInfo purchaserInfo = await Purchases.purchasePackage(_vipPackage);
        if (purchaserInfo.activeSubscriptions.isNotEmpty) {
          bool _needUpdate = false;
          for (int i = 0; i < purchaserInfo.activeSubscriptions.length; i++) {
            if (purchaserInfo.activeSubscriptions[i] == VipSubscription.monthKid ||
                purchaserInfo.activeSubscriptions[i] == VipSubscription.yearKid ||
                purchaserInfo.activeSubscriptions[i] == VipSubscription.halfYearKid) {
              _needUpdate = true;
            }
          }
          if (_needUpdate) {
            /// Send confetti
            hasBought(true);
            /// Set the user as vip first
            myProfile.isVip = true;
            apiManager.setVip();
            /// Then fill the logs
            apiManager.billingLog(_vipPackage.product.identifier, _vipPackage.product.price.toString(), _vipPackage.product.currencyCode);
            apiManager.actionLog(act.Action.finishPurchaseVip);
          }
        }
      } on PlatformException catch (e) {
        var errorCode = PurchasesErrorHelper.getErrorCode(e);
        utils.toast(TextData.purchaseIncomplete, isWarning: true);
        if (errorCode == PurchasesErrorCode.purchaseCancelledError) {
          apiManager.actionLog(act.Action.cancelPurchase);
        }
      } on Exception catch (e) {
        debugPrint("Billing error: $e");
      }
    } else {
      utils.toast(TextData.noProductAvailable, isWarning: true);
    }
  }
}