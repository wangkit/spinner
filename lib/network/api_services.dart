import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:device_info/device_info.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import 'package:spinner/app/splash/splash_page.dart';
import 'package:spinner/config/app_config.dart';
import 'package:spinner/constants/const.dart';
import 'package:spinner/resources/color/color.dart';
import 'package:spinner/resources/models/env.dart';
import 'package:spinner/resources/models/models.dart';
import 'package:spinner/resources/status/status_code.dart';
import 'package:spinner/resources/values/data_key.dart';
import 'package:spinner/resources/values/pref_key.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'dart:io';
import 'package:spinner/resources/values/remote_config_key.dart';
import 'package:spinner/resources/values/text.dart';
import 'package:spinner/utils/extensions/string_extensions.dart';
import 'package:spinner/view/dialogs/binary_dialog.dart';
import 'package:spinner/widget/restart/restart_widget.dart';
import  'package:string_similarity/string_similarity.dart';

class ApiManager {

  Future<void> getAllLists() async {

    Future<List<String>> _getListFromLink(String link, String dataName) async {
      final response = await http.get(link);
      var jsonResponse = json.decode(response.body);
      var list = jsonResponse[dataName];
      List<String> _result = list.cast<String>();
      List<String> _tempList = [];
      _result.sort((a, b) {return a.toLowerCase().compareTo(b.toLowerCase());});
      _result.forEach((text) {
        _tempList.add(text.titleCase);
      });
      return _tempList;
    }

    try {
      countryList = await _getListFromLink(countryLink, DataKey.countries);
      ethnicityList = await _getListFromLink(ethnicityLink, DataKey.ethnicities);
      religionList = await _getListFromLink(religionLink, DataKey.religions);
      schoolList = await _getListFromLink(schoolLink, DataKey.schools);
      educationLevelList = await _getListFromLink(educationLink, DataKey.education);
      professionList = await _getListFromLink(professionLink, DataKey.professions);
      hobbyList = await _getListFromLink(hobbyLink, DataKey.hobbies);
      instructionList = await _getListFromLink(instructionLink, DataKey.instruction);
    } on Exception catch (e) {
      debugPrint("Exception: $e");
    }
  }

  Future<void> updateQuickDisplay(String quickDisplayOne, String quickDisplayTwo) async {
    final url = apiBaseUrl + "/updateQuickDisplay";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
      DataKey.quickDisplayOne: quickDisplayOne ?? "",
      DataKey.quickDisplayTwo: quickDisplayTwo ?? "",
    };
    return http.post(url, body: param);
  }

  Future<dynamic> sendGreeting(String targetId, String message) async {
    final url = apiBaseUrl + "/sendGreeting";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
      DataKey.targetId: targetId,
      DataKey.message: message,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse[DataKey.status];
      } else {
        return StatusCode.exception;
      }
    }).catchError((onError) {
      debugPrint("Send greeting error: $onError");
      return StatusCode.exception;
    });
  }

  Future<void> readAllNotifications() async {
    final url = apiBaseUrl + "/readAllNotifications";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
    };
    http.post(url, body: param);
  }

  Future<dynamic> editStrictPreference() async {
    final url = apiBaseUrl + "/editStrictPreference";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
      DataKey.distance: myStrictPreference.distance.toString(),
      DataKey.religion: myStrictPreference.religion.toString(),
      DataKey.zodiacSign: myStrictPreference.zodiacSign.toString(),
      DataKey.education: myStrictPreference.education.toString(),
      DataKey.ethnicity: myStrictPreference.ethnicity.toString(),
      DataKey.gender: myStrictPreference.gender.toString(),
      DataKey.age: myStrictPreference.age.toString(),
      DataKey.preferredRelationship: myStrictPreference.preferredRelationship.toString(),
      DataKey.isGreenHead: myStrictPreference.isGreenHead.toString(),
      DataKey.hobby: myStrictPreference.hobby.toString(),
      DataKey.likeCount: myStrictPreference.likeCounts.toString(),
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse[DataKey.status];
      } else {
        return StatusCode.exception;
      }
    }).catchError((onError) {
      debugPrint("Edit strict preference error: $onError");
      return StatusCode.exception;
    });
  }

  Future<dynamic> flipIsPush() async {
    final url = apiBaseUrl + "/flipIsPush";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
      DataKey.value: canPushNotifications.toString(),
    };
    return http.post(url, body: param);
  }

  Future<void> billingLog(String productId, String price, String currencyCode) async {
    final url = apiBaseUrl + "/billing";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.productId: productId ?? "",
      DataKey.price: price ?? "",
      DataKey.currencyCode: currencyCode ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
    };
    return http.post(url, body: param).catchError((e) {
      debugPrint("billing error: $e");
    });
  }

  Future<void> setVip() async {
    final url = apiBaseUrl + "/setVip";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
    };
    return http.post(url, body: param);
  }

  Future<void> setNoVip() async {
    final url = apiBaseUrl + "/setNoVip";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
    };
    return http.post(url, body: param);
  }

  Future<void> actionLog(int action) async {
    final url = apiBaseUrl + "/actionLog";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.action: action.toString() ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
    };
    return http.post(url, body: param);
  }

  Future<dynamic> getPpById(String targetId,) async {
    final url = apiBaseUrl + "/getPpById";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.targetId: targetId ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
    };
    return http.post(url, body: param).then((http.Response response) {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((e) {
      debugPrint("Get pp by id error: $e");
    });
  }

  Future<void> signInToFirebase({String email, String password}) async {
   try {
     if (!hasCalledFCMRegistration) {
       firebaseMessaging.getToken().then((String token) {
         if (!token.isNullOrEmpty) {
           apiManager.registerFcm(token);
         }
       });
     }
     firebaseUser = (await firebaseAuth.signInWithEmailAndPassword(
       email: email != null ? email : qp.email,
       password: password != null ? password : qp.password,
     )).user;
   } on Exception catch (e) {
     debugPrint("catch: $e");
   }
  }

  Future<void> createAccountWithEmailAndPassword(String email, String password) async {
    try {
      firebaseUser = (await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      )).user;
    } on Exception catch (e) {
      debugPrint("catch: $e");
    }
  }

  Future<void> getMyProfileAndPreference({String email}) async {

    void _onFailure() {
      getBinaryDialog(TextData.startUpFailure, TextData.tryRestart, () {
        utils.restartApp();
      }, needNegativeButton: false);
    }

    final url = apiBaseUrl + "/getMyProfileAndPreference";
    Map<String, String> param = {
      DataKey.id: qp.id ?? "",
      DataKey.email: email ?? qp.email ?? "",
      DataKey.uniqueId: qp.uniqueId ?? "",
      DataKey.secret: qp.secret ?? "",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        List<dynamic> myProfileList = jsonResponse[DataKey.myProfile];
        List<dynamic> myPreferenceList = jsonResponse[DataKey.myPreference];
        List<dynamic> myStrictList = jsonResponse[DataKey.myStrictPreference];
        var jsonStatus = jsonResponse[DataKey.status];
        catKey = jsonResponse[DataKey.catKey];
        if (jsonStatus == StatusCode.ok) {
          debugPrint("myProfileList: $myProfileList");
          debugPrint("myPreferenceList: $myPreferenceList");
          debugPrint("myStrictList: $myStrictList");
          if (myProfileList.length > 0) {
            myProfile = UserProfile.generateUserProfile(myProfileList).first;
          }
          if (myPreferenceList.length > 0) {
            myPreference = UserPreference.generateUserPreference(myPreferenceList).first;
          }
          if (myStrictList.length > 0) {
            myStrictPreference = StrictPreference.generateStrictPreference(myStrictList).first;
          }
          qp.email = myProfile.email;
          qp.id = myProfile.id;
          qp.password = myProfile.password;
          utils.saveStr(PrefKey.accountColor, myProfile.color);
          utils.saveStr(PrefKey.email, qp.email);
          utils.saveStr(PrefKey.id, qp.id);
          utils.saveStr(PrefKey.password, qp.password);
          if (!hasCalledCatKey) {
            await Purchases.setDebugLogsEnabled(env.flavor == BuildFlavor.development);
            await Purchases.setup(catKey, appUserId: qp.id);
            hasCalledCatKey = true;
            bool currentIsVip = await iapManager.isVipActive();
            if (currentIsVip && !myProfile.isVip) {
              await apiManager.setVip();
            } else if (!currentIsVip && myProfile.isVip) {
              await apiManager.setNoVip();
            }
            myProfile.isVip = currentIsVip;
          }
        }
      } else {
        _onFailure();
      }
    });
  }

  Future<void> getCatKey() async {
    final url = apiBaseUrl + "/getCatKey";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        catKey = jsonResponse[DataKey.catKey];
      }
    }).catchError((onError) {
      utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
    });
  }

  Future<void> robotUser(String targetId) async {
    final url = apiBaseUrl + "/robotUser";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
    });
  }

  Future<void> logAnalysis(String name) async {
    analytics.logEvent(
      name: name.trim(),
    );
  }

  Future<dynamic> reportUser(String targetId, String reason) async {
    final url = apiBaseUrl + "/reportUser";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
      DataKey.reason: reason,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("Report error: $onError");
      utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
    });
  }

  Future<void> verifyUser(String targetId) async {
    final url = apiBaseUrl + "/verifyUser";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
    });
  }

  Future<dynamic> checkOtp(String otp) async {
    final url = apiBaseUrl + "/checkOneTimePassword";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.otp: otp,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      return false;
    });
  }

  Future<dynamic> getDiscover({bool isRefresh = false}) async {
    final url = apiBaseUrl + "/getRandomDiscovers";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.isRefresh: isRefresh.toString(),
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        return json.decode(response.body);
      }
      return StatusCode.exception;
    }).catchError((onError) {
      return StatusCode.exception;
    });
  }

  Future<void> registerFcm(String fcm) async {
    final url = apiBaseUrl + "/registerFCMToken";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.fcm: fcm,
      DataKey.platform: Platform.isAndroid ? "Android" : "iOS",
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        var jsonResponse = json.decode(response.body);
        int status = jsonResponse[DataKey.status];
        hasCalledFCMRegistration = status == StatusCode.ok;
        debugPrint("hasCalledFCMRegistration: $hasCalledFCMRegistration");
        utils.saveBoo(PrefKey.hasRegisteredFCMKey, hasCalledFCMRegistration);
      }
    }).catchError((onError) {
      debugPrint("Fcm Error: $onError");
    });
  }

  Future<dynamic> viewProfileLog(String targetId) async {
    final url = apiBaseUrl + "/viewProfileLog";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
    };
    return http.post(url, body: param);
  }

  Future<dynamic> getProfileById(String targetId) async {
    final url = apiBaseUrl + "/getProfileById";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        return json.decode(response.body);
      }
      return StatusCode.exception;
    }).catchError((onError) {
      return StatusCode.exception;
    });
  }

  Future<dynamic> getPotentialMatches({bool isRefresh = false}) async {
    final url = apiBaseUrl + "/getMyPotentialMatches";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.isRefresh: isRefresh.toString(),
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        return json.decode(response.body);
      }
      return StatusCode.exception;
    }).catchError((onError) {
      return StatusCode.exception;
    });
  }

  Future<dynamic> getVerificationCode(String email) async {
    String deviceName;
    DeviceInfoPlugin deviceInfoPlug = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      final androidInfo = await deviceInfoPlug.androidInfo;
      deviceName = androidInfo.model;
    } else {
      final iosInfo = await deviceInfoPlug.iosInfo;
      deviceName = iosInfo.name;
    }
    final url = apiBaseUrl + "/sendVerificationCode";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.email: email,
      DataKey.deviceName: deviceName,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        return json.decode(response.body);
      }
      return StatusCode.exception;
    }).catchError((onError) {
      print("get veri code error: $onError");
      return StatusCode.exception;
    });
  }

  Future<void> updateVerificationImagePath(String image, String email) async {
    final url = apiBaseUrl + "/updateVerificationImagePath";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.image: image,
      DataKey.email: email,
    };
    return http.post(url, body: param).catchError((onError) {
      debugPrint("Upload error: $onError");
    });
  }

  Future<void> updateImagesPath(dynamic imagePath, String email, bool isVerification) async {
    final url = apiBaseUrl + "/updateImagePath";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.image: imagePath.toString(),
      DataKey.email: email,
      DataKey.isVerification: isVerification.toString(),
    };
    return http.post(url, body: param).catchError((onError) {
      debugPrint("Upload images error: $onError");
    });
  }

  Future<void> block(String targetId, int reason) async {
    final url = apiBaseUrl + "/block";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
      DataKey.reason: reason.toString(),
    };
    return http.post(url, body: param).catchError((onError) {
      debugPrint("Block error: $onError");
    });
  }

  Future<void> updateVideoPath(String videoPath, String email, String videoThumbnailUrl) async {
    final url = apiBaseUrl + "/updateVideoPath";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.video: videoPath ?? "",
      DataKey.email: email,
      DataKey.thumbnail: videoThumbnailUrl ?? "",
    };
    return http.post(url, body: param).catchError((onError) {
      debugPrint("Upload video error: $onError");
    });
  }

  Future<dynamic> fetchActivity(int startFrom) async {
    final url = apiBaseUrl + "/fetchActivity";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.startFrom: startFrom.toString(),
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      debugPrint("fetch activity error: $onError");
    });
  }

  Future<dynamic> likeOrUnlikeProfile(String targetId, {bool isDiscover = false}) async {
    final url = apiBaseUrl + "/likeProfile";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
      DataKey.isDiscover: isDiscover.toString(),
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        int jsonStatus = jsonResponse[DataKey.status];
        return jsonStatus;
      }
    }).catchError((onError) {
      return StatusCode.exception;
    });
  }

  Future<void> passProfile(String targetId) async {
    final url = apiBaseUrl + "/passProfile";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.targetId: targetId,
    };
    return http.post(url, body: param);
  }

  Future<List<bool>> isEmailAvailable(String email) async {
    final url = apiBaseUrl + "/checkEmail";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.email: email,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        var jsonStatus = jsonResponse[DataKey.status];
        if (jsonStatus == StatusCode.ok) {
          var hasProfileStatus = jsonResponse[DataKey.hasProfile];
          var hasPreferenceStatus = jsonResponse[DataKey.hasPreference];
          return [hasProfileStatus == StatusCode.hasProfile, hasPreferenceStatus == StatusCode.hasPreference];
        }
        return [false, false];
      }
      return [false, false];
    }).catchError((onError) {
      debugPrint("is email error: $onError");
      return [false, false];
    });
  }

  Future<dynamic> createUpdatePreference(
      String email,
      List<int> gender,
      int distance,
      List<int> age,
      List<String> ethnicity,
      List<String> religion,
      List<String> zodiacSign,
      List<String> education,
      List<String> hobby,
      bool isGreenHead,
      int preferredRelationship,
      List<int> likeCount,
      ) async {
    final url = apiBaseUrl + "/createUpdatePreference";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.email: email,
      DataKey.id: qp.id,
      DataKey.distance: distance.toString(),
      DataKey.religion: religion.toString(),
      DataKey.zodiacSign: zodiacSign.toString(),
      DataKey.education: education.toString(),
      DataKey.ethnicity: ethnicity.toString(),
      DataKey.gender: gender.toString(),
      DataKey.age: age.toString(),
      DataKey.preferredRelationship: preferredRelationship.toString(),
      DataKey.isGreenHead: isGreenHead.toString(),
      DataKey.hobby: hobby.toString(),
      DataKey.likeCount: likeCount.toString(),
    };
    return http.post(url, body: param).then((http.Response response) {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        var jsonStatus = jsonResponse[DataKey.status];
        return jsonResponse;
      }
    }).catchError((onError) {
      print("Error: $onError");
    });
  }

  Future<dynamic> updateProfile(
      String firstName,
      String lastName,
      String email,
      String country,
      String city,
      double longitude,
      double latitude,
      int gender,
      String color,
      String birthday,
      int age,
      String ethnicity,
      String religion,
      String zodiacSign,
      String profession,
      String schoolName,
      String education,
      String personality,
      int preferredRelationship,
      bool isGreenHead,
      String hobby,
      String verificationInstruction,
      ) async {
    final url = apiBaseUrl + "/editProfile";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.id: qp.id,
      DataKey.email: qp.email,
      DataKey.firstName: firstName,
      DataKey.lastName: lastName,
      DataKey.country: country,
      DataKey.city: city,
      DataKey.longitude: longitude.toString(),
      DataKey.latitude: latitude.toString(),
      DataKey.gender: gender.toString(),
      DataKey.color: color,
      DataKey.birthday: birthday,
      DataKey.age: age.toString(),
      DataKey.ethnicity: ethnicity,
      DataKey.religion: religion,
      DataKey.zodiacSign: zodiacSign,
      DataKey.profession: profession,
      DataKey.education: education,
      DataKey.graduatedFrom: schoolName,
      DataKey.personality: personality,
      DataKey.preferredRelationship: preferredRelationship.toString(),
      DataKey.isGreenHead: isGreenHead.toString(),
      DataKey.hobby: hobby,
      DataKey.instruction: verificationInstruction,
    };
    return http.post(url, body: param).then((http.Response response) {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        return jsonResponse;
      }
    }).catchError((onError) {
      print("Edit profile error: $onError");
    });
  }

  Future<dynamic> createProfile(
    String firstName,
    String lastName,
    String email,
    String password,
    String country,
    String city,
    double longitude,
    double latitude,
    int gender,
    String color,
    String birthday,
    int age,
    String ethnicity,
    String religion,
    String zodiacSign,
    String profession,
    String schoolName,
    String education,
    String personality,
    int preferredRelationship,
    bool isGreenHead,
    String hobby,
    String verificationInstruction,
    ) async {
    final url = apiBaseUrl + "/createProfile";
    Map<String, String> param = {
      DataKey.uniqueId: qp.uniqueId,
      DataKey.secret: qp.secret,
      DataKey.firstName: firstName,
      DataKey.lastName: lastName,
      DataKey.email: email,
      DataKey.password: utils.encodeString(password),
      DataKey.country: country,
      DataKey.city: city,
      DataKey.longitude: longitude.toString(),
      DataKey.latitude: latitude.toString(),
      DataKey.gender: gender.toString(),
      DataKey.color: color,
      DataKey.birthday: birthday,
      DataKey.age: age.toString(),
      DataKey.ethnicity: ethnicity,
      DataKey.religion: religion,
      DataKey.zodiacSign: zodiacSign,
      DataKey.profession: profession,
      DataKey.education: education,
      DataKey.graduatedFrom: schoolName,
      DataKey.personality: personality,
      DataKey.preferredRelationship: preferredRelationship.toString(),
      DataKey.isGreenHead: isGreenHead.toString(),
      DataKey.hobby: hobby,
      DataKey.instruction: verificationInstruction,
    };
    return http.post(url, body: param).then((http.Response response) {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        var jsonStatus = jsonResponse[DataKey.status];
        if (jsonStatus == StatusCode.ok) {
          qp.id = jsonResponse[DataKey.id];
          qp.email = email;
          qp.password = password;
          utils.saveStr(PrefKey.email,qp.email);
          utils.saveStr(PrefKey.id, qp.id);
          utils.saveStr(PrefKey.password, password);
        }
        return jsonResponse;
      }
    }).catchError((onError) {
      print("Error: $onError");
    });
  }

  Future<void> initClientOrRetrieveData() async {
    final url = apiBaseUrl + "/createClient";
    DeviceInfoPlugin deviceInfoPlug = DeviceInfoPlugin();
    String deviceName, deviceVersion, deviceId;
    if (Platform.isAndroid) {
      final androidInfo = await deviceInfoPlug.androidInfo;
      deviceName = androidInfo.model;
      deviceVersion = androidInfo.version.release.toString();
      deviceId = androidInfo.androidId;
    } else {
      final iosInfo = await deviceInfoPlug.iosInfo;
      deviceName = iosInfo.name;
      deviceVersion = iosInfo.systemVersion;
      deviceId = iosInfo.identifierForVendor;
    }
    Map<String, String> param = {
      DataKey.deviceId: deviceId,
      DataKey.deviceName: deviceName,
      DataKey.deviceVersion: deviceVersion,
    };
    return http.post(url, body: param).then((http.Response response) async {
      if (response.statusCode == StatusCode.ok) {
        final jsonResponse = json.decode(response.body);
        var jsonStatus = jsonResponse[DataKey.status];
        if (jsonStatus == StatusCode.ok) {
          qp.uniqueId = jsonResponse[DataKey.uniqueId];
          qp.secret = jsonResponse[DataKey.secret];
          utils.saveStr(PrefKey.uniqueId, qp.uniqueId);
          utils.saveStr(PrefKey.secret, qp.secret);
        }
      } else {
        utils.restartApp();
      }
    }).catchError((onError) {
      debugPrint("Error: $onError");
      utils.toast(TextData.connectionFailed, bgColor: warning, iconData: warningIcon);
    });
  }
}